//
//  UIItemRiwayat.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 06/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIItemRiwayat: CustomUIView {

    // tingginya 90
    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var labelCode: UILabel!
    @IBOutlet weak var labelWeight: UILabel!
    @IBOutlet weak var labelCost: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    func setupXib(){
        self.xibSetup(nibName: "UIItemRiwayat")
    }
    
    func setupXib(modItemRiwayat: ModItemRiwayat){
        if modItemRiwayat.is_complete {
            self.setupXib(is_antar: modItemRiwayat.is_antar,
                          code: modItemRiwayat.code,
                          weight: modItemRiwayat.weight,
                          cost: modItemRiwayat.cost,
                          description: modItemRiwayat.description)
        } else {
            print("your ModItemRiwayat instance is not complete yet")
        }
    }
    
    func setupXib(is_antar: Bool, code: String, weight: String, cost: String, description: String){
        self.xibSetup(nibName: "UIItemRiwayat")
        var image = UIImage(named: "shipment_icon_gray")
        if is_antar {
            image = UIImage(named: "plane_icon_gray")
        }
        
        imageIcon.image = image
        labelCode.text = code
        labelWeight.text = weight
        labelCost.text = cost
        labelDescription.text = description
    }
}

class ModItemRiwayat {
    public var id: Int?
    public var is_antar: Bool = true
    public var code: String = ""
    public var weight: String = ""
    public var cost: String = ""
    public var description: String = ""
    public var is_complete: Bool = false
}
