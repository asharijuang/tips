//
//  RiwayatOrderKirimVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire
import DatePickerDialog

class RiwayatOrderKirimVC: UIViewController {

    let statusPicker        = UIPickerView()
    let kotaTujuanPicker    = UIPickerView()
    
    var statusList          : [SStatusShipmentDelivery] = []
    var kotaTujuanList      : [SCity]                   = []
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var optionContainerView: UIView!
    @IBOutlet weak var imageOptionButton: UIImageView!
    
    // IBOutlet option
    @IBOutlet weak var buttonClear: UIButton!
    @IBOutlet weak var buttonCari: UIButton!
    @IBOutlet weak var fieldStartDate: UITextField!
    @IBOutlet weak var fieldEndDate: UITextField!
    @IBOutlet weak var fieldNamaPenerimaKotaTujuan: UITextField!
    @IBOutlet weak var textNamaPenerima: UILabel!
    @IBOutlet weak var textKotaTujuan: UILabel!
    @IBOutlet weak var fieldStatus: UITextField!
    
    var selectedIdStatus: Int? = 0
    var selectedIdCity: Int?
    var startDate: String?
    var endDate: String?
    
    var dummyData: [ModItemRiwayat] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initLayout()
        imageOptionButton.addTapGestureRecognizer {
            self.optionContainerView.isHidden = !self.optionContainerView.isHidden
        }
        optionContainerView.addTapGestureRecognizer {
            self.optionContainerView.isHidden = !self.optionContainerView.isHidden
        }
        self.ambilRiwayat()
        self.initOptionTextPenerimaKota()
        self.addTapListenerToOptionTextPenerimaKota()
        self.createStatusPicker()
        // Do any additional setup after loading the view.
    }
    
    func initLayout(){
        optionContainerView.isHidden = true
        
        // setup option layout
        ViewController.setOLabel(button: self.buttonClear, text: "Clear", filled: true, reverseColor: false, ratioCircle: 0.4, font_size: 12)
        ViewController.setOLabel(button: self.buttonCari, text: "Cari", filled: true, reverseColor: true, ratioCircle: 0.4, font_size: 12)
        self.fieldStartDate.addTapGestureRecognizer {
            DatePickerDialog(locale: Locale(identifier: "id")).show("Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
                (date) -> Void in
                if let dt = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy"
                    let date_string = formatter.string(from: dt)
                    self.fieldStartDate.text = date_string
                    
                    formatter.dateFormat = "yyyy-MM-dd"
                    self.startDate = formatter.string(from: dt)
                }
            }
        }
        self.fieldEndDate.addTapGestureRecognizer {
            DatePickerDialog(locale: Locale(identifier: "id")).show("End Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
                (date) -> Void in
                if let dt = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy"
                    let date_string = formatter.string(from: dt)
                    self.fieldEndDate.text = date_string
                    
                    formatter.dateFormat = "yyyy-MM-dd"
                    self.endDate = formatter.string(from: dt)
                }
            }
        }
        self.buttonClear.addTapGestureRecognizer {
            self.resetParamAndView()
        }
        self.buttonCari.addTapGestureRecognizer {
            print(self.getCariParam().debugDescription)
            self.ambilRiwayat(customParams: self.getCariParam())
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // for dummy data testing
        //self.createDummy()
        //self.initDataDummy()
    }
    
    func createDummy(){
        for i in 0...15 {
            let item = ModItemRiwayat()
            item.is_antar = false
            item.code = "CODE #" + i.description
            item.cost = String(Int(arc4random_uniform(100000) + 1))
            item.weight = String(Int(arc4random_uniform(25) + 1))
            item.description = "Lorem ipsum dolor sit amet consectetur adipiscing elit"
            item.is_complete = true
            
            dummyData.append(item)
        }
    }
    
    
    func connGetRiwayat(customParams: Parameters? = nil, callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "id_member": HomeVC.user.id!
        ]
        
        Alamofire.request(RiwayatRouter.data_kirim(parameters: customParams != nil ? customParams! : param)).responseJSON(completionHandler: callback)
    }
    
    func ambilRiwayat(customParams: Parameters? = nil){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connGetRiwayat(customParams: customParams, callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getDataAsArr(json_data: response)
            print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.parseDataRiwayat(datas: result)
            self.initDataDummy()
        })
    }
    
    func parseDataRiwayat(datas: [Any]? = nil) {
        if datas == nil {
            print("Error: no result.messages data served")
            return
        }
        
        dummyData.removeAll(keepingCapacity: false)
        for data in datas! {
            let kirim = data as! [String: Any]
            
            let item = ModItemRiwayat()
            item.is_antar = false
            item.code = kirim["shipment_id"] as! String
            
            let flight_cost = kirim["flight_cost"] as! Int
            let add_insurance_cost = kirim["add_insurance_cost"] as! Int
            let estimate_weight = Double.init(kirim["estimate_weight"] as! Int)
            let real_weight = Double.init(kirim["real_weight"] as? String ?? "0")
            
            let weight = real_weight! > Double(0) ? real_weight : estimate_weight
            let total_cost = flight_cost + add_insurance_cost
            
            item.cost = String(total_cost)
            item.weight = String(weight!) + " Kg"
            item.description = kirim["shipment_status_description"] as! String
            item.is_complete = true
            
            dummyData.append(item)
        }
    }
    
    
    func initDataDummy(){
        
        // run this after view loaded to
        // get real width of scrollview
        scrollView.removeAllSubviews()
        
        var total_height = CGFloat(0)
        let width = scrollView.frame.size.width
        let item_height = CGFloat(90)
        for dummy in self.dummyData {
            let view = UIItemRiwayat()
            view.frame = CGRect(x: CGFloat(0), y: total_height, width: width, height: item_height)
            view.setupXib(modItemRiwayat: dummy)
            scrollView.addSubview(view)
            
            total_height += item_height
        }
        
        scrollView.contentSize = CGSize(width: width, height: total_height)
    }
    
    @objc func doneKotaAsalPicker(){
        let kota = self.kotaTujuanList[self.kotaTujuanPicker.selectedRow(inComponent: 0)]
        self.selectedIdCity = kota.id
        self.fieldNamaPenerimaKotaTujuan.text = kota.name
        self.fieldNamaPenerimaKotaTujuan.resignFirstResponder()
    }
    
    @objc func cancelKotaAsalPicker(){
        self.fieldNamaPenerimaKotaTujuan.resignFirstResponder()
    }
    
    @objc func doneStatusPicker(){
        let status = self.statusList[self.statusPicker.selectedRow(inComponent: 0)]
        self.selectedIdStatus = status.id
        self.fieldStatus.text = status.description
        self.fieldStatus.resignFirstResponder()
    }
    
    @objc func cancelStatusPicker(){
        self.fieldStatus.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RiwayatOrderKirimVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func createStatusPicker(){
        self.statusList.removeAll(keepingCapacity: false)
        let semua_status = SStatusShipmentDelivery()
        semua_status.id = 0
        semua_status.step = 0
        semua_status.description = "Semua Status"
        self.statusList.append(semua_status)
        for s in S.statuses_delivery {
            self.statusList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: self.statusPicker, inputField: self.fieldStatus,
                          action_done: #selector(RiwayatOrderKirimVC.doneStatusPicker),
                          action_cancel: #selector(RiwayatOrderKirimVC.cancelStatusPicker))
    }
    
    func createKotaTujuanPicker(){
        self.kotaTujuanList.removeAll(keepingCapacity: false)
        for s in S.airport_city_list {
            self.kotaTujuanList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: self.kotaTujuanPicker, inputField: self.fieldNamaPenerimaKotaTujuan,
                          action_done: #selector(RiwayatOrderKirimVC.doneKotaAsalPicker),
                          action_cancel: #selector(RiwayatOrderKirimVC.cancelKotaAsalPicker))
    }
    
    func removeKotaTujuanPicker(){
        self.removePicker(pickerView: self.kotaTujuanPicker, inputField: self.fieldNamaPenerimaKotaTujuan)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.statusPicker {
            return self.statusList.count
        }
        if pickerView == self.kotaTujuanPicker {
            return self.kotaTujuanList.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.statusPicker {
            return self.statusList[row].description
        }
        if pickerView == self.kotaTujuanPicker {
            return self.kotaTujuanList[row].name
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}

extension RiwayatOrderKirimVC {
    func setTextNamaPenerimaActive(){
        self.textNamaPenerima.textColor = UIColor.init(rgb: ViewController.PRIMARY_COLOR)
        self.textKotaTujuan.textColor = UIColor.black
        self.fieldNamaPenerimaKotaTujuan.placeholder = "Nama Penerima"
        self.fieldNamaPenerimaKotaTujuan.text = ""
        self.fieldNamaPenerimaKotaTujuan.resignFirstResponder()
        self.removeKotaTujuanPicker()
    }
    
    func setTextKotaTujuanActive(){
        self.textKotaTujuan.textColor = UIColor.init(rgb: ViewController.PRIMARY_COLOR)
        self.textNamaPenerima.textColor = UIColor.black
        self.fieldNamaPenerimaKotaTujuan.placeholder = "Pilih Kota Tujuan"
        self.fieldNamaPenerimaKotaTujuan.text = ""
        self.fieldNamaPenerimaKotaTujuan.resignFirstResponder()
        self.createKotaTujuanPicker()
    }
    
    func isNamaPenerimaActive() -> Bool {
        return self.textKotaTujuan.textColor == UIColor.black
    }
    
    func isKotaTujuanActive() -> Bool {
        return self.textNamaPenerima.textColor == UIColor.black
    }
    
    func getNamaPenerimaKotaTujuanData() -> String {
        return self.fieldNamaPenerimaKotaTujuan.text ?? ""
    }
    
    func initOptionTextPenerimaKota(){
        self.setTextNamaPenerimaActive()
    }
    
    func addTapListenerToOptionTextPenerimaKota(){
        self.textNamaPenerima.addTapGestureRecognizer {
            self.setTextNamaPenerimaActive()
        }
        self.textKotaTujuan.addTapGestureRecognizer {
            self.setTextKotaTujuanActive()
        }
    }
    
    func resetParamAndView(){
        self.selectedIdStatus = 0
        self.selectedIdCity = nil
        self.startDate = ""
        self.endDate = ""
        self.fieldEndDate.text = ""
        self.fieldStartDate.text = ""
        self.fieldNamaPenerimaKotaTujuan.text = ""
        self.fieldStatus.text = ""
    }
    
    func getCariParam() -> Parameters {
        let temp = getNamaPenerimaKotaTujuanData()
        let idcity = self.selectedIdCity == nil ? "" : String(self.selectedIdCity!)
        return [
            "id_member": HomeVC.user.id!,
            "id_shipment_status": self.selectedIdStatus!,
            "consignee_name": self.isNamaPenerimaActive() ? temp : "",
            "id_destination_city": self.isKotaTujuanActive() ? idcity : "",
            "start_transaction_date": self.startDate ?? "",
            "end_transaction_date": self.endDate ?? ""
        ]
    }
}
