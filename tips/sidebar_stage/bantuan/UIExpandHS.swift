//
//  UIExpandHS.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIExpandHS: CustomUIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelPlusAtauMinus: UILabel!
    @IBOutlet weak var labelJudul: UILabel!
    @IBOutlet weak var labelTeksPanjang: UILabel!
    @IBOutlet weak var fieldPerihal: TextField!
    @IBOutlet weak var fieldPertanyaan: UITextField!
    
    @IBOutlet weak var buttonKirim: UIButton!
    @IBOutlet weak var labelAndaDapatbla: UILabel!
    
    var statusHide: Bool = true
    var isHubungiKami: Bool = false
    var kirimCallback: ((_ perihal: String?, _ pertanyaan: String?) -> ())? = nil
    
    func setupXib(){
        self.xibSetup(nibName: "UIExpandHS")
        self.setupLayout()
    }
    
    func setupXib(modExpandHS: ModExpandHS){
        if modExpandHS.is_complete {
            if(modExpandHS.teks_panjang_2.count > 0){
                self.setupXib(judul: modExpandHS.judul, teks_panjang: modExpandHS.teks_panjang, teks_panjang_2: modExpandHS.teks_panjang_2)
            } else {
                self.setupXib(judul: modExpandHS.judul, teks_panjang: modExpandHS.teks_panjang)
            }
        } else {
            print("your ModExpandHS instance is not complete yet")
        }
    }
    
    func getExpandHeightSize() -> CGFloat {
        let maxLabelWidth: CGFloat = labelTeksPanjang.frame.size.width
        let neededSize = labelTeksPanjang.sizeThatFits(CGSize(width: maxLabelWidth, height: CGFloat.greatestFiniteMagnitude))
        
        return neededSize.height + CGFloat(isHubungiKami ? 350 : 0)
    }
    
    func setupXib(judul: String, teks_panjang: String){
        self.xibSetup(nibName: "UIExpandHS")
        labelJudul.text = judul
        labelTeksPanjang.text = teks_panjang
        self.setupLayout()
    }
    
    func setupXib(judul: String, teks_panjang: String, teks_panjang_2: String){
        self.xibSetup(nibName: "UIExpandHS")
        labelJudul.text = judul
        labelTeksPanjang.text = teks_panjang
        labelAndaDapatbla.text = teks_panjang_2
        self.setupLayout()
    }
    
    func setupLayout(){
        containerView.layer.cornerRadius = 4
        containerView.layer.masksToBounds = true
        labelTeksPanjang.isHidden = false
        fieldPerihal.layer.cornerRadius = 8
        fieldPertanyaan.layer.cornerRadius = 8
        buttonKirim.addTapGestureRecognizer {
            if self.kirimCallback != nil {
                self.kirimCallback!(self.fieldPerihal.text, self.fieldPertanyaan.text)
            }
        }
        
        ViewController.setOLabel(button: buttonKirim, text: "Kirim", filled: true, reverseColor: true)
    }
    
    func toggleVisibilityText() -> Bool {
        statusHide = !statusHide
        labelPlusAtauMinus.text = statusHide ? "+" : "-"
        labelTeksPanjang.isHidden = statusHide
        
        return statusHide
    }
    
    public func resetField(){
        fieldPerihal.text = ""
        fieldPertanyaan.text = ""
    }
}

class ModExpandHS{
    var judul: String = ""
    var teks_panjang: String = ""
    var teks_panjang_2: String = ""
    var is_complete: Bool = false
}
