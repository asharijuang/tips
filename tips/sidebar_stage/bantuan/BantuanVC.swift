//
//  BantuanVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class BantuanVC: UIViewController {

    @IBOutlet weak var scrollBantuan: UIScrollView!
    
    var dummyBantuan: [ModExpandHS] = []
    var allViewOnScroller: [[String: Any]] = []
    
    let MAGIC_HEIGHT = 56
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ambilBantuan()
        // Do any additional setup after loading the view.
    }
    func connGetHelp(callback: @escaping (DataResponse<Any>) -> Void){
        Alamofire.request(HomeRouter.help(parameters: [:])).responseJSON(completionHandler: callback)
    }
    
    func ambilBantuan(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connGetHelp(callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getDataAsArr(json_data: response)
            print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.parseDataBantuan(helps: result)
            self.createScollBantuan()
            self.createScrollerGesture()
        })
    }
    
    
    func parseDataBantuan(helps: [Any]? = nil) {
        if helps == nil {
            print("Error: no result data served")
            return
        }
        
        for h in helps! {
            let help = h as! [String: Any]
            
            let dummy = ModExpandHS()
            dummy.judul = help["title"] as! String
            let arrDesc = help["description"] as! [String]
            let arrDesc2 = help["addt_info"] as? String
            dummy.teks_panjang = arrDesc.joined(separator: "\n")
            dummy.teks_panjang_2 = arrDesc2 ?? ""
            dummy.is_complete = true
            self.dummyBantuan.append(dummy)
        }
    }
    
    func createDummy(){
        for i in 1...15 {
            let dummy = ModExpandHS()
            dummy.judul = "contoh ke " + i.description
            dummy.teks_panjang = i.description + " lorem ipsum dolor sit amet consectetur adipiscing elit lorem ipsum dolor sit amet consectetur adipiscing elit lorem ipsum dolor sit amet consectetur adipiscing elit"
            dummy.is_complete = true
            self.dummyBantuan.append(dummy)
        }
    }
    
    func createScollBantuan(){
        // againnn...
        let MAGIC_NUMBER = 0
        let MAGIC_TOP_MARGIN = 8
        
        let width = scrollBantuan.frame.size.width - CGFloat(MAGIC_NUMBER)
        var total_height = MAGIC_TOP_MARGIN
        
        var index = 0
        let last_index = self.dummyBantuan.count - 1
        for dummy in self.dummyBantuan {
            let view = UIExpandHS()
            if index == last_index {
                view.isHubungiKami = true
                view.kirimCallback = {
                    perihal, pertanyaan in
                    self.submitReport(topik: perihal!, isi: pertanyaan!, success_cb: {
                        view.resetField()
                    }, fail_cb: {
                        
                    })
                }
            }
            view.frame = CGRect(x: 0, y: total_height, width: Int(width), height: MAGIC_HEIGHT)
            scrollBantuan.addSubview(view)
            view.setupXib(modExpandHS: dummy)
            view.layer.masksToBounds = true
            let d = [
                "view": view,
                "index": index
            ] as [String : Any]
            allViewOnScroller.append(d)
            
            total_height += MAGIC_HEIGHT
            index += 1
        }
        
        scrollBantuan.contentSize = CGSize(width: width, height: CGFloat(total_height))
    }
    
    func createScrollerGesture(){
        // againnn...
        let MAGIC_NUMBER = 0
        let MAGIC_MARGIN_BOTTOM = 4
        
        let width = scrollBantuan.frame.size.width - CGFloat(MAGIC_NUMBER)
        
        for dict in allViewOnScroller {
            let view = dict["view"] as! UIExpandHS
            view.addTapGestureRecognizer {
                let heightToToggle = view.getExpandHeightSize() + CGFloat(MAGIC_MARGIN_BOTTOM)
                print("toggle height", heightToToggle)
                
                UIView.animate(withDuration: 0.2, animations: {
                    let isShow = !view.toggleVisibilityText()
                    let _ix = dict["index"] as! Int
                    view.frame.size = CGSize(width: Int(width), height: Int(64 + (isShow ? heightToToggle : 0)))
                    let delta = isShow ? heightToToggle : -heightToToggle
                    self.scrollBantuan.contentSize.height += delta
                    
                    for innerD in self.allViewOnScroller {
                        let index = innerD["index"] as! Int
                        if index > _ix {
                            let view = innerD["view"] as! UIExpandHS
                            let current_size = view.frame.size
                            view.frame = CGRect(x: 0, y: view.frame.origin.y + delta, width: current_size.width, height: current_size.height)
                        }
                    }
                })
            }
        }
    }
    
    func moveSomeItem(index: Int, delta: CGFloat, currentYAndHeight: CGFloat){

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BantuanVC {
    func submitReport(topik: String, isi: String, success_cb: (() -> ())? = nil, fail_cb: (() -> ())? = nil){
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(HomeRouter.report(parameters: [
            "id": HomeVC.user.id!,
            "topik": topik,
            "isi": isi
            ])).responseJSON {
                json_data in
                
                // check if response is contains error or no result
                if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                    E.showUIError(vc: self, completion: {
                        UIViewController.removeSpinner(spinner: sv)
                    })
                    return
                }
                
                let json = json_data.result.value as? [String: Any]
                // no need to check result
                let error = json!["err"] as? [String: Any]
                
                UIViewController.removeSpinner(spinner: sv)
                
                if(error != nil){
                    if fail_cb != nil {
                        fail_cb!()
                    }
                }
                
                if success_cb != nil {
                    success_cb!()
                }
                
                E.showUISuccess(vc: self, message: (json!["result"]! as! [String: Any])["message"] as! String)
        }
    }
}
