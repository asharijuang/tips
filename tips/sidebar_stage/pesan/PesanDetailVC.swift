//
//  PesanDetailVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 05/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class PesanDetailVC: XUIViewController {

    @IBOutlet weak var imageTrash: UIImageView!
    @IBOutlet weak var labelPengirim: UILabel!
    @IBOutlet weak var labelSubjek: UILabel!
    @IBOutlet weak var labelDateTime: UILabel!
    @IBOutlet weak var labelTeksPanjang: UILabel!
    
    var idPesan: Int?
    var parentVC: TwoHeaderTabVC?
    
    var hapusPesanCaller = HapusPesanCaller()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    public func setupData(id: Int, pengirim: String, subjek: String, date_time: String, teks_panjang: String){
        labelPengirim.text = pengirim
        labelSubjek.text = subjek
        labelDateTime.text = date_time
        labelTeksPanjang.text = teks_panjang
        self.idPesan = id
        imageTrash.addTapGestureRecognizer {
            self.hapusPesanCaller.alertHapusPesan(vcsender: self, id: self.idPesan!, completion: {
                self.parentVC?.doBack()
                self.parentVC?.pesanVC?.ambilPesan()
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
