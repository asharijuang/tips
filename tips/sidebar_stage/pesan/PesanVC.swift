//
//  PesanVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class PesanVC: XUIViewController {

    @IBOutlet weak var scrollPesan: UIScrollView!
    
    var parentVC: TwoHeaderTabVC?
    var detailVC: PesanDetailVC?
    var pesanDummy: [ModItemPesan] = []
    var hapusPesanCaller = HapusPesanCaller()
    
    var isOnDetailMessage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ambilPesan()
        // Do any additional setup after loading the view.
    }
    
    func connGetMessages(callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "id_user": HomeVC.user.id!
        ]
        Alamofire.request(PesanRouter.get_all_messages(parameters: param)).responseJSON(completionHandler: callback)
    }
    
    public func slideLeftToListMessage(){
        UIView.animate(withDuration: 0.3, animations: {
            self.parentVC?.scrollView.contentOffset = CGPoint(x: 0, y: 0)
            self.isOnDetailMessage = false
        })
        
    }
    
    func ambilPesan(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connGetMessages(callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.parseDataPesan(messages: result["messages"] as? [Any])
            self.initScrollPesan()
        })
    }
    
    
    func parseDataPesan(messages: [Any]? = nil) {
        if messages == nil {
            print("Error: no result.messages data served")
            return
        }
        
        self.pesanDummy.removeAll(keepingCapacity: false)
        for msg in messages! {
            let message = msg as! [String: Any]
            
            let d1 = ModItemPesan()
            d1.sender = message["nama_pengirim"] as! String
            d1.subject = message["subjek"] as! String
            d1.date_time = message["created_at"] as! String
            d1.full_text = message["message"] as! String
            d1.id = message["id"] as? Int
            d1.is_complete = true
            self.pesanDummy.append(d1)
        }
    }
    
    func createDummy(){
        for i in 1...15 {
            let d1 = ModItemPesan()
            d1.sender = "TIPS"
            d1.subject = "Test subject " + i.description
            d1.date_time = i.description + " Februari 2018"
            d1.is_complete = true
            self.pesanDummy.append(d1)
        }
    }
    
    func initScrollPesan(){
        // LOL :D
        let MAGIC_NUMBER = 0
        
        var total_height = 0
        let width = scrollPesan.frame.size.width - CGFloat(MAGIC_NUMBER)
        self.scrollPesan.removeAllSubviews()
        for pesan in self.pesanDummy {
            let view = UIItemPesan()
            view.frame = CGRect(x: 0, y: total_height, width: Int(width), height: 60)
            scrollPesan.addSubview(view)
            view.addTapGestureRecognizer {
                UIView.animate(withDuration: 0.3, animations: {
                    self.detailVC?.setupData(id: pesan.id!, pengirim: pesan.sender, subjek: pesan.subject, date_time: pesan.date_time, teks_panjang: pesan.full_text)
                    self.parentVC?.scrollView.contentOffset = CGPoint(x: (self.parentVC?.scrollView.frame.size.width)!, y: 0)
                    self.isOnDetailMessage = true
                    self.parentVC?.scrollView.isScrollEnabled = true
                })
            }
            view.setupXib(modItemPesan: pesan)
            view.imageTrash.addTapGestureRecognizer {
                self.hapusPesanCaller.alertHapusPesan(vcsender: self, id: pesan.id!, completion: {
                    self.ambilPesan()
                })
            }
            total_height += 60
        }
        scrollPesan.contentSize = CGSize(width: CGFloat(width), height: CGFloat(total_height))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

open class HapusPesanCaller {
    func alertHapusPesan(vcsender: UIViewController, id: Int, completion: (() -> ())?){
        let finalTitle = "Hapus Pesan"
        let finalMessage = "Anda ingin menghapus pesan ini?"
        
        let refreshAlert = UIAlertController(title: finalTitle, message: finalMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.hapus_pesan(vcsender: vcsender, id: id, completion: completion)
        }))
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        vcsender.present(refreshAlert, animated: true, completion: nil)
    }
    
    private func hapus_pesan(vcsender: UIViewController, id: Int, completion: (() -> ())?){
        
        let sv = UIViewController.displaySpinner(onView: vcsender.view)
        self.call_hapus_pesan(id: id) { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: vcsender, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // we dont care with the result
            UIViewController.removeSpinner(spinner: sv)
            E.showUISuccess(vc: vcsender, message: "Pesan berhasil dihapus", completion: {
                if completion != nil {
                    completion!()
                }
            });
        }
    }
    
    private func call_hapus_pesan(id: Int, callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "id_user": HomeVC.user.id!,
            "id_pesan": id
        ]
        
        Alamofire.request(PesanRouter.delete_message(parameters: param)).responseJSON(completionHandler: callback)
    }
}
