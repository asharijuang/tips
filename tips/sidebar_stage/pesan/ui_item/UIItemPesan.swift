//
//  ItemPesan.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIItemPesan: CustomUIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var labelSender: UILabel!
    @IBOutlet weak var labelDateTime: UILabel!
    @IBOutlet weak var labelSubject: UILabel!
    @IBOutlet public weak var imageTrash: UIImageView!
    
    func setupXib(){
        self.xibSetup(nibName: "UIItemPesan")
    }
    
    func setupXib(modItemPesan: ModItemPesan){
        if modItemPesan.is_complete {
            self.setupXib(sender: modItemPesan.sender,
                          date_time: modItemPesan.date_time,
                          subject: modItemPesan.subject)
        } else {
            print("your ModItemPesan instance is not complete yet")
        }
    }
    
    func setupXib(sender: String, date_time: String, subject: String){
        self.xibSetup(nibName: "UIItemPesan")
        labelSender.text = sender
        labelDateTime.text = date_time
        labelSubject.text = subject
    }
}

class ModItemPesan {
    public var id: Int?
    public var sender: String = ""
    public var date_time: String = ""
    public var subject: String = ""
    public var full_text: String = ""
    public var is_complete: Bool = false
}
