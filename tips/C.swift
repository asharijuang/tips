//
//  C.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 11/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation

class C {
    
    static let KEY_IS_NOT_FIRST_TIME = "is_not_first_time"
    static let KEY_IS_NOT_GUEST = "is_not_guest"
    static let KEY_ENDPOINT_URL = "endpoint_url"
    
    func getDefaultConfig() -> UserDefaults{
        return UserDefaults.standard
    }
    
    func isGuest() -> Bool {
        return !self.getDefaultConfig().bool(forKey: C.KEY_IS_NOT_GUEST)
    }
    
    func setNotGuest() {
        self.getDefaultConfig().set(true, forKey: C.KEY_IS_NOT_GUEST)
    }
    
    func setGuest() {
        self.getDefaultConfig().set(false, forKey: C.KEY_IS_NOT_GUEST)
    }
    
    func setProductionEndpoint() {
        self.getDefaultConfig().set(true, forKey: C.KEY_ENDPOINT_URL)
        __CR.changeEndpoint()
    }
    
    func setDevelopmentEndpoint() {
        self.getDefaultConfig().set(false, forKey: C.KEY_ENDPOINT_URL)
        __CR.changeEndpoint()
    }
    
    func setupFirstTime() {
        self.getDefaultConfig().set(true, forKey: C.KEY_IS_NOT_FIRST_TIME)
    }
    
    func getEndpointConfig() -> Bool {
        return self.getDefaultConfig().bool(forKey: C.KEY_ENDPOINT_URL) // default is false if not existed yet
    }
    
    func isFirstTime() -> Bool {
        return !self.getDefaultConfig().bool(forKey: C.KEY_IS_NOT_FIRST_TIME)
    }
}
