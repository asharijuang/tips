//
//  AntarSuksesVC.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 15/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class AntarSuksesVC: XUIViewController {

    @IBOutlet weak var containerDataAntar: UIView!
    @IBOutlet weak var labelPalingAtas: UILabel!
    @IBOutlet weak var labelKodeAntar: UILabel!
    @IBOutlet weak var flightOriDestData: UIFlightOriDest!
    @IBOutlet weak var labelKeterangan1: UILabel!
    @IBOutlet weak var labelKeterangan2: UILabel!
    @IBOutlet weak var labelKeterangan3: UILabel!
    @IBOutlet weak var buttonKembaliKeDashboard: UIButton!
    
    var model_data = ModAntarSuksesVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restyleUIComponents()
        buttonKembaliKeDashboard.addTapGestureRecognizer{
            self.backToRootHome()
        }
        // Do any additional setup after loading the view.
    }
    
    func restyleUIComponents(){
        containerDataAntar.layer.cornerRadius = 10
        model_data.flightOriDest!.ori_date_time = model_data.flightOriDest!.ori_date_time.left_str(n_right: 3)
        flightOriDestData.setupXib(modFlightOriDest: model_data.flightOriDest!)
        
        labelKodeAntar.text     = model_data.kodeBooking!
        labelPalingAtas.text    = "Selamat!\nPenerbangan telah terdaftar."
        labelKeterangan1.text   = "Kami akan mencari barang antaran\nsesuai penerbangan Anda."
        labelKeterangan2.text   = "Apabila tersedia, kami akan\nmengirimkan notifikasi."
        labelKeterangan3.text   = "Jika tidak ada antaran sampai akhir\nbatas waktu, maka kode pengantar ini\nakan terhapus otomatis"
        ViewController.setOLabel(button: buttonKembaliKeDashboard, text: "Kembali ke Dashboard", filled: true, reverseColor: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backToRootHome(){
        self.navigationController?.dismiss(animated: true, completion: {
            if let navc = self.navigationController as? GeneralNAVC {
                if navc.completion != nil {
                    navc.completion!()
                }else{
                    print("GeneralNAVC completion callback not implemented")
                }
            }
        })
    }
}

class ModAntarSuksesVC{
    public var kodeBooking: String?
    public var flightOriDest: ModFlightOriDest?
}

extension String {
    func left_str(n_right: Int) -> String {
        let endIndex = self.index(self.endIndex, offsetBy: -n_right)
        return self.substring(to: endIndex)
    }
}
