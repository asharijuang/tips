//
//  BookingDetailVC.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 15/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import DatePickerDialog
import PopupDialog
import Alamofire

class BookingDetailVC: XUIViewController{
    @IBOutlet weak var labelEditPenerbangan: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var daftarPenerbanganImage: UIRoundedImageView!
    @IBOutlet weak var daftarPenerbanganButton: UIButton!
    
    @IBOutlet weak var backgroundView: UIUpperRoundedImageView!
    @IBOutlet weak var bg: UILowerRoundedImageView!
    @IBOutlet weak var labelKeteranganIntensif: UILabel!
    @IBOutlet weak var labelBerat: UILabel!
    
    @IBOutlet weak var pickerBerat: UITextField!
    @IBOutlet weak var flightOriDestBooking: UIFlightOriDest!
    @IBOutlet weak var menu_burger: UIImageView!
    
    let model_data = ModBookingDetailVC()
    
    let weightGoodsPicker = UIPickerView()
    
    var _saveFlightCallbackToPass: (([String: Any]) -> ())? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restyleUIComponents()
        daftarPenerbanganImage.addTapGestureRecognizer{
            self.showPopupDataFlight()
        }
        backImage.addTapGestureRecognizer{
            self.navigationController?.popViewController(animated: true)
        }
        _saveFlightCallbackToPass = {
            datas in
            self.showDataFlight(dict: datas)
        }
        daftarPenerbanganButton.addTapGestureRecognizer{
            if self.model_data.baggage_space == nil {
                E.showUIUncomplete(vc: self, message: "Berat bagasi tersedia tidak boleh kosong", completion: nil)
                return
            }
            self.gotoKetentuanPenggunaAntarVC()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // TODO: check if data is available from web scrapper then invoke below
        if self.model_data.flight_code == nil {
            self.showPopupDataFlight()
        }
    }
    
    func restyleUIComponents(){
        flightOriDestBooking.isHidden = true
        labelKeteranganIntensif.text = ""
        labelEditPenerbangan.text = "Klik untuk mengedit\ndetail penerbangan"
        labelBerat.text = "Berat bagasi\nyang tersedia"
        pickerBerat.layer.masksToBounds = true
        pickerBerat.showArrow()
        pickerBerat.layer.cornerRadius = 6
        self.createWeightGoodsPicker()
        ViewController.setOLabel(button: daftarPenerbanganButton, text: "Daftar Penerbangan ini", filled: true, reverseColor: false)
        self.bg.createTwoHalfCircle(col: UIColor(rgb: 0xE2E2E2))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showDataFlight(dict: [String: Any]){
        let f_data = ModFlightOriDest()
        f_data.flight_code          = (dict[PopupDataFlightVC.KEY_KODE_PENERBANGAN]! as? String)!
        f_data.ori_airport_code     = (dict[PopupDataFlightVC.KEY_AIRPORT_ASAL]! as? String)!
        f_data.dest_airport_code    = (dict[PopupDataFlightVC.KEY_AIRPORT_TUJUAN]! as? String)!
        f_data.ori_date_time        = String((dict[PopupDataFlightVC.KEY_TANGGAL_WAKTU_BERANGKAT]! as? String)!
                                      .split(separator: " ")[0])
        f_data.ori_city             = (dict[PopupDataFlightVC.KEY_KOTA_ASAL]! as? String)!
        f_data.dest_city            = (dict[PopupDataFlightVC.KEY_KOTA_TUJUAN]! as? String)!
        f_data.is_complete          = true
        
        flightOriDestBooking.setupXib(modFlightOriDest: f_data)
        flightOriDestBooking.isHidden = false
        flightOriDestBooking.containerView.backgroundColor = backgroundView.backgroundColor
        
        model_data.flight_code              = dict[PopupDataFlightVC.KEY_KODE_PENERBANGAN]! as? String
        model_data.origin_airport           = dict[PopupDataFlightVC.KEY_AIRPORT_ASAL]! as? String
        model_data.destination_airport      = dict[PopupDataFlightVC.KEY_AIRPORT_TUJUAN]! as? String
        model_data.origin_city              = dict[PopupDataFlightVC.KEY_KOTA_ASAL]! as? String
        model_data.destination_city         = dict[PopupDataFlightVC.KEY_KOTA_TUJUAN]! as? String
        model_data.booking_code             = dict[PopupDataFlightVC.KEY_KODE_BOOKING]! as? String
        model_data.departure_date_time      = dict[PopupDataFlightVC.KEY_TANGGAL_WAKTU_BERANGKAT]! as? String
        model_data.id_origin_airport        = dict[PopupDataFlightVC.KEY_ID_AIRPORT_ASAL]! as? Int
        model_data.id_destination_airport   = dict[PopupDataFlightVC.KEY_ID_AIRPORT_TUJUAN]! as? Int
        
        let tipster_price_str               = dict[PopupDataFlightVC.KEY_TIPSTER_PRICE]! as? String
        model_data.tipster_price            = Double(tipster_price_str!)
        
        self.setIntensifLabelData()
    }
    
    func setIntensifLabelData(){
        if(model_data.tipster_price == nil || model_data.baggage_space == nil){
            return
        }
        
        let tipster_price   = Int(model_data.tipster_price!)
        let bag_space       = Int(model_data.baggage_space!)
        let total_money     = tipster_price * bag_space
        
        let per_kg = Double(tipster_price).currencyIDR
        let total_kg = Double(total_money).currencyIDR
        labelKeteranganIntensif.text = "TIPS Credit per kg: \(per_kg)\nTotal TIPS Credit yang bisa didapatkan:\n\(total_kg)"
    }
    
    func createWeightGoodsPicker(){
        weightGoodsPicker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AntarVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AntarVC.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerBerat.inputView = weightGoodsPicker
        pickerBerat.inputAccessoryView = toolBar
    }
    
    @objc func donePicker (sender:UIBarButtonItem){
        pickerBerat.resignFirstResponder()
        let weight_string = S.goods_weight_list_delivery[weightGoodsPicker.selectedRow(inComponent: 0)].value?.description
        model_data.baggage_space = Int(weight_string!)
        setIntensifLabelData()
        pickerBerat.text = weight_string! + " kg"
    }
    
    @objc func cancelPicker (sender:UIBarButtonItem){
        pickerBerat.resignFirstResponder()
//        if model_data.kode_airport == nil {
//            kodeAirportField.text = ""
//        }
    }
    
    func showPopupDataFlight(){
        let storyboard = UIStoryboard(name: "Antar", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PopupDataFlightVCID") as! PopupDataFlightVC
        // self.present(vc, animated: true, completion: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.saveFlightCallback = _saveFlightCallbackToPass
        self.present(vc, animated: true, completion: nil)
    }
    
    func gotoKetentuanPenggunaAntarVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "KetentuanPenggunaAntarVCID") as! KetentuanPenggunaAntarVC
        vc.model_data = self.model_data
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension BookingDetailVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return S.goods_weight_list_delivery.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (S.goods_weight_list_delivery[row].value?.description)! + " kg"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}

class ModBookingDetailVC{
    public var departure_date_time: String? //
    public var flight_code: String? //
    public var booking_code: String? //
    public var id_origin_airport: Int? //
    public var id_destination_airport: Int? //
    public var origin_airport: String? //
    public var destination_airport: String? //
    public var origin_city: String? //
    public var destination_city: String? //
    public var tipster_price: Double? //

    public var baggage_space: Int?
}
