//
//  PopupDataFlightVC.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 15/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import DatePickerDialog
import Alamofire

class PopupDataFlightVC: KirimVCFamily{
    @IBOutlet weak var modalBackground: UIView!
    @IBOutlet weak var blueArea: UIView!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var kodePenerbanganField: TextField!
    @IBOutlet weak var airportAsalField: TextField!
    @IBOutlet weak var airportTujuanField: TextField!
    @IBOutlet weak var tanggalBerangkatField: TextField!
    @IBOutlet weak var waktuBerangkatField: TextField!
    
    let model_data = ModPopupDataFlightVC()
    
    var saveFlightCallback: (([String: Any]) -> ())? = nil
    
    let airportAsalPicker = UIPickerView()
    let airportTujuanPicker = UIPickerView()
    
    
    static let KEY_KODE_BOOKING: String = "KEY_KODE_BOOKING"
    static let KEY_KODE_PENERBANGAN: String = "KODE_PENERBANGAN"
    static let KEY_AIRPORT_ASAL: String = "AIRPORT_ASAL"
    static let KEY_AIRPORT_TUJUAN: String = "AIRPORT_TUJUAN"
    static let KEY_ID_AIRPORT_ASAL: String = "ID_AIRPORT_ASAL"
    static let KEY_ID_AIRPORT_TUJUAN: String = "ID_AIRPORT_TUJUAN"
    static let KEY_KOTA_ASAL: String = "KOTA_ASAL"
    static let KEY_KOTA_TUJUAN: String = "KOTA_TUJUAN"
    static let KEY_TIPSTER_PRICE: String = "TIPSTER_PRICE"
    static let KEY_TANGGAL_WAKTU_BERANGKAT: String = "KEY_TANGGAL_WAKTU_BERANGKAT"
    
    var airportAsalList     : [SAirport] = []
    var airportTujuanList   : [SAirport] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restyleUIComponents()
        blueArea.addTapGestureRecognizer{
            self.backStack()
        }
        modalBackground.addTapGestureRecognizer{
            self.dismissKeyboard()
            return
        }
        tanggalBerangkatField.addTapGestureRecognizer{
            self.showDatePicker()
        }
        waktuBerangkatField.addTapGestureRecognizer{
            self.showTimePicker()
        }
        cancelButton.addTapGestureRecognizer{
            self.backStack()
        }
        saveButton.addTapGestureRecognizer{

            self.model_data.kode_penerbangan = self.kodePenerbanganField.text
            
            if !self.validateFieldOK(views: [
                self.model_data.airport_asal,
                self.model_data.airport_tujuan,
                self.model_data.kode_penerbangan,
                self.model_data.tanggal_berangkat,
                self.model_data.waktu_berangkat
                ]){
                self.showErrorTextField()
                // do something
                return
            }
            
            self.hideErrorTextField()
            self.save()
        }
    }
    
    func backWithSuccess(booking_data: [String: Any]? = nil){
        if booking_data == nil {
            print("Error: no booking data served")
            return
        }
        
        print(booking_data?.description ?? "")
        
        let airport_asal            = booking_data!["origin_airport"]       as? [String: Any]
        let airport_tujuan          = booking_data!["destination_airport"]  as? [String: Any]
        let kodeAirportAsal         = airport_asal!["initial_code"]         as? String
        let kodeAirportTujuan       = airport_tujuan!["initial_code"]       as? String
        let idAirportAsal           = airport_asal!["id"]                   as? Int
        let idAirportTujuan         = airport_tujuan!["id"]                 as? Int
        let kodePenerbangan         = booking_data!["flight_code"]          as? String
        let kodeBooking             = booking_data!["booking_code"]         as? String
        let tipsterPrice            = booking_data!["tipster_price"]        as? String
        let kotaAsal                = booking_data!["origin_city"]          as? String
        let kotaTujuan              = booking_data!["destination_city"]     as? String
        let tanggalWaktuBerangkat   = booking_data!["depature"]             as? String
        
        if(self.saveFlightCallback != nil){
            self.saveFlightCallback!([
                PopupDataFlightVC.KEY_KODE_BOOKING: kodeBooking!,
                PopupDataFlightVC.KEY_KODE_PENERBANGAN: kodePenerbangan!,
                PopupDataFlightVC.KEY_AIRPORT_ASAL: kodeAirportAsal!,
                PopupDataFlightVC.KEY_AIRPORT_TUJUAN: kodeAirportTujuan!,
                PopupDataFlightVC.KEY_ID_AIRPORT_ASAL: idAirportAsal!,
                PopupDataFlightVC.KEY_ID_AIRPORT_TUJUAN: idAirportTujuan!,
                PopupDataFlightVC.KEY_KOTA_ASAL: kotaAsal!,
                PopupDataFlightVC.KEY_KOTA_TUJUAN: kotaTujuan!,
                PopupDataFlightVC.KEY_TANGGAL_WAKTU_BERANGKAT: tanggalWaktuBerangkat!,
                PopupDataFlightVC.KEY_TIPSTER_PRICE: tipsterPrice!,
                ])
            self.backStack()
        }
    }
    
    func save(){
        model_data.kode_penerbangan = kodePenerbanganField.text
        
        let sv = UIViewController.displaySpinner(onView: self.view)
        connCreateFlight { (response) in
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, title: "Error", completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            self.backWithSuccess(booking_data: result["booking"] as? [String : Any])
            UIViewController.removeSpinner(spinner: sv)
        }
    }
    
    func connCreateFlight(callback: @escaping (DataResponse<Any>) -> Void){
        let bookingParam: Parameters = [
            "booking_code": "Data booking tidak ditemukan",
            "flight_code": model_data.kode_penerbangan!,
            "code_origin": model_data.airport_asal!,
            "code_destination": model_data.airport_tujuan!,
            "date_origin": model_data.tanggal_berangkat! + " " + model_data.waktu_berangkat!
        ]
        
        Alamofire.request(FlightRouter.create_flight(parameters: bookingParam)).responseJSON(completionHandler: callback)
    }
    
    func restyleUIComponents(){
        ViewController.setOLabel(button: saveButton, text: "SAVE", filled: true, reverseColor: true)
        ViewController.setOLabel(button: cancelButton, text: "CANCEL", filled: true, reverseColor: false)
        
        kodePenerbanganField.layer.cornerRadius = 6
        kodePenerbanganField.setUpperCase()
        airportAsalField.layer.cornerRadius = 6
        airportTujuanField.layer.cornerRadius = 6
        tanggalBerangkatField.layer.cornerRadius = 6
        waktuBerangkatField.layer.cornerRadius = 6
        airportAsalField.showArrow()
        airportTujuanField.showArrow()
        tanggalBerangkatField.showCalendar()
        waktuBerangkatField.showClock()
        
        modalBackground.layer.cornerRadius = 10
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        createAirportAsalPicker()
        createAirportTujuanPicker()
    }
    
    
    func showDatePicker() {
        let date6month = Calendar.current.date(byAdding: .month, value: 6, to: Date())
        DatePickerDialog(locale: Locale(identifier: "id")).show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", maximumDate: date6month, datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                let tanggal_string = formatter.string(from: dt)
                self.tanggalBerangkatField.text = tanggal_string
                
                formatter.dateFormat = "yyyy-MM-dd"
                self.model_data.tanggal_berangkat = formatter.string(from: dt)
                self.tanggalBerangkatField.hideError()
            }
        }
    }
    
    func showTimePicker() {
        DatePickerDialog(locale: Locale(identifier: "id")).show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"
                let waktu_string = formatter.string(from: dt)
                self.waktuBerangkatField.text = waktu_string
                self.model_data.waktu_berangkat = waktu_string
                self.waktuBerangkatField.hideError()
            }
        }
    }
    
    func createAirportAsalPicker(){
        airportAsalList.removeAll(keepingCapacity: false)
        for s in S.airport_list {
            airportAsalList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: airportAsalPicker, inputField: airportAsalField,
                          action_done: #selector(PopupDataFlightVC.doneAirportAsalPicker),
                          action_cancel: #selector(PopupDataFlightVC.cancelAirportTujuanPicker))
    }
    
    func createAirportTujuanPicker(){
        airportTujuanList.removeAll(keepingCapacity: false)
        for s in S.airport_list {
            airportTujuanList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: airportTujuanPicker, inputField: airportTujuanField,
                          action_done: #selector(PopupDataFlightVC.doneAirportTujuanPicker),
                          action_cancel: #selector(PopupDataFlightVC.cancelAirportTujuanPicker))
    }
    
    @objc func doneAirportAsalPicker(){
        //
        let airport = airportAsalList[airportAsalPicker.selectedRow(inComponent: 0)]
        self.model_data.airport_asal = airport.initial_code
        airportAsalField.text = airport.initial_code! + " - " + airport.name!
        airportAsalField.resignFirstResponder()
    }
    
    @objc func cancelAirportAsalPicker(){
        airportAsalField.resignFirstResponder()
        //
        if self.model_data.airport_asal == nil {
            airportAsalField.text = ""
        }
    }
    
    @objc func doneAirportTujuanPicker(){
        //
        let airport = airportTujuanList[airportTujuanPicker.selectedRow(inComponent: 0)]
        self.model_data.airport_tujuan = airport.initial_code
        airportTujuanField.text = airport.initial_code! + " - " + airport.name!
        airportTujuanField.resignFirstResponder()
    }
    
    @objc func cancelAirportTujuanPicker(){
        airportTujuanField.resignFirstResponder()
        //
        if self.model_data.airport_tujuan == nil {
            airportTujuanField.text = ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension PopupDataFlightVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == airportAsalPicker){
            return airportAsalList.count
        }
        
        if(pickerView == airportTujuanPicker){
            return airportTujuanList.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == airportAsalPicker){
            return airportAsalList[row].initial_code! + " - " + airportAsalList[row].name!
        }
        
        if(pickerView == airportTujuanPicker){
            return airportTujuanList[row].initial_code! + " - " + airportTujuanList[row].name!
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == airportAsalPicker){
            //self.airportAsalField.text = self.airportAsalList[row].name
        }
        
        if(pickerView == airportTujuanPicker){
            //self.airportTujuanField.text = self.airportTujuanList[row].name
        }
    }
}

class ModPopupDataFlightVC{
    var kode_penerbangan: String?
    var airport_asal: String?
    var airport_tujuan: String?
    var tanggal_berangkat: String?
    var waktu_berangkat: String?
}
