//
//  AntarSuksesDrawerVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/10/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import KYDrawerController

class AntarDrawerVC: KYDrawerController {

    var completion: (() -> ())?
    @IBOutlet weak var burger_menu: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawerDirection = .right
        
        if self.drawerViewController is SideDrawerMenuVC {
            let dvc = self.drawerViewController as! SideDrawerMenuVC
            dvc.parentdrawer = self
        }
        
        self.burger_menu.addTapGestureRecognizer {
            self.setDrawerState(.opened, animated: true)
        }
        let vc = self.mainViewController as! GeneralNAVC
        vc.listGesturesStackCan = [
            2 // only on second stack navigation
        ]
        vc.completion = self.completion
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
