//
//  KetentuanPenggunaAntarVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 26/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class KetentuanPenggunaAntarVC: UIViewController {
    
//    @IBOutlet weak var backImage: UIImageView!
//    @IBOutlet weak var containerView: UIView!
//    @IBOutlet weak var toggleSetuju: UIToggleCheck!
//    @IBOutlet weak var textScroll: UITextView!
//    @IBOutlet weak var buttonBatal: UIButton!
//    @IBOutlet weak var buttonSetuju: UIButton!
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var toggleSetuju: UIToggleCheck!
    @IBOutlet weak var textScroll: UILabel!
    
    @IBOutlet weak var buttonBatal: UIButton!
    @IBOutlet weak var buttonSetuju: UIButton!
    
    
    var model_data = ModBookingDetailVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.ambilSyaratKirim()
        self.backImage.addTapGestureRecognizer { self.navigationController?.popViewController(animated: true) }
        self.buttonBatal.addTapGestureRecognizer { self.navigationController?.popViewController(animated: true) }
        self.buttonSetuju.addTapGestureRecognizer {
            if !self.toggleSetuju.getToggleState() {
                E.showUIUncomplete(vc: self, message: "Silahkan centang persetujuan terlebih dahulu", completion: nil)
                // do something
                return
            }
            
            self.kirim_dan_selesai()
        }
        // Do any additional setup after loading the view.
    }
    
    func setupLayout(){
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        self.toggleSetuju.setupXib(text: "Telah membaca dan menyetujui syarat & ketentuan", state: false)
        ViewController.setOLabel(button: self.buttonSetuju, text: "Setuju", filled: true, reverseColor: false)
        ViewController.setOLabel(button: self.buttonBatal, text: "Batal", filled: false, reverseColor: false)
    }
    
    func connGetTerms(callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "type": "antar"
        ]
        
        Alamofire.request(HomeRouter.terms_n_conds(parameters: param))
            .responseJSON(completionHandler: callback)
    }
    
    func ambilSyaratKirim(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connGetTerms(callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            // print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.parseData(data: result["value"] as? [String])
        })
    }
    
    
    func parseData(data: [String]? = nil) {
        if data == nil {
            print("Error: no result.value data served")
            return
        }
        
        let text = data!.joined().htmlToAttributedString
        print(text)
        self.textScroll.attributedText = text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension KetentuanPenggunaAntarVC {
    
    func gotoAntarSukses(data: ModAntarSuksesVC){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AntarSuksesVCID") as! AntarSuksesVC
        vc.model_data = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func kirim_dan_selesai(){
        
        let sv = UIViewController.displaySpinner(onView: self.view)
        connPostDelivery { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.gotoAntarSukses(data: self.parseAntarSuksesModelVC(slot: result["slot"] as? [String: Any]))
        }
    }
    
    func parseAntarSuksesModelVC(slot: [String: Any]? = nil) -> ModAntarSuksesVC {
        if slot == nil {
            print("Error: no result.slot data served")
            return ModAntarSuksesVC()
        }
        
        let a                   = ModAntarSuksesVC()
        a.kodeBooking           = slot!["slot_id"] as? String
        
        let origin_airport      = slot!["origin_airport"] as? [String: Any]
        let destination_airport = slot!["destination_airport"] as? [String: Any]
        
        let fod = ModFlightOriDest()
        fod.dest_airport_code   = (destination_airport!["initial_code"]! as? String)!
        fod.dest_city           = (slot!["destination_city"] as? String)!
        fod.flight_code         = (slot!["flight_code"] as? String)!
        fod.ori_airport_code    = (origin_airport!["initial_code"]! as? String)!
        fod.ori_city            = (slot!["origin_city"] as? String)!
        fod.ori_date_time       = (slot!["depature"] as? String)!
        fod.is_complete         = true
        
        a.flightOriDest         = fod
        
        return a
    }
    
    func connPostDelivery(callback: @escaping (DataResponse<Any>) -> Void){
        let antarParam: Parameters = [
            "flight_code": model_data.flight_code!,
            "booking_code": model_data.booking_code!,
            "origin_airport": model_data.origin_airport!,
            "destination_airport": model_data.destination_airport!,
            "id_origin_airport": model_data.id_origin_airport!,
            "id_destination_airport": model_data.id_destination_airport!,
            "origin_city": model_data.origin_city!,
            "destination_city": model_data.destination_city!,
            "tipster_price": model_data.tipster_price!,
            "depature": model_data.departure_date_time!,
            "baggage_space": model_data.baggage_space!,
            "id_member": HomeVC.user.id!,
            "first_name": AntarVC.model_data.nama_depan,
            "last_name": AntarVC.model_data.nama_belakang
        ]
        
        print(antarParam.description)
        
        Alamofire.request(AntarRouter.submit_antar(parameters: antarParam)).responseJSON(completionHandler: callback)
    }
}
