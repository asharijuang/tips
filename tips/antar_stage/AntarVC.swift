//
//  AntarVC.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 14/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import DatePickerDialog
import Alamofire

class AntarVC: KirimVCFamily{
    
    var airportList: [SAirport] = []
    let apPicker = UIPickerView()
    static var model_data = ModAntarVC()
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var buttonLanjut: UIButton!
    @IBOutlet weak var backgroundCard: UIView!
    @IBOutlet weak var namaDepanField: TextField!
    @IBOutlet weak var namaBelakangField: TextField!
    @IBOutlet weak var kodeBookingField: TextField!
    @IBOutlet weak var dateField: TextField!
    @IBOutlet weak var kodeAirportField: TextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        restyleUIComponents()
        buttonLanjut.addTapGestureRecognizer{
            
            AntarVC.model_data.nama_depan      = self.namaDepanField.text!
            AntarVC.model_data.nama_belakang   = self.namaBelakangField.text!
            AntarVC.model_data.kode_booking    = self.kodeBookingField.text!
            
            if !self.validateFieldOK(views: [
                AntarVC.model_data.nama_depan,
                AntarVC.model_data.nama_belakang,
                AntarVC.model_data.kode_booking,
                AntarVC.model_data.tanggal_penerbangan,
                AntarVC.model_data.kode_airport
                ]){
                self.showErrorTextField()
                print(AntarVC.model_data.nama_depan)
                print(AntarVC.model_data.nama_belakang)
               
                print(AntarVC.model_data.kode_booking)
                print(AntarVC.model_data.tanggal_penerbangan); print(AntarVC.model_data.kode_airport)
                // do something
                return
            }
            
            self.hideErrorTextField()
            self.lanjut()
        }
        dateField.addTapGestureRecognizer{
            self.datePickerTapped()
        }
        backImage.addTapGestureRecognizer{
            self.backStack()
            AntarVC.model_data = ModAntarVC()
        }
    }
    
    func lanjut(){
        
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connCheckFlight { (response) in
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            print(result)
            UIViewController.removeSpinner(spinner: sv)
            
            // next
            self.gotoAntarBookingDetail()
        }
    }
    
    func connCheckFlight(callback: @escaping (DataResponse<Any>) -> Void){
        let bookingParam: Parameters = [
            "nama_depan": AntarVC.model_data.nama_depan,
            "nama_belakang": AntarVC.model_data.nama_belakang,
            "booking_code": AntarVC.model_data.kode_booking,
            "booking_date": AntarVC.model_data.tanggal_penerbangan,
            "kode_airport": AntarVC.model_data.kode_airport
        ]
        
        print(bookingParam)
        
        Alamofire.request(FlightRouter.check_flight(parameters: bookingParam)).responseJSON(completionHandler: callback)
    }
    
    func restyleUIComponents(){
        backgroundCard.layer.cornerRadius = 10
        namaDepanField.layer.cornerRadius = 6
        namaBelakangField.layer.cornerRadius = 6
        kodeBookingField.layer.cornerRadius = 6
        kodeBookingField.setMaxInputLength(max: 6)
        kodeBookingField.setUpperCase()
        dateField.layer.cornerRadius = 6
        dateField.showCalendar()
        kodeAirportField.layer.cornerRadius = 6
        kodeAirportField.showArrow()
        ViewController.setOLabel(button: buttonLanjut, text: "Lanjut", filled: true, reverseColor: true)
        self.createAirportPicker()
        
        self.namaDepanField.text = HomeVC.user.first_name
        self.namaBelakangField.text = HomeVC.user.last_name
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func datePickerTapped() {
        DatePickerDialog(locale: Locale(identifier: "id")).show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                let date_string = formatter.string(from: dt)
                self.dateField.text = date_string
                
                formatter.dateFormat = "yyyy-MM-dd"
                AntarVC.model_data.tanggal_penerbangan = formatter.string(from: dt)
            }
        }
    }
    
    @objc func donePicker (sender:UIBarButtonItem){
        kodeAirportField.resignFirstResponder()
        kodeAirportField.hideError()
        kodeAirportField.text = airportList[apPicker.selectedRow(inComponent: 0)].initial_code!
                                + " - "
                                + airportList[apPicker.selectedRow(inComponent: 0)].name!
        AntarVC.model_data.kode_airport = airportList[apPicker.selectedRow(inComponent: 0)].initial_code!
    }
    
    @objc func cancelPicker (sender:UIBarButtonItem){
        kodeAirportField.resignFirstResponder()
        
        if AntarVC.model_data.kode_airport == "" {
            kodeAirportField.text = ""
        }
    }
    
    func gotoAntarBookingDetail(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetailVCID") as! BookingDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AntarVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func createAirportPicker(){
        airportList.removeAll(keepingCapacity: false)
        for data in S.airport_list{
            airportList.append(data)
        }
        self.assignPicker(delegateTo: self, pickerView: apPicker, inputField: kodeAirportField,
                          action_done: #selector(AntarVC.donePicker),
                          action_cancel: #selector(AntarVC.cancelPicker))
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return airportList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return airportList[row].initial_code! + " - " + airportList[row].name!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}

extension AntarVC {
}

class ModAntarVC{
    public var nama_depan: String = ""
    public var nama_belakang: String = ""
    public var kode_booking: String = ""
    public var tanggal_penerbangan: String = ""
    public var kode_airport: String = ""
}
