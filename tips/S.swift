//
//  S.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 18/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation

class S{
    static var airport_list                 : [SAirport]            = []
    static var statuses_delivery            : [SStatusShipmentDelivery] = []
    static var statuses_shipment            : [SStatusShipmentDelivery] = []
    static var goods_weight_list_shipment   : [SWeightInt]          = []
    static var goods_weight_list_delivery   : [SWeightInt]          = []
    static var airport_city_list            : [SCity]               = []
    static var airport_city_price_list      : [SCityPrice]          = []
    static var location_province            : [SLocProvince]        = []
    static var location_city                : [SLocCity]            = []
    static var location_subdistrict         : [SLocSubdistrict]     = []
    static var price_goods_estimate         : [SPriceGoodsEstimate] = []
    static var payment_method               : [SPaymentMethod]      = []
    static var insurance                    : SInsurance            = SInsurance()
    static var referral                     : SReferral?            = nil
    static var etc_message                  : String                = ""
    
    public static func getEstimateGoodsValue(id: Int?) -> SPriceGoodsEstimate? {
        for egv in S.price_goods_estimate {
            if egv.id == id {
                return egv
            }
        }
        
        return nil
    }
    
    public static func getPrice(id_origin: Int, id_destination: Int) -> SCityPrice? {
        for price in S.airport_city_price_list {
            if price.id_origin == id_origin && price.id_destination == id_destination{
                return price
            }
        }
        
        return nil
    }
    
    public static func getLocationCity(id_province: Int) -> [SLocCity] {
        var cities: [SLocCity] = []
        for loc_city in S.location_city {
            if loc_city.id_province == id_province {
                cities.append(loc_city)
            }
        }
        
        return cities
    }
    
    public static func getLocationSubdistrict(id_city: Int) -> [SLocSubdistrict] {
        var subdistricts: [SLocSubdistrict] = []
        for loc_subdistrict in S.location_subdistrict {
            if loc_subdistrict.id_city == id_city {
                subdistricts.append(loc_subdistrict)
            }
        }
        
        return subdistricts
    }
    
    public static func getProvinceById(id: Int) -> SLocProvince {
        for p in location_province {
            if p.id == id {
                return p
            }
        }
        
        return SLocProvince()
    }
    
    public static func getCityById(id: Int) -> SLocCity {
        for p in location_city {
            if p.id == id {
                return p
            }
        }
        
        return SLocCity()
    }
    
    public static func getSubdistrictById(id: Int) -> SLocSubdistrict {
        for p in location_subdistrict {
            if p.id == id {
                return p
            }
        }
        
        return SLocSubdistrict()
    }
}

class SAirport{
    public var id: Int?;
    public var initial_code: String?
    public var name: String?
    public var id_city: Int?
    public var city_name: String?
}

class SWeightInt{
    public var value: Int?
    
    init(x: Int){
        self.value = x
    }
}

class SCity{
    public var id: Int?
    public var name: String?
}

class SStatusShipmentDelivery{
    public var id: Int?
    public var step: Int?
    public var description: String?
}

class SCityPrice{
    public var id_origin: Int?
    public var id_destination: Int?
    public var gold: Int?
    public var regular: Int?
}

class SLocProvince{
    public var id: Int?
    public var name: String?
}

class SLocCity{
    public var id: Int?
    public var name: String?
    public var id_province: Int?
    public var id_airportcity: Int?
}

class SLocSubdistrict{
    public var id: Int?
    public var id_city: Int?
    public var id_province: Int?
    public var name: String?
}

class SPriceGoodsEstimate{
    public var id: Int?
    public var price_estimate: String?
    public var nominal: Int?
}

class SPaymentMethod{
    public var bank_code: String?
    public var product_code: String?
    public var product_name: String?
    public var is_cash: Bool?
}

class SInsurance{
    public var default_insurance: Float?
    public var additional_insurance: Float?
}

class SReferral{
    public var referral_amount: Double?
    public var referred_amount: Double?
}
