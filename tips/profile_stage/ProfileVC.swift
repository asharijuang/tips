//
//  ProfileVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 30/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import DatePickerDialog
import SDWebImage
import Alamofire

class ProfileVC: XUIViewController {

    @IBOutlet weak var containerField: UIView!
    @IBOutlet weak var backButton: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var editPhotoButton: UILabel!
    @IBOutlet weak var namaDepanField: TextField!
    @IBOutlet weak var namaBelakangField: TextField!
    @IBOutlet weak var emailField: TextField!
    @IBOutlet weak var noHpField: TextField!
    @IBOutlet weak var tanggalLahirField: TextField!
    @IBOutlet weak var newPasswordField: TextField!
    @IBOutlet weak var confNewPasswordField: TextField!
    @IBOutlet weak var buttonBatal: UIButton!
    @IBOutlet weak var buttonSimpan: UIButton!
    @IBOutlet weak var jkField: TextField!
    
    let imagePicker = UIImagePickerController()
    var dataImage: UIImage?
    
    let sexPicker      = UIPickerView()
    var sexList        : [String] = ["L", "P"]
    
    public static var hasProfileChange: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTapGestureRecognizer {
            self.backStack()
        }
        buttonBatal.addTapGestureRecognizer {
            self.backStack()
        }
        tanggalLahirField.addTapGestureRecognizer {
            self.datePickerTapped()
        }
        editPhotoButton.addTapGestureRecognizer {
            self.showActionSheet(sender: self.editPhotoButton, mainTitle: "Foto Profile", titleA: "Take Photo", actionA: {
                self.imagePicker.sourceType = .camera
                self.imagePicker.delegate =  self
                self.present(self.imagePicker, animated: true, completion: nil)
            }, titleB: "Choose from Gallery", actionB: {
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.delegate =  self
                self.present(self.imagePicker, animated: true, completion: nil)
            })
        }
        buttonSimpan.addTapGestureRecognizer {
            let nohp = self.noHpField.text
            let ndep = self.namaDepanField.text
            let nbel = self.namaBelakangField.text
            let emil = self.emailField.text
            let seex = self.jkField.text
            let tglh = self.tanggalLahirField.text
            let pasw = self.newPasswordField.text
            let cpas = self.confNewPasswordField.text
            if !self.isPasswordOK(p1: pasw, p2: cpas) {
                E.showUIError(vc: self, message: "Password pertama dan kedua harus sama")
            }
            self.updateProfile(image: self.dataImage, noHP: nohp, namDep: ndep, namBel: nbel,
                               email: emil, sex: seex, tglLahir: tglh, password: pasw)
        }
        self.restyleUIComponents()

        // Do any additional setup after loading the view.
    }
    
    func isPasswordOK(p1: String?, p2: String?) -> Bool {
        if p1 == nil && p2 == nil { return true }
        if p1 == nil && p2 != nil { return false }
        if p2 == nil && p1 != nil { return false }
        if p1 != nil && p2 != nil {
            if p1!.isEmpty && p2!.isEmpty { return true }
            if p1!.isEmpty && !p2!.isEmpty { return false }
            if p2!.isEmpty && !p1!.isEmpty { return false }
            if p1!.count != p2!.count { return false }
        }
        
        return true
    }
    
    func restyleUIComponents(){
        containerField.layer.cornerRadius = 10
        namaDepanField.layer.cornerRadius = 6
        namaBelakangField.layer.cornerRadius = 6
        emailField.layer.cornerRadius = 6
        noHpField.layer.cornerRadius = 6
        tanggalLahirField.layer.cornerRadius = 6
        newPasswordField.layer.cornerRadius = 6
        confNewPasswordField.layer.cornerRadius = 6
        ViewController.setOLabel(button: buttonBatal, text: "Batal", filled: false, reverseColor: false)
        ViewController.setOLabel(button: buttonSimpan, text: "Simpan", filled: true, reverseColor: false)
        newPasswordField.isSecureTextEntry = true
        confNewPasswordField.isSecureTextEntry = true
        jkField.layer.cornerRadius = 6
        jkField.showArrow()
        self.createSexPicker()
        
        noHpField.setInputPhoneNumber()
        
        namaDepanField.text         = HomeVC.user.first_name!
        namaBelakangField.text      = HomeVC.user.last_name!
        emailField.text             = HomeVC.user.email!
        noHpField.text              = HomeVC.user.mobile_phone_no!
        jkField.text                = HomeVC.user.sex ?? ""
        
        if HomeVC.user.birth_date != nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let date_birth = formatter.date(from: HomeVC.user.birth_date!)
            formatter.dateFormat = "dd-MM-yyyy"
            let date_string = formatter.string(from: date_birth!)
            tanggalLahirField.text      = HomeVC.user.birth_date == nil
                                          ? ""
                                          : date_string.description
        }
        
        if HomeVC.user.profil_picture != nil {
            profileImage.sd_setImage(with: URL(string: HomeVC.user.profil_picture!)) { (image, err, sdicach, url) in
                // self.profileImage.image = image.
                self.profileImage.image = ViewController.maskCircleImage(image: image!)
                self.profileImage.contentMode = .scaleAspectFill
            }
        }
    }
    
    func datePickerTapped() {
        DatePickerDialog(locale: Locale(identifier: "id")).show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                let date_string = formatter.string(from: dt)
                self.tanggalLahirField.text = date_string
                
                formatter.dateFormat = "yyyy-MM-dd"
                formatter.string(from: dt)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func doneSexPicker(){
        //
        let selected_row    = sexPicker.selectedRow(inComponent: 0)
        let sex             = sexList[selected_row]
        
        jkField.text = sex
        jkField.resignFirstResponder()
    }
    
    @objc func cancelSexPicker(){
        //
        jkField.resignFirstResponder()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ProfileVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func createSexPicker(){
        self.assignPicker(delegateTo: self, pickerView: sexPicker, inputField: jkField,
                          action_done: #selector(ProfileVC.doneSexPicker),
                          action_cancel: #selector(ProfileVC.cancelSexPicker))
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sexList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sexList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}

extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController,
                                     didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        self.dataImage = image
        self.profileImage.image = ViewController.maskCircleImage(image: image!)
    }
    
    func updateProfile(image: UIImage?, noHP: String?, namDep: String?, namBel: String?,
                       email: String?, sex: String?, tglLahir: String?, password: String?){
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if image != nil {
                    multipartFormData.append(UIImageJPEGRepresentation(image!, CGFloat(0.5))!, withName: "profil_picture", fileName: "sample.jpg", mimeType: "image/pg")
                }
                if noHP != nil {
                    multipartFormData.append(noHP!.data(using: .utf8)!, withName: "mobile_phone_no")
                }
                if namDep != nil {
                    multipartFormData.append(namDep!.data(using: .utf8)!, withName: "first_name")
                }
                if namBel != nil {
                    multipartFormData.append(namBel!.data(using: .utf8)!, withName: "last_name")
                }
                if email != nil {
                    multipartFormData.append(email!.data(using: .utf8)!, withName: "email")
                }
                if sex != nil {
                    multipartFormData.append(sex!.data(using: .utf8)!, withName: "sex")
                }
                if tglLahir != nil {
                    multipartFormData.append(tglLahir!.data(using: .utf8)!, withName: "birth_date")
                }
                if password != nil {
                    multipartFormData.append(password!.data(using: .utf8)!, withName: "password")
                }
                multipartFormData.append(String(HomeVC.user.id!).data(using: .utf8)!, withName: "member_id")
        },
            to: String(AntarRouter.baseURLString) + "/profile/update",
            method: .post,
            headers: ["app-kind": "ios"],
            encodingCompletion: { encodingResult in
                print(encodingResult)
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { json_data in
                        
                        print(json_data)
                        let result = json_data.result.value as? [String: Any]
                        let user = LoginVC.parseUserLoginData(data: result)
                        if user != nil {
                            print("user data OK")
                            ProfileVC.hasProfileChange = true
                            HomeVC.user = user!
                            MasterDB().insertSingleUser(user: result!)
                        }
                        
                        UIViewController.removeSpinner(spinner: sv)
                        let completeMessage = "Profile berhasil diubah"
                        E.showUISuccess(vc: self, message: completeMessage, completion: {
                            self.backStack()
                        });
                        
                        // fucking stupid API response
                        return
                        
                        // check if response is contains error or no result
                        if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                            E.showUIError(vc: self, completion: {
                                UIViewController.removeSpinner(spinner: sv)
                            })
                            return
                        }
                        
                        // we dont care with the result
                        UIViewController.removeSpinner(spinner: sv)
                        E.showUIError(vc: self, message: completeMessage, completion: {
                            self.backStack()
                        });
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    UIViewController.removeSpinner(spinner: sv)
                    print(encodingError)
                }
        }
        )
    }
}
