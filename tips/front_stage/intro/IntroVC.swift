//
//  IntroVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 11/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class IntroVC: XUIViewController, UIScrollViewDelegate {

    @IBOutlet weak var labelSkipFinish: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.labelSkipFinish.addTapGestureRecognizer {
            C().setupFirstTime()
            self.backStack()
        }

        self.scrollView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(self.scrollView.contentOffset.x / self.scrollView.frame.size.width)
        
        self.pageControl.currentPage = Int(pageNumber)
        self.labelSkipFinish.text = pageNumber == 2 ? "Finish" : "Skip"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
