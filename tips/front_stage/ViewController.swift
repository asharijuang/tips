//
//  ViewController.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Foundation

class ViewController: XUIViewController {

    static var isStackNextVCActive = false
    @IBOutlet weak var teksBerbagi: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var labelSampai: UILabel!
    @IBOutlet weak var teks1: UILabel!
    @IBOutlet weak var teks2: UILabel!
    @IBOutlet weak var teks3: UILabel!
    @IBOutlet weak var teks4: UILabel!
    @IBOutlet weak var teks5: UILabel!
    @IBOutlet weak var teksSudahPunyaAkun: UILabel!
    @IBOutlet weak var teksBelumPunyaAkun: UILabel!
    
    static let PRIMARY_COLOR: Int = 0x417DBC
    static let WHITE_COLOR: Int = 0xFFFFFF
    
    static let RED_COLOR: Int = 0xCC3355
    static let GRAY_COLOR: Int = 0xAAAAAA
    
    static var general_view_controller_width: CGFloat = CGFloat(0)
    
    static var special_drawer_width: CGFloat? = nil
    
    @IBOutlet weak var viewDebug: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        ViewController.setOLabel(button: leftButton, text: "SIGN IN", filled: true)
        ViewController.setOLabel(button: rightButton, text: "SIGN UP", filled: false)
        labelSampai.addTapGestureRecognizer {
            // TODO: only for testing
            
            if !AppDelegate.modeProduction {
                self.test()
            }
        }
        
        MasterDB().createAllTables()
        MasterDB().resetShipments()
        
        self.teks1.adjustFontSizeToDevice()
        self.teks2.adjustFontSizeToDevice()
        self.teks3.adjustFontSizeToDevice()
        self.teks4.adjustFontSizeToDevice()
        self.teks5.adjustFontSizeToDevice()
        self.teksSudahPunyaAkun.adjustFontSizeToDevice()
        self.teksBelumPunyaAkun.adjustFontSizeToDevice()
        self.leftButton.adjustFontSizeToDevice()
        self.rightButton.adjustFontSizeToDevice()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ViewController.general_view_controller_width = self.view.bounds.size.width - 16 * 2
        if C().isFirstTime() {
            self.gotoIntro()
        } else {
            let user = MasterDB().getSingleUser()
            print(user.debugDescription)
            if user.count > 0 && user["sms_code"] != nil && (user["sms_code"] as! String) == "-1" {
                self.gotoHome(user: LoginVC.parseUserLoginData(data: user)!)
            }
        }
        
        if DevProdSwitchVC.hasChanges {
            self.viewDebug.isHidden = false
            MasterDB().deleteSingleUser()
        }
    }
    
    // TODO: only for testing
    func test(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DevProdSwitchVCID") as! DevProdSwitchVC
        self.present(vc, animated: true, completion: nil)

    }
    
    // TODO: only for testing
    func gotoIntro(){
        print("loaded")
        let storyboard = UIStoryboard(name: "Intro", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "IntroVCID") as! IntroVC
        self.present(vc, animated: true, completion: nil)
    }
    
    func gotoHome(user: User){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(
            withIdentifier: Device.getSimpleNameDevice() == Device.IPAD
            ? "HomeDrawerVCIPAD"
            : "HomeDrawerVC"
        ) as! HomeDrawerVC
        HomeVC.user = user
        HomeVC.drawerController = vc
        
        self.present(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    static func setOLabel(button: UIButton, text: String, filled: Bool) {
        button.layer.cornerRadius = button.frame.size.height / 2
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        
        if(filled){
            if #available(iOS 11.0, *) {
                button.setTitleColor(UIColor(named: "primary"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            button.backgroundColor = UIColor.white
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.white.cgColor
        }else{
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = .clear
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    static func setFilledLabel(button: UIButton, text: String, colorGrayElseRed: Bool? = false, ratioCircle: Float? = 1, font_size: Float? = 13) {
        button.layer.cornerRadius = button.frame.size.height * CGFloat(ratioCircle! / 2)
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: CGFloat(font_size!))
        
        let color_x = UIColor(rgb: (!colorGrayElseRed! ? ViewController.RED_COLOR : ViewController.GRAY_COLOR))
    
        button.setTitleColor(UIColor(rgb: ViewController.WHITE_COLOR), for: .normal)
        button.backgroundColor = color_x
        button.layer.borderWidth = 1
        button.layer.borderColor = color_x.cgColor
    }
    
    static func setOLabel(button: UIButton, text: String, filled: Bool, reverseColor: Bool? = false, ratioCircle: Float? = 1, font_size: Float? = 13) {
        button.layer.cornerRadius = button.frame.size.height * CGFloat(ratioCircle! / 2)
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: CGFloat(font_size!))
        
        let color_x = UIColor(rgb: (!reverseColor! ? ViewController.PRIMARY_COLOR : ViewController.WHITE_COLOR))
        let color_y = UIColor(rgb: (reverseColor! ? ViewController.PRIMARY_COLOR : ViewController.WHITE_COLOR))
        if(filled){
            button.setTitleColor(color_x, for: .normal)
            button.backgroundColor = color_y
            button.layer.borderWidth = 1
            button.layer.borderColor = color_y.cgColor
        }else{
            button.setTitleColor(color_y, for: .normal)
            button.backgroundColor = .clear
            button.layer.borderWidth = 1
            button.layer.borderColor = color_y.cgColor
        }
    }
    
    static func maskCircleImage(image: UIImage, exactRadius: Double? = -1) -> UIImage {
        let minHW = min(image.size.height, image.size.width)
        let imageView: UIImageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFill
        if exactRadius == -1 {
            imageView.bounds.size = CGSize(width: minHW, height: minHW)
        }
        let layer = imageView.layer
        layer.masksToBounds = true
        if exactRadius == -1 {
            layer.cornerRadius = minHW / 2
        } else {
            layer.cornerRadius = CGFloat(exactRadius!)
        }
        UIGraphicsBeginImageContext(imageView.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return roundedImage!
    }
}

class XUIViewController: UIViewController{
    
    static var y_keyboard_move: CGFloat = CGFloat(0)
    static var originView: CGRect? = nil
    static var originViewWithKeyboard: CGRect? = nil
    static var isKeyboardShow: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(XUIViewController.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(XUIViewController.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.hideKeyboardWhenTappedAround()
    }
    
    func backStack(){
        if XUIViewController.isKeyboardShow {
            self.dismissKeyboard()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func backBackStack(){
        if XUIViewController.isKeyboardShow {
            self.dismissKeyboard()
        }
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    // issue on ipad when presenting pop modal got exception without context sender
    func showActionSheet(sender: AnyObject, mainTitle: String, titleA: String, actionA: @escaping (() -> ()),
                         titleB: String, actionB: @escaping (() -> ())) {
        guard let button = sender as? UIView else {
            return
        }
        
        let actionSheet = UIAlertController(title: mainTitle, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: titleA, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            actionA()
        }))
        
        actionSheet.addAction(UIAlertAction(title: titleB, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            actionB()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let presenter = actionSheet.popoverPresentationController {
            presenter.sourceView = button
            presenter.sourceRect = button.bounds
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showSingleActionSheet(sender: AnyObject, mainTitle: String, titleA: String, actionA: @escaping (() -> ())) {
        guard let button = sender as? UIView else {
            return
        }
        
        let actionSheet = UIAlertController(title: mainTitle, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: titleA, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            actionA()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let presenter = actionSheet.popoverPresentationController {
            presenter.sourceView = button
            presenter.sourceRect = button.bounds
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        XUIViewController.isKeyboardShow = true
        if let keyboardFrame: NSValue = sender.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            if XUIViewController.originView == nil {
                XUIViewController.originView = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y,
                                                      width: self.view.frame.width, height: self.view.frame.height)
            }
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y,
                                     width: self.view.frame.width, height: (XUIViewController.originView?.height)! - keyboardHeight)
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        XUIViewController.isKeyboardShow = false
        if XUIViewController.originView != nil {
            self.view.frame = XUIViewController.originView!
        }
    }
}

extension UIViewController {
    func validateFieldOK(views: [Any?]) -> Bool {
        for view in views {
            if view == nil || (view is String ? (view as! String).isEmpty : false) {
                return false
            }
        }
        return true
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func assignPicker(delegateTo: UIPickerViewDelegate, pickerView: UIPickerView, inputField: UITextField,
                      action_done: Selector?, action_cancel: Selector?){
        pickerView.delegate = delegateTo
        inputField.inputView = pickerView
        inputField.inputAccessoryView = self.createToolbarPickerView(
            pickerView: pickerView,
            action_done: action_done,
            action_cancel: action_cancel
        )
        pickerView.reloadAllComponents()
    }
    
    func removePicker(pickerView: UIPickerView, inputField: UITextField){
        pickerView.delegate = nil
        inputField.inputView = nil
        inputField.inputAccessoryView = nil
    }
    
    func getThreeUIsViewPicker(pickerView: UIPickerView, s_done: Selector?, s_cancel: Selector?) -> [UIBarButtonItem] {
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: s_done)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: s_cancel)
        
        return [cancelButton, spaceButton, doneButton]
    }
    
    func createToolbarPickerView(pickerView: UIPickerView, action_done: Selector?, action_cancel: Selector?) -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        toolBar.setItems(self.getThreeUIsViewPicker(pickerView: pickerView, s_done: action_done, s_cancel: action_cancel), animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
}

extension UIView {
    
    func adjustFontSizeToDevice(){
        if Device.getSimpleNameDevice() != Device.IPAD {
            return
        }
        
        print("SSDevice" + Device.getSimpleNameDevice())
        let percentageFont = (AppDelegate.screenSize?.width)! / CGFloat(Device.getSimpleNameDevice() == Device.IPHONE ? 720 : 850)
        if self is UILabel {
            let selflabel = self as? UILabel
            let fontSize = (selflabel?.font.pointSize)! * percentageFont
            selflabel?.font = selflabel?.font.withSize(fontSize)
        }
        
        if self is UIButton {
            let selfbutton = self as? UIButton
            let fontSize = (selfbutton?.titleLabel?.font.pointSize)! * percentageFont
            selfbutton?.titleLabel?.font = selfbutton?.titleLabel?.font.withSize(fontSize)
        }
        
        if self is UITextField {
            let selftextfield = self as? UITextField
            let fontSize = (selftextfield?.font?.pointSize)! * percentageFont
            selftextfield?.font = selftextfield?.font?.withSize(fontSize)
        }
        
        if self is UIImageView {
            let selfimage = self as? UIImageView
            selfimage?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            let w = (selfimage?.frame.size.width)! * percentageFont
            let h = (selfimage?.frame.size.height)! * percentageFont
            selfimage?.bounds.size.width = w
            selfimage?.bounds.size.height = h
            selfimage?.layoutIfNeeded()
        }
    }
    
    func createTwoHalfCircle(col: UIColor? = UIColor(rgb: 0xF2F2F2)) {
        let radius = CGFloat(15)
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: ViewController.general_view_controller_width, height: self.bounds.size.height), cornerRadius: 0)
        let circlePath = UIBezierPath(roundedRect: CGRect(x: -radius, y: self.bounds.size.height / 2 - radius, width: 2 * radius, height: 2 * radius), cornerRadius: radius)
        let circlePath2 = UIBezierPath(roundedRect: CGRect(x: ViewController.general_view_controller_width - radius, y: self.bounds.size.height / 2 - radius, width: 2 * radius, height: 2 * radius), cornerRadius: radius)
        path.append(circlePath)
        path.append(circlePath2)
        path.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = col?.cgColor
        fillLayer.opacity = 1
        fillLayer.borderWidth = 0
        self.backgroundColor = UIColor.clear
        self.layer.borderWidth = 0
        self.layer.addSublayer(fillLayer)
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func setBorderBottom(){
        let border = CALayer()
        let height = CGFloat(1)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - height, width: self.frame.size.width, height: height)
        border.borderWidth = self.frame.size.width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

extension UITextField {
    private static var last_value_text: String = ""
    private static var chars_max_length: [String: Int] = [:]
    func setInputOnlyNumber() {
        self.keyboardType = UIKeyboardType.decimalPad
    }
    
    func getUID() -> String{
        return String(ObjectIdentifier(self).hashValue)
    }
    
    func setInputPhoneNumber() {
        self.setInputOnlyNumber()
        self.addTarget(self, action: #selector(phoneNumberListener(textField:)), for: .editingChanged)
    }
    
    func setMaxInputLength(max: Int) {
        UITextField.chars_max_length[self.getUID()] = max
        self.addTarget(self, action: #selector(inputLengthConstraint(textField:)), for: .editingChanged)
    }
    
    func setUpperCase(){
        self.autocapitalizationType = UITextAutocapitalizationType.allCharacters
    }
    
    func showRightImage(img_name: String? = nil, w: CGFloat = CGFloat(50), h: CGFloat = CGFloat(10)){
        if img_name == nil {
            return
        }
        
        let imageView = UIImageView();
        let image = UIImage(named: img_name!);
        imageView.image = image;
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0, y: 0, width: w, height: h)
        self.rightView = imageView;
        self.rightViewMode = .always
    }
    
    func showCalendar(){
        self.showRightImage(img_name: "ic_calendar", w: CGFloat(40), h: CGFloat(18))
    }
    
    func showClock(){
        self.showRightImage(img_name: "ic_clock", w: CGFloat(40), h: CGFloat(18))
    }
    
    func showArrow() {
        self.showRightImage(img_name: "arrow")
    }
    
    @objc func inputLengthConstraint(textField: UITextField) {
        let maxl = UITextField.chars_max_length[self.getUID()]
        
        if self.text!.count > maxl! {
            while (self.text!.count > maxl!) {
                self.deleteBackward()
            }
        }
    }
    
    @objc func phoneNumberListener(textField: UITextField) {
        let data = String(self.text!)
        
        if data == "0" {
            self.text = "+62"
        } else if UITextField.last_value_text.count == 0 && data.count == 1 {
            self.text = "+62" + data
        }
        UITextField.last_value_text = self.text!
    }
    
    open override func becomeFirstResponder() -> Bool {
        XUIViewController.y_keyboard_move = self.frame.origin.y
        return super.becomeFirstResponder()
    }
}

extension Int64 {
    func intValue() -> Int {
        return Int(self)
    }
}

// source: https://stackoverflow.com/questions/37048759/swift-display-html-data-in-a-label-or-textview
extension String {
    var htmlToAttributedString: NSAttributedString? {
        do {
            let fontx = UIFont.systemFont(ofSize: 11.0)
            print(fontx.debugDescription)
            let datax = try? NSAttributedString(data: String(format:("<div style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(fontx.pointSize)\">%@</div>" as NSString) as String, description).data(using: .utf8, allowLossyConversion: true)!,
                                                options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
                                                documentAttributes: nil)
            
            if let datax = datax {
                print("parsing OK")
                return datax
            }else{
                print("parsing not OK")
                return NSAttributedString()
            }
        } catch (let error) {
            print(error.localizedDescription)
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


extension UILabel {
    var numberTruncated: Int {
        
        guard let labelText = text else {
            return 0
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: font],
            context: nil).size
        
        return Int(floor(labelTextSize.height / bounds.size.height))
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func formatNumber(_ number: Double, toPlaces: Int) -> String? {
        
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = toPlaces // minimum number of fraction digits on right
        formatter.maximumFractionDigits = toPlaces // maximum number of fraction digits on right, or comment for all available
        formatter.minimumIntegerDigits = 1 // minimum number of integer digits on left (necessary so that 0.5 don't return .500)
        
        let formattedNumber = formatter.string(from: NSNumber.init(value: number))
        
        return formattedNumber
        
    }
}

extension Double {
    struct Number {
        static let formatterIDR: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.positiveFormat = "¤ #.##0"
            formatter.negativeFormat = "¤ -#.##0"
            formatter.currencySymbol = "Rp"
            return formatter
        }()
    }
    var currencyIDR:    String {
        return Number.formatterIDR.string(from: self as NSNumber)?.description ?? "Rp 0"
    }
}
