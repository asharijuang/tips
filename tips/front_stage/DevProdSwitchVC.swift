//
//  DevProdSwitchVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 29/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class DevProdSwitchVC: XUIViewController {

    @IBOutlet weak var toggleDevProd: UISwitch!
    @IBOutlet weak var labelDevelopment: UILabel!
    @IBOutlet weak var labelProduction: UILabel!
    @IBOutlet weak var buttonBack: UIButton!
    
    static var hasChanges: Bool = false
    var currentState: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelDevelopment.text = "Development\nragnar tips"
        self.labelProduction.text = "Production\niris tips"
        self.currentState = C().getEndpointConfig()
        ViewController.setOLabel(button: self.buttonBack, text: "Kembali", filled: true, reverseColor: true)
        self.buttonBack.addTapGestureRecognizer {
            if DevProdSwitchVC.hasChanges {
                let refreshAlert = UIAlertController(title: "Perubahan terdeteksi!!!", message: "setelah perubahan endpoint, aplikasi harus di restart, tetap lanjut?", preferredStyle: UIAlertControllerStyle.alert)
                refreshAlert.addAction(UIAlertAction(title: "Lanjut", style: .default, handler: { (action: UIAlertAction!) in
                    self.backStack()
                }))
                refreshAlert.addAction(UIAlertAction(title: "Batal", style: .cancel, handler: nil))
                self.present(refreshAlert, animated: true, completion: nil)
            } else {
                self.backStack()
            }
        }
        self.toggleDevProd.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        changeActive(isProduction: C().getEndpointConfig())
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let isProduction = mySwitch.isOn
        
        if isProduction {
            C().setProductionEndpoint()
        } else {
            C().setDevelopmentEndpoint()
        }
        
        changeActive(isProduction: isProduction)
        DevProdSwitchVC.hasChanges = self.currentState != isProduction
    }
    
    func changeActive(isProduction: Bool) {
        self.toggleDevProd.setOn(isProduction, animated: false)
        
        let prod_color = isProduction ? UIColor.white : UIColor.lightGray
        let dev_color = !isProduction ? UIColor.white : UIColor.lightGray
        
        labelProduction.textColor = prod_color
        labelDevelopment.textColor = dev_color
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
