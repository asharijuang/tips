//
//  GuestMiddleware.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class GuestMiddleware {
    var cb_success: ((_ user: User) -> ())?
    var cb_error:   (() -> ())?
    var vc:         UIViewController?
    
    init(success: ((_ user: User) -> ())?, error: (() -> ())?, viewcont: UIViewController) {
        self.cb_success = success
        self.cb_error = error
        self.vc = viewcont
        testLogin()
    }
    
    private func getID() -> String {
        let id = UIDevice.current.identifierForVendor!.uuidString
        print("id print: \(id)")
        return id
    }
    
    
    func testLogin(){
        let loginParam: Parameters = [
            "mobile_phone_no": self.getID()
        ]
        
        Alamofire.request(AuthRouter.guest(parameters: loginParam)).responseJSON {
            json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self.vc!, completion: {
                    self.cb_error?()
                })
                return
            }
            
            let result = E.getData(json_data: json_data)
            let user = LoginVC.parseUserLoginData(data: result)
            print(result)
            
            if user != nil {
                MasterDB().insertSingleUser(user: result)
                self.cb_success?(user!)
            } else {
                self.cb_error?()
            }
        }
    }
}
