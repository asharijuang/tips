//
//  ForgotPasswordVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 11/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: KirimVCFamily {

    @IBOutlet weak var fieldEmail: TextField!
    @IBOutlet weak var imageBack: UIButton!
    @IBOutlet weak var buttonKirim: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageBack.addTapGestureRecognizer {
            self.backStack()
        }
        self.buttonKirim.addTapGestureRecognizer {
            self.reset_password(completion: nil)
        }
        self.setupLayout()

        // Do any additional setup after loading the view.
    }
    
    func setupLayout(){
        self.fieldEmail.layer.cornerRadius = 4
        ViewController.setOLabel(button: self.buttonKirim, text: "Kirim", filled: false, reverseColor: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ForgotPasswordVC {
    func reset_password(completion: (() -> ())?){
        let email = self.fieldEmail.text
        
        if email == nil || (email?.isEmpty)! {
            self.fieldEmail.showError()
            E.showUIError(vc: self, message: "Email tidak boleh kosong", completion: nil)
            return
        }
        
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.call_reset_password(email: email!) { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // we dont care with the result
            UIViewController.removeSpinner(spinner: sv)
            E.showUISuccess(vc: self, message: "Email telah dikirim ke " + email!, completion: {
                self.backStack()
                if completion != nil {
                    completion!()
                }
            });
        }
    }
    
    func call_reset_password(email: String, callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "email": email
        ]
        
        Alamofire.request(AuthRouter.reset_password(parameters: param)).responseJSON(completionHandler: callback)
    }
}
