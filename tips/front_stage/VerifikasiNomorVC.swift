//
//  VerifikasiNomorVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 17/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class VerifikasiNomorVC: XUIViewController {

    @IBOutlet weak var backgroundNoHPField: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var fieldNoHP: UITextField!
    @IBOutlet weak var buttonVerify: UIButton!
    @IBOutlet weak var fieldNoHPLeading: NSLayoutConstraint!
    @IBOutlet weak var fieldNoHPTrailing: NSLayoutConstraint!
    
    public static var user = User()
    public var uniqsocialid: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.backButton.addTapGestureRecognizer {
            self.backStack()
        }
        self.buttonVerify.addTapGestureRecognizer {
            let noHp = self.fieldNoHP.text ?? ""
            if noHp.isEmpty {
                E.showUIError(vc: self, message: "Nomor HP tidak boleh kosong")
                return
            }
            print("logx: \(noHp)");
            self.verifikasiNoHP(nohp: noHp)
        }

        // Do any additional setup after loading the view.
    }
    
    func setupLayout(){
        let halfHeight = self.backgroundNoHPField.frame.size.height / 2
        self.backgroundNoHPField.layer.cornerRadius = halfHeight
        self.backgroundNoHPField.layer.masksToBounds = true
        self.fieldNoHP.setInputPhoneNumber()
        self.fieldNoHPLeading.constant = halfHeight
        self.fieldNoHPTrailing.constant = halfHeight
        ViewController.setOLabel(button: self.buttonVerify, text: "Verify", filled: false, reverseColor: false)
    }
    
    func verifikasiNoHP(nohp: String){
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(AuthRouter.verify_fb_phonenum(parameters: [
            "mobile_phone_no": nohp,
            "uniq_social_id": self.uniqsocialid
            ])).responseJSON {
                json_data in
                
                // check if response is contains error or no result
                if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                    E.showUIError(vc: self, completion: {
                        UIViewController.removeSpinner(spinner: sv)
                    })
                    return
                }
                
                let json = json_data.result.value as? [String: Any]
                // no need to check result
                let error = json!["err"] as? [String: Any]
                
                UIViewController.removeSpinner(spinner: sv)
                
                if(error != nil){
                    return
                }
                
                // success
                // in case user facebook account doesn't have phone number
                VerifikasiNomorVC.user.mobile_phone_no = nohp
                self.gotoVerification(user: VerifikasiNomorVC.user)
        }
    }
    
    func gotoVerification(user: User){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SMSVerificationVCID") as! SMSVerificationVC
        SMSVerificationVC.user = user
        
        self.present(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
