//
//  SMSVerificationVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 08/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class SMSVerificationVC: XUIViewController {
    @IBOutlet weak var imageBack: UIButton!
    
    @IBOutlet weak var fieldSMS1: UITextField!
    @IBOutlet weak var fieldSMS2: UITextField!
    @IBOutlet weak var fieldSMS3: UITextField!
    @IBOutlet weak var fieldSMS4: UITextField!
    @IBOutlet weak var fieldSMS5: UITextField!
    @IBOutlet weak var fieldSMS6: UITextField!
    
    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var labelTimerAndError: UILabel!
    @IBOutlet weak var buttonVerifikasi: UIButton!
    @IBOutlet weak var buttonResend: UIButton!
    
    public static var user = User()
    
    var textFieldState: [Int] = [-1, -1, -1, -1, -1, -1]
    
    var seconds = -1
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.runTimer()
        self.imageBack.addTapGestureRecognizer {
            self.backStack()
        }
        buttonVerifikasi.addTapGestureRecognizer {
            let sv = UIViewController.displaySpinner(onView: self.view)
            self.verifySMS(callback: { json_data in
                // check if response is contains error or no result
                if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                    E.showUIError(vc: self, completion: {
                        UIViewController.removeSpinner(spinner: sv)
                    })
                    return
                }
                
                let json = json_data.result.value as? [String: Any]
                let result = json!["result"] as? [String: Any]
                let error = json!["err"] as? [String: Any]
                
                UIViewController.removeSpinner(spinner: sv)
                if(error != nil){
                    return
                }
                
                print(result)
                let user = SMSVerificationVC.user
                if user != nil {
                    let parentVC = self.presentingViewController?.presentingViewController
                    parentVC?.dismiss(animated: true, completion: {
                        C().setNotGuest()
                        self.gotoHome(user: user, parentVC: parentVC!)
                    })
                } else {
                    print("this is an imposible state, please contact TIPS Administrator")
                }
            })
        }
        
        buttonResend.addTapGestureRecognizer {
            let sv = UIViewController.displaySpinner(onView: self.view)
            self.resendSMS(callback: { json_data in
                
                // check if response is contains error or no result
                if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                    E.showUIError(vc: self, completion: {
                        UIViewController.removeSpinner(spinner: sv)
                    })
                    return
                }
                
                let json = json_data.result.value as? [String: Any]
                let result = json!["result"] as? String
                let error = json!["err"] as? [String: Any]
                
                UIViewController.removeSpinner(spinner: sv)
                if(error != nil){
                    return
                }
                
                E.showUIError(vc: self, message: result, completion: nil)
                self.stopTimer()
                self.runTimer()
            })
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.stopTimer()
        super.viewWillDisappear(animated)
    }
    
    func gotoHome(user: User, parentVC: UIViewController) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(
            withIdentifier: Device.getSimpleNameDevice() == Device.IPAD
                ? "HomeDrawerVCIPAD"
                : "HomeDrawerVC"
            ) as! HomeDrawerVC
        HomeVC.user = user
        HomeVC.drawerController = vc
        
        parentVC.present(vc, animated: true, completion: nil)
    }
    
    func getSMSCodeFromView() -> String {
        var str_out = ""
        for z in self.textFieldState {
            str_out += String(z)
        }
        
        return str_out
    }
    
    func setupLayout(){
        self.fieldSMS1.setBorderBottom()
        self.fieldSMS1.setInputOnlyNumber()
        self.fieldSMS1.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.fieldSMS2.setBorderBottom()
        self.fieldSMS2.setInputOnlyNumber()
        self.fieldSMS2.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.fieldSMS3.setBorderBottom()
        self.fieldSMS3.setInputOnlyNumber()
        self.fieldSMS3.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.fieldSMS4.setBorderBottom()
        self.fieldSMS4.setInputOnlyNumber()
        self.fieldSMS4.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.fieldSMS5.setBorderBottom()
        self.fieldSMS5.setInputOnlyNumber()
        self.fieldSMS5.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.fieldSMS6.setBorderBottom()
        self.fieldSMS6.setInputOnlyNumber()
        self.fieldSMS6.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        ViewController.setOLabel(button: buttonVerifikasi, text: "Verifikasi", filled: false, reverseColor: false)
        ViewController.setOLabel(button: buttonResend, text: "Resend", filled: true, reverseColor: false)
        labelPhoneNumber.text = SMSVerificationVC.user.mobile_phone_no!
    }

    @objc func textFieldDidChange(textField: UITextField){
        switch textField {
        case self.fieldSMS1:
            let last_state = self.textFieldState[0]
            let now_state = (self.fieldSMS1.text?.isEmpty)! ? -1 : Int(self.fieldSMS1.text!)!
            if last_state == -1 && now_state != -1 {
                fieldSMS2.becomeFirstResponder()
            }
            if last_state != -1 && now_state == -1 {
                // no action
            }
            if last_state != -1 && now_state != -1 {
                self.textFieldState[1] = now_state % 10
                self.fieldSMS1.text = String(self.textFieldState[0])
                self.fieldSMS2.text = String(self.textFieldState[1])
                fieldSMS3.becomeFirstResponder()
            } else {
                self.textFieldState[0] = now_state
            }
            break;
        case self.fieldSMS2:
            let last_state = self.textFieldState[1]
            let now_state = (self.fieldSMS2.text?.isEmpty)! ? -1 : Int(self.fieldSMS2.text!)!
            if last_state == -1 && now_state != -1 {
                fieldSMS3.becomeFirstResponder()
            }
            if last_state != -1 && now_state == -1 {
                fieldSMS1.becomeFirstResponder()
            }
            if last_state != -1 && now_state != -1 {
                self.textFieldState[2] = now_state % 10
                self.fieldSMS2.text = String(self.textFieldState[1])
                self.fieldSMS3.text = String(self.textFieldState[2])
                fieldSMS4.becomeFirstResponder()
            } else {
                self.textFieldState[1] = now_state
            }
            break;
        case self.fieldSMS3:
            let last_state = self.textFieldState[2]
            let now_state = (self.fieldSMS3.text?.isEmpty)! ? -1 : Int(self.fieldSMS3.text!)!
            if last_state == -1 && now_state != -1 {
                fieldSMS4.becomeFirstResponder()
            }
            if last_state != -1 && now_state == -1 {
                fieldSMS2.becomeFirstResponder()
            }
            if last_state != -1 && now_state != -1 {
                self.textFieldState[3] = now_state % 10
                self.fieldSMS3.text = String(self.textFieldState[2])
                self.fieldSMS4.text = String(self.textFieldState[3])
                fieldSMS5.becomeFirstResponder()
            } else {
                self.textFieldState[2] = now_state
            }
            break;
        case self.fieldSMS4:
            let last_state = self.textFieldState[3]
            let now_state = (self.fieldSMS4.text?.isEmpty)! ? -1 : Int(self.fieldSMS4.text!)!
            if last_state == -1 && now_state != -1 {
                fieldSMS5.becomeFirstResponder()
            }
            if last_state != -1 && now_state == -1 {
                fieldSMS3.becomeFirstResponder()
            }
            if last_state != -1 && now_state != -1 {
                self.textFieldState[4] = now_state % 10
                self.fieldSMS4.text = String(self.textFieldState[3])
                self.fieldSMS5.text = String(self.textFieldState[4])
                fieldSMS6.becomeFirstResponder()
            } else {
                self.textFieldState[3] = now_state
            }
            break;
        case self.fieldSMS5:
            let last_state = self.textFieldState[4]
            let now_state = (self.fieldSMS5.text?.isEmpty)! ? -1 : Int(self.fieldSMS5.text!)!
            if last_state == -1 && now_state != -1 {
                fieldSMS6.becomeFirstResponder()
            }
            if last_state != -1 && now_state == -1 {
                fieldSMS4.becomeFirstResponder()
            }
            if last_state != -1 && now_state != -1 {
                self.textFieldState[5] = now_state % 10
                self.fieldSMS5.text = String(self.textFieldState[4])
                self.fieldSMS6.text = String(self.textFieldState[5])
                fieldSMS6.becomeFirstResponder()
            } else {
                self.textFieldState[4] = now_state
            }
            break;
        case self.fieldSMS6:
            let last_state = self.textFieldState[5]
            let now_state = (self.fieldSMS6.text?.isEmpty)! ? -1 : Int(self.fieldSMS6.text!)!
            if last_state == -1 && now_state != -1 {
                // no action
            }
            if last_state != -1 && now_state == -1 {
                fieldSMS5.becomeFirstResponder()
            }
            if last_state != -1 && now_state != -1 {
                self.fieldSMS6.text = String(self.textFieldState[5])
            } else {
                self.textFieldState[5] = now_state
            }
            break;
        default:
            //
            break;
        }
    }
    
    func runTimer() {
        self.labelTimerAndError.textColor = UIColor.white
        self.seconds = 59
        self.setLabelTime()
        self.buttonVerifikasi.isHidden = false
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(SMSVerificationVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        timer.invalidate()
        self.labelTimerAndError.textColor = UIColor.red
        self.buttonVerifikasi.isHidden = true
        self.labelTimerAndError.text = "Your code has been expired, click resend to get a new code"
    }
    
    func setLabelTime(){
        let str_sec = (seconds < 10 ? "0" : "") + String(seconds)
        self.labelTimerAndError.text = "Your code will be expired in 00:\(str_sec)"
    }
    
    @objc func updateTimer(){
        if seconds > 1 {
            self.seconds -= 1
            self.setLabelTime()
        } else {
            self.stopTimer()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func verifySMS(callback: @escaping (DataResponse<Any>) -> Void){
        let noHP: String = SMSVerificationVC.user.mobile_phone_no!
        
        let param: Parameters = [
            "mobile_phone_no": noHP,
            "sms_code": self.getSMSCodeFromView()
        ]
        
        print(param)
        
        Alamofire.request(AuthRouter.verifySMS(parameters: param)).responseJSON(completionHandler: callback)
    }
    
    func resendSMS(callback: @escaping (DataResponse<Any>) -> Void){
        let noHP: String = SMSVerificationVC.user.mobile_phone_no!
        
        let param: Parameters = [
            "mobile_phone_no": noHP
        ]
        
        Alamofire.request(AuthRouter.resendSMS(parameters: param)).responseJSON(completionHandler: callback)
    }

}
