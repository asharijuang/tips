//
//  TestVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 26/09/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class TestVC: UIViewController {

    @IBOutlet weak var labelToBeFIlled: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ambilSyaratKirim()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func connGetTerms(callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "type": "kirim"
        ]
        
        Alamofire.request(HomeRouter.terms_n_conds(parameters: param))
            .responseJSON(completionHandler: callback)
    }
    
    func ambilSyaratKirim(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connGetTerms(callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            // print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.parseData(data: result["value"] as? [String])
        })
    }
    
    
    func parseData(data: [String]? = nil) {
        if data == nil {
            print("Error: no result.value data served")
            return
        }
        
        let text = data!.joined().htmlToAttributedString
        print(text)
        self.labelToBeFIlled.attributedText = text
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
