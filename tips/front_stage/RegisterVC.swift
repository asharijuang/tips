//
//  Register.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class RegisterVC: KirimVCFamily {
    
    @IBOutlet weak var fieldNamaDepan: TextField!
    @IBOutlet weak var fieldNamaBelakang: TextField!
    @IBOutlet weak var fieldEmail: TextField!
    @IBOutlet weak var fieldNoHP: TextField!
    @IBOutlet weak var fieldPassword: TextField!
    @IBOutlet weak var fieldConfirmPassword: TextField!
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var switchPassword: UISwitch!
    
    var uiSpinner = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setOLabel(button: buttonRegister, text: "Sign Up", filled: false)
        self.fieldNoHP.setInputPhoneNumber()
        self.fieldPassword.isSecureTextEntry = true
        self.fieldConfirmPassword.isSecureTextEntry = true
        self.switchPassword.addTarget(self, action: #selector(showPassword), for: UIControlEvents.valueChanged)
        
        self.fieldNamaDepan.layer.cornerRadius = 4
        self.fieldNamaBelakang.layer.cornerRadius = 4
        self.fieldEmail.layer.cornerRadius = 4
        self.fieldNoHP.layer.cornerRadius = 4
        self.fieldPassword.layer.cornerRadius = 4
        self.fieldConfirmPassword.layer.cornerRadius = 4
        buttonRegister.addTapGestureRecognizer {
            if !self.validateFieldOK(views: [
                self.fieldNamaDepan.text,
                self.fieldNamaBelakang.text,
                self.fieldNoHP.text,
                self.fieldEmail.text,
                self.fieldPassword.text,
                self.fieldConfirmPassword.text
                ]){
                self.showErrorTextField()
                // do something
                return
            }
            
            self.hideErrorTextField()
            
            let sv = UIViewController.displaySpinner(onView: self.view)
            self.uiSpinner = sv
            self.signUp(callback: { json_data in
                // check if response is contains error or no result
                if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                    E.showUIError(vc: self, completion: {
                        UIViewController.removeSpinner(spinner: sv)
                    })
                    return
                }
                
                let json = json_data.result.value as? [String: Any]
                let result = json!["result"] as? [String: Any]
                let error = json!["err"] as? [String: Any]
                
                UIViewController.removeSpinner(spinner: sv)
                
                if(error != nil){
                    return
                }
                
                print(result)
                let user = LoginVC.parseUserLoginData(data: result)
                if user != nil {
                    MasterDB().insertSingleUser(user: result!)
                    self.gotoVerification(user: user!)
                } else {
                    print("there is an error while parsing register data")
                }
            })
        }
        
    }
    
    @objc func showPassword(mySwitch: UISwitch) {
        self.fieldPassword.isSecureTextEntry = !self.switchPassword.isOn
        self.fieldConfirmPassword.isSecureTextEntry = !self.switchPassword.isOn
    }
    
    func gotoVerification(user: User){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SMSVerificationVCID") as! SMSVerificationVC
        SMSVerificationVC.user = user

        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonRegister(_ sender: UIButton) {
    }
    @IBAction func backButton(_ sender: UIButton) {
        backStack()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBOutlet weak var gotoSignIn: UIButton!
    @IBAction func gotoSignIn(_ sender: UIButton) {
        NSLog("Invoked button")
        
        if(ViewController.isStackNextVCActive){
            backStack()
        }else{
            ViewController.isStackNextVCActive = true
            let loginVC  = self.storyboard?.instantiateViewController(withIdentifier: "LoginVCID") as! LoginVC
            present(loginVC, animated: true)
        }
    }
    
    func signUp(callback: @escaping (DataResponse<Any>) -> Void){
        let namaDepan: String = (fieldNamaDepan.text?.isEmpty)! ? "" : fieldNamaDepan.text!
        let namaBelakang: String = (fieldNamaBelakang.text?.isEmpty)! ? "" : fieldNamaBelakang.text!
        let email: String = (fieldEmail.text?.isEmpty)! ? "" : fieldEmail.text!
        let noHP: String = (fieldNoHP.text?.isEmpty)! ? "" : fieldNoHP.text!
        let password: String = (fieldPassword.text?.isEmpty)! ? "" : fieldPassword.text!
        let confirmPassword: String = (fieldConfirmPassword.text?.isEmpty)! ? "" : fieldConfirmPassword.text!
        
        if password == "" || confirmPassword == "" {
            E.showUIError(vc: self, message: "Password tidak boleh kosong", completion: nil)
            UIViewController.removeSpinner(spinner: self.uiSpinner)
            return
        }
        
        if password != confirmPassword {
            E.showUIError(vc: self, message: "Password harus sama", completion: nil)
            UIViewController.removeSpinner(spinner: self.uiSpinner)
            return
        }
        
        let fcmtoken = InstanceID.instanceID().token() == nil ? "" : InstanceID.instanceID().token()
        
        let registerParam: Parameters = [
            "mobile_phone_no": noHP,
            "first_name": namaDepan,
            "last_name": namaBelakang,
            "email": email,
            "password": password,
            "token": fcmtoken ?? ""
            
        ]
        
        Alamofire.request(AuthRouter.register(parameters: registerParam)).responseJSON(completionHandler: callback)
    }
    
    override func backStack(){
        ViewController.isStackNextVCActive = false
        super.backStack()
    }
    
    func setOLabel(button: UIButton, text: String, filled: Bool) {
        button.layer.cornerRadius = button.frame.size.height / 2
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        if(filled){
            if #available(iOS 11.0, *) {
                button.setTitleColor(UIColor(named: "primary"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            button.backgroundColor = UIColor.white
        }else{
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = .clear
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.white.cgColor
        }
    }
}
