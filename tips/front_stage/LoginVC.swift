//
//  LoginVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire
import KYDrawerController
import FacebookCore
import FBSDKLoginKit
import FBSDKCoreKit
import TwitterCore
import TwitterKit

import Firebase

class LoginVC: KirimVCFamily {
    
    @IBOutlet weak var labelGuest: UIButton!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var field_nohp: TextField!
    @IBOutlet weak var field_password: TextField!
    @IBOutlet weak var viewFacebook: UILabel!
    @IBOutlet weak var viewTwitter: UILabel!
    @IBOutlet weak var imageLogo: UIImageView!
    let facebookReadPermissions = ["public_profile"]
    struct MyProfileRequest: GraphRequestProtocol {
        struct Response: GraphResponseProtocol {
            var first_name: String = ""
            var last_name: String = ""
            var email: String = ""
            var id: String = ""
            init(rawResponse: Any?) {
                let dict = rawResponse as? [String: Any]
                let name = (dict!["name"] as! String).components(separatedBy: " ")
                self.first_name = name[0]
                self.last_name  = name.count > 1 ? name[1] : ""
                self.email      = dict!["email"] as! String
                self.id         = dict!["id"] as! String
            }
        }
        
        var graphPath = "/me"
        var parameters: [String : Any]? = ["fields": "id, name, email"]
        var accessToken = AccessToken.current
        var httpMethod: GraphRequestHTTPMethod = .GET
        var apiVersion: GraphAPIVersion = .defaultVersion
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        field_nohp.layer.cornerRadius = 4
        field_nohp.setInputPhoneNumber()
        field_password.layer.cornerRadius = 4
        field_password.isSecureTextEntry = true
        viewFacebook.layer.cornerRadius = 8
        viewFacebook.layer.masksToBounds = true
        viewTwitter.layer.cornerRadius = 8
        viewTwitter.layer.masksToBounds = true
        self.addFacebookTwitterTarget()
        //field_nohp.text = "+6281214338487"
        //field_password.text = "1"
        setOLabel(button: buttonLogin, text: "Sign In", filled: false)
//        self.field_nohp.adjustFontSizeToDevice()
//        self.field_password.adjustFontSizeToDevice()
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        self.labelGuest.addTapGestureRecognizer {
            let sv = UIViewController.displaySpinner(onView: self.view)
            GuestMiddleware(success: { (user) in
                UIViewController.removeSpinner(spinner: sv)
                C().setGuest()
                self.gotoHome(user: user)
            }, error: {
                UIViewController.removeSpinner(spinner: sv)
                E.showUIError(vc: self, message: "There is an error while start guest session, please contact Tips Administrator")
            }, viewcont: self)
        }
    }
    
    func addFacebookTwitterTarget(){
        viewFacebook.addTapGestureRecognizer {
            let loginManager = FBSDKLoginManager()
            loginManager.logIn(withReadPermissions: self.facebookReadPermissions, from: self, handler: { (loginResult, error) in
                guard let result = loginResult else { return }
                if error != nil {
                    //According to Facebook:
                    //Errors will rarely occur in the typical login flow because the login dialog
                    //presented by Facebook via single sign on will guide the users to resolve any errors.
                    
                    // Process error
                    FBSDKLoginManager().logOut()
                } else if result.isCancelled {
                    // Handle cancellations
                    FBSDKLoginManager().logOut()
                } else {
                    // If you ask for multiple permissions at once, you
                    // should check if specific permissions missing
//                    var allPermsGranted = true
//
//                    //result.grantedPermissions returns an array of _NSCFString pointers
//                    let grantedPermissions = result.grantedPermissions.allObjects.map( {"\($0)"} )
//                    for permission in self.facebookReadPermissions {
//                        if !contains(grantedPermissions, permission) {
//                            allPermsGranted = false
//                            break
//                        }
//                    }
//                    if allPermsGranted {
//                        // Do work
//                        let fbToken = result.token.tokenString
//                        let fbUserID = result.token.userID
//                        self.getFBUserData()
////                        successBlock()
//                    } else {
////                        failureBlock((nil)
//                    }
                    self.getFBUserData()
                }
            })
        }
        
        viewTwitter.addTapGestureRecognizer {
            // Swift
            TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
                if (session != nil) {
                    print("signed in as \(session?.userName ?? "")");
                    self.getTwitterUserData()
                } else {
                    print("error: \(error?.localizedDescription ?? "")");
                }
            })
        }
    }
    
    func getFBUserData(){
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                print(response.id)
                print(response.first_name)
                print(response.last_name)
                print(response.email)
                self.loginFacebookTwitter(is_twitter: false, id: response.id, fname: response.first_name,
                                          lname: response.last_name, email: response.email)
            case .failed(let error):
                print("Custom Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
    
    func getTwitterUserData(){
        let twitterUserID: String = (TWTRTwitter.sharedInstance().sessionStore.session()?.userID)!
        // Swift
        let client = TWTRAPIClient.withCurrentUser()
        
        client.requestEmail { email, error in
            if (email != nil) {
                let client2 = TWTRAPIClient.withCurrentUser()
                
                client2.loadUser(withID: twitterUserID) { (twitter_user, err) in
                    if(twitter_user == nil) {
                        print("error: \(err?.localizedDescription ?? "")");
                        return
                    }
                    
                    let name = twitter_user?.name
                    self.loginFacebookTwitter(is_twitter: true, id: twitterUserID,
                                              fname: name!, lname: "", email: email!)
                }
            } else {
                print("error: \(error?.localizedDescription ?? "")");
            }
        }
    }
    
    func loginFacebookTwitter(is_twitter: Bool, id: String, fname: String, lname: String, email: String){
        let fcmtoken = InstanceID.instanceID().token() == nil ? "" : InstanceID.instanceID().token()
        let sv = UIViewController.displaySpinner(onView: self.view)
        let the_route = is_twitter
            ? AuthRouter.login_twitter(parameters: [
                "first_name": fname,
                "last_name": lname,
                "uniq_social_id": id,
                "email": email,
                "twitter_token": "-ios-",
                "token": fcmtoken ?? "",
                "birth_date": "",
                "id_city": "",
                "address": ""
            ])
            : AuthRouter.login_fb(parameters: [
                "first_name": fname,
                "last_name": lname,
                "uniq_social_id": id,
                "email": email,
                "fb_token": "-ios-",
                "token": fcmtoken ?? "",
                "birth_date": "",
                "id_city": "",
                "address": ""
            ])
        
        Alamofire.request(the_route).responseJSON {
            json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            let json = json_data.result.value as? [String: Any]
            var result = json!["result"] as? [String: Any]
            let error = json!["err"] as? [String: Any]
            
            UIViewController.removeSpinner(spinner: sv)
            
            if(error != nil){
                return
            }
            
            // prevent database error by not null restriction on mobile_phone_no
            let mobile_phone_no = result!["mobile_phone_no"] as? String
            if mobile_phone_no == nil {
                result!["mobile_phone_no"] = ""
            }
            
            print(result)
            let user = LoginVC.parseUserLoginData(data: result)
            if user != nil {
                MasterDB().insertSingleUser(user: result!)
                if (user?.mobile_phone_no?.isEmpty)! {
                    self.gotoIsiNoHP(user: user!, uniqsocialid: id)
                } else if user?.sms_code == nil || user?.sms_code != "-1" {
                    self.gotoIsiNoHP(user: user!, uniqsocialid: id)
                } else {
                    self.gotoHome(user: user!)
                }
            } else {
                print("there is an error while parsing result login")
            }
        }
    }
    
    @IBAction func gotoSignUp(_ sender: Any) {
        if(ViewController.isStackNextVCActive){
            backStack()
        }else{
            ViewController.isStackNextVCActive = true
            let registerVC  = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVCID") as! RegisterVC
            present(registerVC, animated: true)
        }
    }
    @IBAction func backButton(_ sender: UIButton) {
        backStack()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonLogin(_ sender: UIButton) {

        if !self.validateFieldOK(views: [
            self.field_nohp.text,
            self.field_password.text
            ]){
            self.showErrorTextField()
            // do something
            return
        }
        
        self.hideErrorTextField()
        
        
        let sv = UIViewController.displaySpinner(onView: self.view)
        testLogin(callback: { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            let json = json_data.result.value as? [String: Any]
            let result = json!["result"] as? [String: Any]
            let error = json!["err"] as? [String: Any]
            
            UIViewController.removeSpinner(spinner: sv)
            
            if(error != nil){
                return
            }

            print(result)
            let user = LoginVC.parseUserLoginData(data: result)
            if user != nil {
                MasterDB().insertSingleUser(user: result!)
                
                let userx = MasterDB().getSingleUser()
                print("check begin")
                print(userx)
                print("check end")
                if user?.sms_code != nil && user?.sms_code == "-1" {
                    C().setNotGuest()
                    self.gotoHome(user: user!)
                } else {
                    self.gotoVerification(user: user!)
                }
            } else {
                print("there is an error while parsing result login")
            }
        })
    }
    
    public static func parseUserLoginData(data: [String: Any]? = nil) -> User?{
        if data == nil {
            print("Error: no user result data served")
            return nil
        }
        
        let user = User()
        user.id                 = data!["id"] as? Int
        user.first_name         = data!["first_name"] as? String
        user.last_name          = data!["last_name"] as? String
        user.birth_date         = data!["birth_date"] as? String
        user.email              = data!["email"] as? String
        user.mobile_phone_no    = data!["mobile_phone_no"] as? String
        user.money              = (data!["money"] as? Int)?.description
        user.profil_picture     = data!["profil_picture"] as? String
        user.ref_code           = data!["ref_code"] as? String
        user.sms_code           = data!["sms_code"] as? String
        user.sex                = data!["sex"] as? String
        
        return user
    }
    
    func gotoHome(user: User){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(
            withIdentifier: Device.getSimpleNameDevice() == Device.IPAD
                ? "HomeDrawerVCIPAD"
                : "HomeDrawerVC"
            ) as! HomeDrawerVC
        HomeVC.user = user
        HomeVC.drawerController = vc
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func gotoVerification(user: User){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SMSVerificationVCID") as! SMSVerificationVC
        SMSVerificationVC.user = user
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func gotoIsiNoHP(user: User, uniqsocialid: String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "VerifikasiNomorVCID") as! VerifikasiNomorVC
        vc.uniqsocialid = uniqsocialid
        VerifikasiNomorVC.user = user
        
        self.present(vc, animated: true, completion: nil)
    }
    
    override func backStack(){
        ViewController.isStackNextVCActive = false
        super.backStack()
    }
    
    func setOLabel(button: UIButton, text: String, filled: Bool) {
        button.layer.cornerRadius = button.frame.size.height / 2
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        if(filled){
            if #available(iOS 11.0, *) {
                button.setTitleColor(UIColor(named: "primary"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            button.backgroundColor = UIColor.white
        }else{
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = .clear
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    func testLogin(callback: @escaping (DataResponse<Any>) -> Void){
        let fcmtoken = InstanceID.instanceID().token() == nil ? "" : InstanceID.instanceID().token()
        let noHp: String = field_nohp.text!
        let password: String = field_password.text!
        
        let loginParam: Parameters = [
            "mobile_phone_no": noHp,
            "password": password,
            "token": fcmtoken ?? ""
        ]
        
        Alamofire.request(AuthRouter.login(parameters: loginParam)).responseJSON(completionHandler: callback)
    }
}

class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16);
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    func showError(){
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.red.cgColor
    }
    
    func hideError(){
        self.layer.borderWidth = 0
    }
    
    override func resignFirstResponder() -> Bool {
        if (self.text?.isEmpty)! {
            self.showError()
        } else {
            self.hideError()
        }
        
        return super.resignFirstResponder()
    }
}

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
