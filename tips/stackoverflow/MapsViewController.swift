//
//  MapsViewController.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 14/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class MapsViewController: XUIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var imageBack: UIImageView!
    
    @IBOutlet weak var containerDetailOK: UIView!
    @IBOutlet weak var labelPilihLokasi: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    
    @IBOutlet weak var buttonSelesai: UIButton!
    @IBOutlet weak var containerMap: UIView!
    var selfMarker = GMSMarker()
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    var success_callback: ((_ lat: Double, _ lng: Double, _ location: String) -> ()) = {lat, lng, location in }
    var failure_callback: ((_ errorMessage: String) -> ()) = {err in}
    
    var lastLocation = CLLocation()
    var lastLocationName = ""
    var isFollowedBySearch = false
    var locationManager = CLLocationManager()
    
    var myLastLocation: CLLocation? = nil
    
    @IBOutlet weak var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonSelesai.addTapGestureRecognizer {
            if self.lastLocationName.isEmpty {
                E.showUIError(vc: self, message: "Pilih lokasi pada peta terlebih daulu")
                return
            }
            self.backStack()
            self.success_callback(self.lastLocation.coordinate.latitude,
                                  self.lastLocation.coordinate.longitude, self.lastLocationName)
        }
        containerDetailOK.addTapGestureRecognizer {
            print("tidak bisa")
        }
        self.imageBack.addTapGestureRecognizer {
            self.backStack()
        }
    }
    
    func setupCorelocation(){
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupMap()
        self.setupSearch()
        self.setupCorelocation()
        
    }
    
    func setupSearch() {
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        //searchController?.searchBar.frame.origin.y = 48
        self.containerMap.addSubview((searchController?.searchBar)!)
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        // definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        // searchController?.hidesNavigationBarDuringPresentation = false
    }
    
    func moveToPosition(coordinate: CLLocationCoordinate2D, setCamera: Bool? = false){
        // Creates a marker in the center of the map.
        self.selfMarker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        self.selfMarker.map = mapView
        
        if setCamera! {
            mapView.camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 12.0)
        }
    }
    
    func showDetailMap(detail_location: String){
        labelDetail.text = detail_location
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if isFollowedBySearch {
            self.geocodeCoordinate(the_location: self.lastLocation)
            isFollowedBySearch = false
        }
        print("start move")
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        self.locationManager.startUpdatingLocation()
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        moveToPosition(coordinate: coordinate)
        
        let the_location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        self.geocodeCoordinate(the_location: the_location)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if locations.count > 0{
            print("begin")
            print((locations.last?.coordinate)!)
            let center = CLLocationCoordinate2D(latitude: (locations.last?.coordinate)!.latitude, longitude: (locations.last?.coordinate)!.longitude)
            moveToPosition(coordinate: center)
            let camera = GMSCameraPosition.camera(withLatitude: center.latitude, longitude: center.longitude, zoom: 15.0)
            mapView.camera = camera
            
            let the_location = CLLocation(latitude: (locations.last?.coordinate)!.latitude, longitude: (locations.last?.coordinate)!.longitude)
            self.myLastLocation = the_location
            self.geocodeCoordinate(the_location: the_location)
            print("end")
            self.locationManager.stopUpdatingLocation()
        } else {
            let camera = GMSCameraPosition.camera(withLatitude: -6.178959, longitude: 106.822453, zoom: 8.0)
            mapView.camera = camera
        }
    }
    
    func geocodeCoordinate(the_location: CLLocation){
        self.containerDetailOK.isHidden = false
        self.labelDetail.text = "loading..."
        CLGeocoder().reverseGeocodeLocation(the_location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print("Reverse geocoder failed with error: " + error!.localizedDescription)
                return
            }
            
            if placemarks == nil {
                print("Reverse geocoder failed with error: placemark is nil")
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                let str_addr = pm.addressDictionary!["FormattedAddressLines"] as? [String]
                let address = str_addr?.joined(separator: ", ")
                self.showDetailMap(detail_location: address as! String)
                self.lastLocationName = address as! String
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }

    func setupMap() {
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.frame = CGRect.zero
        mapView.delegate = self

        return
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// Handle the user's selection.
extension MapsViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {}
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        self.searchController?.isActive = false
        moveToPosition(coordinate: place.coordinate, setCamera: true)
        
        self.lastLocation = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        self.isFollowedBySearch = true
        print("start result")
    }
}
