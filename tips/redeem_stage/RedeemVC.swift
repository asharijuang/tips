//
//  RedeemVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 08/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class RedeemVC: XUIViewController {

    @IBOutlet weak var imageBack: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var promos: [ModVoucher] = []
    public static var money: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ambilRedeem()
        imageBack.addTapGestureRecognizer {
            self.backStack()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createDummy(){
        for i in 1...15 {
            let dummy = ModVoucher()
            dummy.id = i
            dummy.description = "test" + String(i)
            dummy.file_name = "http://borobudurpark.com/wp-content/uploads/2017/05/Borobudur-000141-300x200.jpg"
            dummy.url = "https://tips.co.id"
            dummy.remarks = "test"
            dummy.is_complete = true
            
            promos.append(dummy)
        }
    }
    
    func initScrollView(){
        let width = scrollView.frame.size.width
        var total_height = CGFloat(0)
        
        let header = UIMoneyHeader()
        let headerModel = ModMoneyHeader()
        headerModel.money = RedeemVC.money
        headerModel.is_complete = true
        header.frame = CGRect(x: 0, y: total_height, width: width, height: CGFloat(112))
        header.setupXib(modMoneyHeader: headerModel)
        scrollView.addSubview(header)
        
        total_height += CGFloat(112)
        
        for dummy in promos {
            let view = UIVoucher()
            view.frame = CGRect(x: 0, y: total_height, width: width, height: CGFloat(150))
            view.setupXib(modVoucher: dummy)
            total_height += CGFloat(150)
            scrollView.addSubview(view)
        }
        
        scrollView.contentSize = CGSize(width: width, height: total_height)
    }
    
    
    func connGetRedeem(callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "id_member": HomeVC.user.id
        ]
        Alamofire.request(HomeRouter.redeem(parameters: param)).responseJSON(completionHandler: callback)
    }
    
    func ambilRedeem(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connGetRedeem(callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.parseDataRedeem(redeems: result["promos"] as? [Any])
            self.initScrollView()
        })
    }
    
    
    func parseDataRedeem(redeems: [Any]? = nil) {
        if redeems == nil {
            print("Error: no result.promos data served")
            return
        }
        
        for reds in redeems! {
            let redeem = reds as! [String: Any]
            
            let dummy = ModVoucher()
            dummy.id = redeem["id"] as? Int
            dummy.description = (redeem["description"] as? String)!
            dummy.file_name = (redeem["file_name"] as? String)!
            dummy.url = (redeem["url"] as? String)!
            dummy.remarks = (redeem["remarks"] as? String)!
            dummy.is_complete = true
            
            promos.append(dummy)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
