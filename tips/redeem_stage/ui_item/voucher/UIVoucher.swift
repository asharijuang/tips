//
//  UIVoucher.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 08/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import SDWebImage

class UIVoucher: CustomUIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // height = 150
    
    @IBOutlet weak var containerRounded: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var labelCaption: UILabel!
    @IBOutlet weak var buttonBuy: UIButton!
    
    func setupXib(){
        self.xibSetup(nibName: "UIVoucher")
    }
    
    func setupXib(modVoucher: ModVoucher){
        if modVoucher.is_complete {
            self.setupXib(description: modVoucher.description, file_name: modVoucher.file_name, url: modVoucher.url)
        } else {
            print("your ModVoucher instance is not complete yet")
        }
    }
    
    func setupXib(description: String, file_name: String, url: String){
        self.xibSetup(nibName: "UIVoucher")
        labelCaption.text = description
        image.sd_setImage(with: URL(string: file_name)) { (image, err, sdicach, url) in
            self.image.image = image
        }
        ViewController.setOLabel(button: buttonBuy, text: "BUY", filled: true, reverseColor: true,
                                 ratioCircle: 0.4, font_size: 10)
        
        buttonBuy.addTapGestureRecognizer {
            let _url = NSURL(string: url)!
            UIApplication.shared.openURL(_url as URL)
        }
        
        containerRounded.layer.cornerRadius = 6
        containerRounded.layer.masksToBounds = true
    }

}

class ModVoucher{
    public var id: Int?
    public var description: String = ""
    public var file_name: String = ""
    public var remarks: String = ""
    public var url: String = ""
    public var is_complete: Bool = false
}
