//
//  UIMoneyHeader.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 08/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIMoneyHeader: CustomUIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // height = 112
    
    @IBOutlet weak var labelMoney: UILabel!
    
    func setupXib(){
        self.xibSetup(nibName: "UIMoneyHeader")
    }
    
    func setupXib(modMoneyHeader: ModMoneyHeader){
        if modMoneyHeader.is_complete {
            self.setupXib(money: modMoneyHeader.money)
        } else {
            print("your ModMoneyHeader instance is not complete yet")
        }
    }
    
    func setupXib(money: String){
        self.xibSetup(nibName: "UIMoneyHeader")
        labelMoney.text = money
    }
}

class ModMoneyHeader {
    public var money: String = ""
    public var is_complete: Bool = false
}
