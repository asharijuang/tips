//
//  KirimSuksesVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class KirimSuksesVC: KirimVCFamily {

    @IBOutlet weak var labelDetailLeftConstaint: NSLayoutConstraint!
    @IBOutlet weak var labelDetailRightConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var backgroundCard: UIView!
    @IBOutlet weak var labelSelamat: UILabel!
    
    @IBOutlet weak var labelKodePengiriman: UILabel!
    @IBOutlet weak var labelBerat: UILabel!
    @IBOutlet weak var labelKontenPengiriman: UILabel!
    
    @IBOutlet weak var labelNamaPengirim: UILabel!
    @IBOutlet weak var labelAlamatPengirim: UILabel!
    @IBOutlet weak var labelNoHpPengirim: UILabel!
    
    @IBOutlet weak var labelNamaPenerima: UILabel!
    @IBOutlet weak var labelAlamatPenerima: UILabel!
    @IBOutlet weak var labelNoHpPenerima: UILabel!
    
    @IBOutlet weak var buttonKembali: UIButton!
    
    var model_data = ModKirimSuksesVC()
    
    var totalLeftLabelCharacter: Int = 0
    var totalRightLabelCharacter: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restyleUIComponents()
        buttonKembali.addTapGestureRecognizer {
            self.backToRootHome()
            KirimVC.removeCacheDataPengirimPenerima()
        }
        // Do any additional setup after loading the view.
    }
    
    func restyleUIComponents(){
        backgroundCard.layer.masksToBounds = true
        backgroundCard.layer.cornerRadius = 10
        ViewController.setOLabel(button: buttonKembali, text: "Kembali ke Dashboard", filled: true, reverseColor: false)
        labelSelamat.text = "Selamat!\nBarang telah terdaftar."
        self.init_data()
    }
    
    func init_data(){
        if model_data.is_complete {
            labelKodePengiriman.text    = model_data.kode_pengiriman!
            labelBerat.text             = String(Int(model_data.berat!)) + " kg"
            labelKontenPengiriman.text  = model_data.konten_barang!
            
            labelNamaPengirim.text      = model_data.nama_pengirim!
            labelAlamatPengirim.text    = model_data.alamat_pengirim!
            labelNoHpPengirim.text      = model_data.no_hp_pengirim!
            
            totalLeftLabelCharacter     += labelNamaPengirim.text!.count
            totalLeftLabelCharacter     += labelAlamatPengirim.text!.count
            totalLeftLabelCharacter     += labelNoHpPengirim.text!.count
            
            labelNamaPenerima.text      = model_data.nama_penerima!
            labelAlamatPenerima.text    = model_data.alamat_penerima!
            labelNoHpPenerima.text      = model_data.no_hp_penerima!
            
            totalRightLabelCharacter    += labelNamaPenerima.text!.count
            totalRightLabelCharacter    += labelAlamatPenerima.text!.count
            totalRightLabelCharacter    += labelNoHpPenerima.text!.count
            
            let isRightKeepActive       = totalRightLabelCharacter > totalLeftLabelCharacter
            labelDetailLeftConstaint.isActive = !isRightKeepActive
            labelDetailRightConstaint.isActive = isRightKeepActive
            
        } else {
            labelKodePengiriman.text    = ""
            labelBerat.text             = ""
            labelKontenPengiriman.text  = ""
            
            labelNamaPengirim.text      = ""
            labelAlamatPengirim.text    = ""
            labelNoHpPengirim.text      = ""
            
            labelNamaPenerima.text      = ""
            labelAlamatPenerima.text    = ""
            labelNoHpPenerima.text      = ""
        }
    }
    
    func backToRootHome(){
        self.navigationController?.dismiss(animated: true, completion: {
            if let navc = self.navigationController as? GeneralNAVC {
                if navc.completion != nil {
                    navc.completion!()
                }else{
                    print("GeneralNAVC completion callback not implemented")
                }
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class ModKirimSuksesVC{
    var kode_pengiriman: String?
    var berat: Float?
    var konten_barang: String?
    
    var nama_pengirim: String?
    var alamat_pengirim: String?
    var no_hp_pengirim: String?
    
    var nama_penerima: String?
    var alamat_penerima: String?
    var no_hp_penerima: String?
    
    var is_complete: Bool = false
}
