//
//  MetodePembayaranVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class MetodePembayaranVC: KirimVCFamily {
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var backgroundContainer: UIView!
    
    @IBOutlet weak var labelGratis: UILabel!
    @IBOutlet weak var labelPerkiraanBerat: UILabel!
    @IBOutlet weak var perkiraanBeratField: TextField!
    @IBOutlet weak var labelBiayaAntar: UILabel!
    @IBOutlet weak var labelBiayaAsuransi: UILabel!
    @IBOutlet weak var labelTotalHarga: UILabel!
    @IBOutlet weak var metodePembayaranField: TextField!
    @IBOutlet weak var buttonLanjut: UIButton!
    
    let metodePembayaranPicker  = UIPickerView()
    let beratPicker             = UIPickerView()
    
    var metodePembayaranList    : [SPaymentMethod]  = []
    var beratList               : [SWeightInt]      = []
    
    var payment_method_selected : Int?
    var isOnOnlinePayment       : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restyleUIComponents()
        backImage.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        buttonLanjut.addTapGestureRecognizer {
            self.set_FINAL_DATA_toModel()
            print(">> SEND-SHIPMENT")
            print(">> BEGIN ---")
            print(self.get_FINAL_DATA_PARAMETER())
            print(">> END -----")
            if !self.validateFieldOK(views: [
                self.payment_method_selected
                ]){
                self.showErrorTextField()
                // do something
                return
            }
            
            self.hideErrorTextField()
            
            if !self.metodePembayaranList[self.payment_method_selected!].is_cash! {
                self.gotoOnlinePayment()
                return
            }
            self.kirim_dan_selesai()
        }
        labelGratis.text = S.etc_message
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if self.isOnOnlinePayment {
            self.isOnOnlinePayment = false
            
            KirimVC.model_data.espay_payment_code = WebEspayTips.idPayment
            self.checkAndIsPaymentOK(paymentId: WebEspayTips.idPayment, success_cb: {
                self.kirim_dan_selesai()
            })
        }
    }
    
    func gotoOnlinePayment(){
        let harga_per_kg        = KirimVC.model_data.helper.harga_per_kg
        let berat               = KirimVC.model_data.estimate_weight
        let biaya_asuransi      = Double(KirimVC.model_data.is_add_insurance! ? KirimVC.model_data.helper.biaya_asuransi! : 0)
        
        // prevent precision error -> casting Int to Float
        let harga               = Double(harga_per_kg!) * Double(berat!)
        
        self.getTipsTransactionID(amount: harga + biaya_asuransi, success_cb: { payment_id in
            let bankCode        = self.metodePembayaranList[self.payment_method_selected!].bank_code
            let bankProductCode = self.metodePembayaranList[self.payment_method_selected!].product_code
            
            let param1          = "&bankCode=" + bankCode!
            let param2          = "&bankProduct=" + bankProductCode!
            let url = __CR.VERY_BASE_URL + "payment/start?payment_id=" + payment_id + param1 + param2
            let vc = WebEspayTips.getViewController(url: url, payment_id: payment_id)
            self.gotoPaymentEspay(vc: vc)
        }) {
            //
        }
    }
    
    func checkAndIsPaymentOK(paymentId: String, success_cb: (() -> ())? = nil, fail_cb: (() -> ())? = nil){
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(PaymentRouter.payment_status(parameters: ["payment_id": paymentId])).responseJSON { response in

            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                    if fail_cb != nil {
                        fail_cb!()
                    }
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            E.showUISuccess(vc: self, message: result["message"] as? String, completion: nil)
            success_cb!()
        }
    }
    
    func connPostShipment(callback: @escaping (DataResponse<Any>) -> Void){
        let kirimParam: Parameters = self.get_FINAL_DATA_PARAMETER()
        
        Alamofire.request(KirimRouter.submit_kirim(parameters: kirimParam)).responseJSON(completionHandler: callback)
    }
    
    func kirim_dan_selesai(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connPostShipment(callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.gotoKirimSuksesVC(shipment: result["shipment"] as? [String : Any])
        })
    }
    
    func parseKirimSuksesModelVC(shipment: [String: Any]? = nil) -> ModKirimSuksesVC {
        if shipment == nil {
            print("Error: no result.shipment data served")
            return ModKirimSuksesVC()
        }
        
        let a               = ModKirimSuksesVC()
        a.kode_pengiriman   = shipment!["shipment_id"] as? String
        a.berat             = Float((shipment!["estimate_weight"] as? String)!)
        a.konten_barang     = shipment!["shipment_contents"] as? String
        
        a.nama_pengirim     = (shipment!["shipper_first_name"] as? String)! + " " + (shipment!["shipper_last_name"] as? String)!
        a.no_hp_pengirim    = shipment!["shipper_mobile_phone"] as? String
        a.alamat_pengirim   = shipment!["shipper_address"] as? String
        
        a.nama_penerima     = (shipment!["consignee_first_name"] as? String)! + " " + (shipment!["consignee_last_name"] as? String)!
        a.no_hp_penerima    = shipment!["consignee_mobile_phone"] as? String
        a.alamat_penerima   = shipment!["consignee_address"] as? String
        
        a.is_complete       = true
        
        return a
    }
    
    func restyleUIComponents(){
        backgroundContainer.layer.masksToBounds = true
        backgroundContainer.layer.cornerRadius = 10
        labelPerkiraanBerat.text = "Perkiraan\nBerat"
        perkiraanBeratField.layer.cornerRadius = 6
        metodePembayaranField.layer.cornerRadius = 6
        metodePembayaranField.showArrow()
        ViewController.setOLabel(button: buttonLanjut, text: "Lanjut", filled: true, reverseColor: true)
        self.createBeratPicker()
        self.createMetodePembayaranPicker()
        
        beratPicker.selectRow(KirimVC.model_data.helper.estimate_weight_picker_index!, inComponent: 0, animated: false)
        let string_berat = (beratList[KirimVC.model_data.helper.estimate_weight_picker_index!].value?.description)! + " kg"
        perkiraanBeratField.text = string_berat
        self.setPerhitunganBiaya()
    }
    
    func setPerhitunganBiaya(){
        let harga_per_kg        = KirimVC.model_data.helper.harga_per_kg
        let berat               = KirimVC.model_data.estimate_weight
        let biaya_asuransi      = KirimVC.model_data.is_add_insurance! ? KirimVC.model_data.helper.biaya_asuransi! : 0
        
        // prevent precision error -> casting Int to Float
        let harga               = Int(Float(harga_per_kg!) * berat!)
        labelBiayaAntar.text    = "Rp " + String(harga)
        labelBiayaAsuransi.text = "Rp " + String(biaya_asuransi)
        labelTotalHarga.text    = "Rp " + String(harga + biaya_asuransi)
    }
    
    func gotoKirimSuksesVC(shipment: [String: Any]?){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "KirimSuksesVCID") as! KirimSuksesVC
        vc.model_data = self.parseKirimSuksesModelVC(shipment: shipment)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func gotoPaymentEspay(vc: UIViewController){
        self.isOnOnlinePayment = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func set_FINAL_DATA_toModel(){
        let model = KirimVC.model_data
        model.id_shipper    = HomeVC.user.id
        model.id_device     = ""
    }
    
    func get_FINAL_DATA_PARAMETER() -> Parameters{
        let model_data = KirimVC.model_data
        return [
            "id_shipper": model_data.id_shipper!,
            "id_device": model_data.id_device!,
            
            "is_first_class": model_data.is_first_class!,
            "shipment_contents": model_data.shipment_contents!,
            "estimate_weight": model_data.estimate_weight!,
            "is_add_insurance": model_data.is_add_insurance!,
            "is_delivery": model_data.is_delivery!,
            "is_take": model_data.is_take!,
            "id_estimate_goods_value": model_data.id_estimate_goods_value!,
            
            "id_origin_city": model_data.id_origin_city!,
            "id_destination_city": model_data.id_destination_city!,
            
            "consignee_first_name": model_data.consignee_first_name!,
            "consignee_last_name": model_data.consignee_last_name,
            "consignee_mobile_phone": model_data.consignee_mobile_phone!,
            "id_consignee_district": model_data.id_consignee_district!,
            "consignee_latitude": model_data.consignee_latitude,
            "consignee_longitude": model_data.consignee_longitude,
            "consignee_address_detail": model_data.consignee_address_detail,
            "consignee_postal_code": model_data.consignee_postal_code!,
            "consignee_address": model_data.consignee_address!,
            "consignee_keterangan_tempat_penerima": model_data.consignee_keterangan_tempat_penerima!,
            
            "shipper_first_name": model_data.shipper_first_name!,
            "shipper_last_name": model_data.shipper_last_name,
            "shipper_mobile_phone": model_data.shipper_mobile_phone!,
            "id_shipper_district": model_data.id_shipper_district!,
            "shipper_latitude": model_data.shipper_latitude,
            "shipper_longitude": model_data.shipper_longitude,
            "shipper_address_detail": model_data.shipper_address_detail,
            "shipper_postal_code": model_data.shipper_postal_code!,
            "shipper_address": model_data.shipper_address!,
            "shipper_keterangan_tempat_pengirim": model_data.shipper_keterangan_tempat_pengirim!,
            
            "id_payment_type": model_data.id_payment_type,
            "espay_payment_code": model_data.espay_payment_code,
            
            "savePengirim": model_data.savePengirim,
            "savePenerima": model_data.savePenerima,
            
            "verse2": model_data.verse2
        ]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @objc func doneMetodePembayaranPicker(){
        //
        let index = metodePembayaranPicker.selectedRow(inComponent: 0)
        let metode = metodePembayaranList[index]
        metodePembayaranField.text = metode.product_name
        self.payment_method_selected = index
        metodePembayaranField.resignFirstResponder()
    }
    
    @objc func cancelMetodePembayaranPicker(){
        //
        metodePembayaranField.resignFirstResponder()
    }
    
    @objc func doneBeratPicker(){
        //
        let berat = beratList[beratPicker.selectedRow(inComponent: 0)]
        KirimVC.model_data.estimate_weight  = Float(berat.value!)
        perkiraanBeratField.text = (berat.value?.description)! + " kg"
        
        // hitung ulang biaya setelah perubahan
        self.setPerhitunganBiaya()
        perkiraanBeratField.resignFirstResponder()
    }
    
    @objc func cancelBeratPicker(){
        //
        perkiraanBeratField.resignFirstResponder()
    }

}

extension MetodePembayaranVC {
    func getTipsTransactionID(amount: Double, success_cb: ((_ result_dict: String) -> ())? = nil, fail_cb: (() -> ())? = nil){
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(PaymentRouter.transaction(parameters: [
                "amount": amount,
                "user_id": HomeVC.user.id!
        ])).responseJSON {
            json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    if fail_cb != nil {
                        fail_cb!()
                    }
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            UIViewController.removeSpinner(spinner: sv)
            let payment_id = E.getData(json_data: json_data)["payment_id"] as! String
            if success_cb != nil {
                success_cb!(payment_id)
            }
        }
    }
}

extension MetodePembayaranVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func createMetodePembayaranPicker(){
        metodePembayaranList.removeAll(keepingCapacity: false)
        for s in S.payment_method {
            metodePembayaranList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: metodePembayaranPicker, inputField: metodePembayaranField,
                          action_done: #selector(MetodePembayaranVC.doneMetodePembayaranPicker),
                          action_cancel: #selector(MetodePembayaranVC.cancelMetodePembayaranPicker))
    }
    
    func createBeratPicker(){
        beratList.removeAll(keepingCapacity: false)
        for s in S.goods_weight_list_shipment {
            beratList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: beratPicker, inputField: perkiraanBeratField,
                          action_done: #selector(MetodePembayaranVC.doneBeratPicker),
                          action_cancel: #selector(MetodePembayaranVC.cancelBeratPicker))
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == metodePembayaranPicker {
            return metodePembayaranList.count
        }
        if pickerView == beratPicker {
            return beratList.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == metodePembayaranPicker {
            return metodePembayaranList[row].product_name
        }
        if pickerView == beratPicker {
            return beratList[row].value?.description
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}
