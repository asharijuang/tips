//
//  KetentuanPenggunaVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 26/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class KetentuanPenggunaVC: XUIViewController {

    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var toggleSetuju: UIToggleCheck!
    @IBOutlet weak var textScroll: UILabel!
    @IBOutlet weak var buttonBatal: UIButton!
    @IBOutlet weak var buttonSetuju: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.ambilSyaratKirim()
        self.backImage.addTapGestureRecognizer { self.navigationController?.popViewController(animated: true) }
        self.buttonBatal.addTapGestureRecognizer { self.navigationController?.popViewController(animated: true) }
        self.buttonSetuju.addTapGestureRecognizer {
            if !self.toggleSetuju.getToggleState() {
                E.showUIUncomplete(vc: self, message: "Silahkan centang persetujuan terlebih dahulu", completion: nil)
                // do something
                return
            }
            
            self.gotoPersetujuanVC()
        }
        // Do any additional setup after loading the view.
    }
    
    func setupLayout(){
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.masksToBounds = true
        self.toggleSetuju.setupXib(text: "Telah membaca dan menyetujui syarat & ketentuan", state: false)
        ViewController.setOLabel(button: self.buttonSetuju, text: "Setuju", filled: true, reverseColor: false)
        ViewController.setOLabel(button: self.buttonBatal, text: "Batal", filled: false, reverseColor: false)
    }
    
    func connGetTerms(callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "type": "kirim"
        ]
        
        Alamofire.request(HomeRouter.terms_n_conds(parameters: param))
            .responseJSON(completionHandler: callback)
    }
    
    func ambilSyaratKirim(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.connGetTerms(callback: { (response) in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: response) || !E.isResponseDataExist(json_data: response) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // parse the main result
            let result = E.getData(json_data: response)
            // print(result)
            
            UIViewController.removeSpinner(spinner: sv)
            self.parseData(data: result["value"] as? [String])
        })
    }
    
    
    func parseData(data: [String]? = nil) {
        if data == nil {
            print("Error: no result.value data served")
            return
        }
        
        let text = data!.joined().htmlToAttributedString
        print(text)
        self.textScroll.attributedText = text
    }
    
    func gotoPersetujuanVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersetujuanVCID") as! PersetujuanVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
