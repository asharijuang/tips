//
//  KirimVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 19/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class KirimVC: KirimVCFamily {

    @IBOutlet weak var containerScrollView: UIView!
    @IBOutlet weak var kotaAsalField: TextField!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var kotaTujuanField: TextField!
    @IBOutlet weak var beratField: TextField!
    @IBOutlet weak var labelBerat: UILabel!
    @IBOutlet weak var labelHargaEstimasi: UILabel!
    @IBOutlet weak var buttonLanjut: UIButton!
    @IBOutlet weak var labelGratis: UILabel!
    @IBOutlet weak var bg: UIRoundedImageView!
    
    static var model_data = ModKirimVC()
    
    let cityAsalPicker      = UIPickerView()
    let cityTujuanPicker    = UIPickerView()
    let beratPicker         = UIPickerView()
    
    var cityAsalList        : [SCity]       = []
    var cityTujuanList      : [SCity]       = []
    var beratList           : [SWeightInt]  = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restyleUIComponents()
        backImage.addTapGestureRecognizer {
            self.backStack()
            KirimVC.removeCacheDataPengirimPenerima()
            KirimVC.model_data = ModKirimVC()
        }
        buttonLanjut.addTapGestureRecognizer {
            if !self.validateFieldOK(views: [
                KirimVC.model_data.estimate_weight,
                KirimVC.model_data.id_origin_city,
                KirimVC.model_data.id_destination_city
                ]){
                self.showErrorTextField()
                // do something
                return
            }
            
            if self.labelHargaEstimasi.text == "0" {
                self.kotaAsalField.showError()
                self.kotaTujuanField.showError()
                return
            }
            
            self.hideErrorTextField()
            // self.gotoDataPengirimVC()
            self.gotoAlamatPengirimVC()
        }
        labelGratis.text = S.etc_message
        
        KirimVC.model_data = ModKirimVC()
        // Do any additional setup after loading the view.
        
        print(self.getTextFieldData())
//        kotaAsalField.text = "kota---"
//        let tf = self.valueTextFieldData(property: "kotaAsalField", of: self.self)
//        print(tf?.text ?? "nil")
    }
    
    func restyleUIComponents(){
        labelBerat.text = "Perkiraan\nBerat"
        labelHargaEstimasi.text = "0"
        kotaAsalField.layer.cornerRadius = 6
        kotaTujuanField.layer.cornerRadius = 6
        beratField.layer.cornerRadius = 6
        kotaAsalField.showArrow()
        kotaTujuanField.showArrow()
        beratField.showArrow()
        containerScrollView.backgroundColor = UIColor.clear
        containerScrollView.layer.cornerRadius = 10
        containerScrollView.layer.masksToBounds = true
        ViewController.setOLabel(button: buttonLanjut, text: "Lanjut", filled: false, reverseColor: true)
        self.createCityAsalPicker()
        self.createCityTujuanPicker()
        self.createBeratPicker()
        self.bg.createTwoHalfCircle()
    }

    static func removeCacheDataPengirimPenerima(){
        DataPengirimVC.address = nil
        DataPenerimaVC.address = nil
    }
    
    func hitungEstimasiHarga(){
        let data = KirimVC.model_data
        if data.estimate_weight == nil || data.id_origin_city == nil || data.id_destination_city == nil {
            return
        }
        
        let harga_per_kg = S.getPrice(id_origin: data.id_origin_city!, id_destination: data.id_destination_city!)
        if harga_per_kg == nil {
            labelHargaEstimasi.text = "0"
            return
        }
        
        // NOTE
        // digunakan berikutnya pada View Controller Metode Pembayaran
        KirimVC.model_data.helper.harga_per_kg = harga_per_kg?.regular
        
        // prevent precision error -> casting Int to Float
        let harga = Int(Float((harga_per_kg?.regular!)!) * data.estimate_weight!)
        labelHargaEstimasi.text = harga.description
    }
    
    func gotoAlamatPengirimVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataAlamatVCID") as! DataAlamatVC
        vc.modePengirim = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoDataPengirimVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataPengirimVCID") as! DataPengirimVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func asalTujuanSama() -> Bool {
        if KirimVC.model_data.id_origin_city == nil || KirimVC.model_data.id_destination_city == nil {
            return false
        }
        
        if KirimVC.model_data.id_origin_city == KirimVC.model_data.id_destination_city {
            E.showUIError(vc: self, message: "Asal tidak boleh sama dengan tujuan")
            return true
        }
        
        return false
    }

    @objc func doneCityAsalPicker(){
        //
        let city = cityAsalList[cityAsalPicker.selectedRow(inComponent: 0)]
        KirimVC.model_data.id_origin_city = city.id
        
        // periksa asal dan tujuan
        if self.asalTujuanSama() {
            KirimVC.model_data.id_origin_city = nil
            return
        }
        
        kotaAsalField.text = city.name
        kotaAsalField.resignFirstResponder()
        
        self.hitungEstimasiHarga()
    }
    
    @objc func cancelCityAsalPicker(){
        //
        kotaAsalField.resignFirstResponder()
    }
    
    @objc func doneCityTujuanPicker(){
        //
        let city = cityTujuanList[cityTujuanPicker.selectedRow(inComponent: 0)]
        KirimVC.model_data.id_destination_city = city.id
        
        // periksa asal dan tujuan
        if self.asalTujuanSama() {
            KirimVC.model_data.id_destination_city = nil
            return
        }
        
        kotaTujuanField.text = city.name
        kotaTujuanField.resignFirstResponder()
        
        self.hitungEstimasiHarga()
    }
    
    @objc func cancelCityTujuanPicker(){
        //
        kotaTujuanField.resignFirstResponder()
    }
    
    @objc func doneBeratPicker(){
        //
        let selected_row                    = beratPicker.selectedRow(inComponent: 0)
        let berat                           = beratList[selected_row]
        KirimVC.model_data.estimate_weight  = Float(berat.value!)
        
        // NOTE
        // nilai ini digunakan lagi pada view controller MetodePembayaran
        KirimVC.model_data.helper.estimate_weight_picker_index = selected_row
        
        beratField.text = (berat.value?.description)! + " kg"
        beratField.resignFirstResponder()
        
        self.hitungEstimasiHarga()
    }
    
    @objc func cancelBeratPicker(){
        //
        beratField.resignFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension KirimVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func createCityAsalPicker(){
        cityAsalList.removeAll(keepingCapacity: false)
        for s in S.airport_city_list {
            cityAsalList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: cityAsalPicker, inputField: kotaAsalField,
                          action_done: #selector(KirimVC.doneCityAsalPicker),
                          action_cancel: #selector(KirimVC.cancelCityAsalPicker))
    }
    
    func createCityTujuanPicker(){
        cityTujuanList.removeAll(keepingCapacity: false)
        for s in S.airport_city_list {
            cityTujuanList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: cityTujuanPicker, inputField: kotaTujuanField,
                          action_done: #selector(KirimVC.doneCityTujuanPicker),
                          action_cancel: #selector(KirimVC.cancelCityTujuanPicker))
    }
    
    func createBeratPicker(){
        beratList.removeAll(keepingCapacity: false)
        for s in S.goods_weight_list_shipment {
            beratList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: beratPicker, inputField: beratField,
                          action_done: #selector(KirimVC.doneBeratPicker),
                          action_cancel: #selector(KirimVC.cancelBeratPicker))
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cityAsalPicker {
            return cityAsalList.count
        }
        if pickerView == cityTujuanPicker {
            return cityTujuanList.count
        }
        if pickerView == beratPicker {
            return beratList.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityAsalPicker {
            return cityAsalList[row].name
        }
        if pickerView == cityTujuanPicker {
            return cityTujuanList[row].name
        }
        if pickerView == beratPicker {
            return beratList[row].value?.description
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}

class ModKirimVC{
    
    struct SHelper {
        // Add your model helper here
        var estimate_weight_picker_index: Int?
        var biaya_asuransi: Int?
        var harga_per_kg: Int?
    }
    
    var helper: SHelper = SHelper()
    
    // NEVER FORGET ME!
    // --- BEGIN ---
    var id_shipper: Int?                                // ~~@@~~
    var id_device: String?                              // ~~@@~~
    // ===  END  ===
    
    // SECTION: goods datas
    // --- BEGIN ---
    var is_first_class: Bool? = false                   // **
    var shipment_contents: String?                      // **
    var estimate_weight: Float?                         // **
    var is_add_insurance: Bool?                         // **
    var is_delivery: Bool?                              // **
    var is_take: Bool?                                  // **
    var id_estimate_goods_value: Int?                   // **
    // ===  END  ===
    
    // SECTION: goods origin and destination
    // for tipster
    // --- BEGIN ---
    var id_origin_city: Int?                            // **
    var id_destination_city: Int?                       // **
    // ===  END  ===
    
    // #SECTION: consignee data
    // --- BEGIN ---
    // name & mobile phone number
    var consignee_first_name: String?                   // **
    var consignee_last_name: String = ""                // **
    var consignee_mobile_phone: String?                 // **
    // address
    var id_consignee_district: Int?                     // **
    var consignee_latitude: Double = 0.00               // **
    var consignee_longitude: Double = 0.00              // **
    var consignee_address_detail: String = ""           // **
    var consignee_postal_code: String?                  // **
    var consignee_address: String?                      // **
    var consignee_keterangan_tempat_penerima: String?   // **
    // ===  END  ===
    
    // #SECTION: shipper data
    // --- BEGIN ---
    // name & mobile phone number
    var shipper_first_name: String?                     // **
    var shipper_last_name: String = ""                  // **
    var shipper_mobile_phone: String?                   // **
    // address
    var id_shipper_district: Int?                       // **
    var shipper_latitude: Double = 0.00                 // **
    var shipper_longitude: Double = 0.00                // **
    var shipper_address_detail: String = ""             // **
    var shipper_postal_code: String?                    // **
    var shipper_address: String?                        // **
    var shipper_keterangan_tempat_pengirim: String?     // **
    // ===  END  ===
    
    // #SECTION: payment
    // --- BEGIN ---
    var id_payment_type: Int = 0                        // tidak digunakan
    var espay_payment_code: String = ""                 // **
    // ===  END  ===
    
    // #SECTION: address
    // --- BEGIN ---
    var savePengirim: Bool = false                      // **
    var savePenerima: Bool = false                      // **
    // ===  END  ===
    
    var verse2: Bool = true
    
}

class KirimVCFamily: XUIViewController{
    func showErrorTextField(){
        let text_fields: [TextField] = self.getTextFieldData()
        for tf in text_fields {
            if (tf.text?.isEmpty)! {
                tf.showError()
            } else {
                tf.hideError()
            }
        }
    }
    
    func hideErrorTextField(){
        let text_fields: [TextField] = self.getTextFieldData()
        for tf in text_fields {
            tf.hideError()
        }
    }
}

extension KirimVCFamily{
    func getTextFieldData() -> [TextField]{
        var kv: [TextField] = []
        let mirror = Mirror(reflecting: self.self)
        for (name, value) in mirror.children {
            guard let name = name else { continue }
            if let tf = value as? TextField {
                kv.append(tf)
            }
        }
        
        return kv
    }
    
    func isNilDescendant(_ any: Any?) -> Bool {
        return String(describing: any) == "Optional(nil)"
    }
    
    // will be use someday
    func valueTextFieldData(property:String, of object:Any) -> TextField? {
        let mirror = Mirror(reflecting: object)
        if let child = mirror.descendant(property), !isNilDescendant(child) {
            return child as? TextField
        }
        else {
            return nil
        }
    }
}
