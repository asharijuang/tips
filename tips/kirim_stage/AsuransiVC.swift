//
//  AsuransiVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class AsuransiVC: KirimVCFamily {

    @IBOutlet weak var scrollContainer: UIScrollView!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var backgroundContainer: UIView!
    @IBOutlet weak var toggleAsuransi: UIToggleCheck!
    @IBOutlet weak var toggleDiambilPetugas: UIToggleCheck!
    @IBOutlet weak var toggleDiantarPetugas: UIToggleCheck!
    @IBOutlet weak var buttonLanjut: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restyleUIComponents()
        backImage.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        buttonLanjut.addTapGestureRecognizer {
            self.setUIDataToModel()
            self.gotoMetodePembayaranVC()
        }
        // Do any additional setup after loading the view.
    }
    
    func restyleUIComponents(){
        KirimVC.model_data.helper.biaya_asuransi = self.insurance_total()
        
        scrollContainer.layer.masksToBounds = true
        scrollContainer.layer.cornerRadius = 10
        toggleAsuransi.setupXib(text: "Saya setuju menambahkan premi asuransi\nRp \(String(KirimVC.model_data.helper.biaya_asuransi!))",
                                state: false,
                                isChangeable: true)
        toggleDiambilPetugas.setupXib(text: "Saya ingin paket saya diambil oleh petugas",
                                      state: true,
                                      isChangeable: false)
        toggleDiantarPetugas.setupXib(text: "Saya ingin paket saya diantar ke tempat penerima berada oleh petugas",
                                      state: true,
                                      isChangeable: false)
        ViewController.setOLabel(button: buttonLanjut, text: "Lanjut", filled: true, reverseColor: true)
    }
    
    func insurance_total() -> Int {
        let id_estimate_goods_value = KirimVC.model_data.id_estimate_goods_value
        let nilai_barang = S.getEstimateGoodsValue(id: id_estimate_goods_value!)?.nominal
        let insurance_percent = S.insurance.default_insurance! / 100
        
        return Int(Float(nilai_barang!) * insurance_percent)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoMetodePembayaranVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MetodePembayaranVCID") as! MetodePembayaranVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setUIDataToModel(){
        let model               = KirimVC.model_data
        model.is_add_insurance  = toggleAsuransi.getToggleState()
        model.is_take           = toggleDiambilPetugas.getToggleState()
        model.is_delivery       = toggleDiantarPetugas.getToggleState()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
