//
//  PersetujuanVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class PersetujuanVC: KirimVCFamily {

    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var backgroundDataTeks: UIView!
    @IBOutlet weak var toggleSetuju: UIToggleCheck!
    @IBOutlet weak var buttonBatal: UIButton!
    @IBOutlet weak var buttonSetuju: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restyleUIComponents()
        backImage.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        buttonBatal.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        buttonSetuju.addTapGestureRecognizer {
            if !self.toggleSetuju.getToggleState() {
                E.showUIUncomplete(vc: self, message: "Silahkan centang persetujuan terlebih dahulu", completion: nil)
                // do something
                return
            }
            
            self.hideErrorTextField()
            self.gotoAsuransiVC()
        }
        // Do any additional setup after loading the view.
    }
    
    func restyleUIComponents(){
        backgroundDataTeks.layer.masksToBounds = true
        backgroundDataTeks.layer.cornerRadius = 10
        toggleSetuju.setupXib(text: "Tidak membawa barang-barang yang tertera diatas", state: false)
        ViewController.setOLabel(button: buttonBatal, text: "Batal", filled: false, reverseColor: false)
        ViewController.setOLabel(button: buttonSetuju, text: "Setuju", filled: true, reverseColor: false)
    }
    
    func gotoAsuransiVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AsuransiVCID") as! AsuransiVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
