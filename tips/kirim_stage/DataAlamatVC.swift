//
//  DataAlamatVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 26/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class DataAlamatVC: XUIViewController {

    @IBOutlet weak var titleBar: UILabel!
    @IBOutlet weak var labelTextDataPengirim: UILabel!
    @IBOutlet weak var labelTextNamaPengirim: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var removeImage: UIImageView!
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerScroll: UIView!
    @IBOutlet weak var viewClickTambahAlamat: UIView!
    @IBOutlet weak var textFieldAlamat: TextField!
    @IBOutlet weak var labelNamaPengirim: UILabel!
    @IBOutlet weak var labelNomorTelp: UILabel!
    @IBOutlet weak var labelAlamat: UILabel!
    @IBOutlet weak var buttonLanjut: UIButton!
    @IBOutlet weak var containerPickerAddrs: UIView!
    
    var addrsList   : [ModAddr] = []
    let addrsPicker             = UIPickerView()
    var currentSelectedAddrs    = -1
    var currentSelectedAddrsID  = -1
    
    var modePengirim: Bool?
    
    static var needRefreshFromBack = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.getAddrsData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if DataAlamatVC.needRefreshFromBack {
            DataAlamatVC.needRefreshFromBack = false
            self.getAddrsData()
        }
    }
    
    func setupLayout(){
        self.backImage.addTapGestureRecognizer { self.navigationController?.popViewController(animated: true) }
        self.removeImage.addTapGestureRecognizer { self.alertRemoveAddr(completion: { self.getAddrsData() }) }
        self.scrollView.layer.cornerRadius = 10
        self.scrollView.layer.masksToBounds = true
        self.viewClickTambahAlamat.layer.cornerRadius = 6
        self.viewClickTambahAlamat.layer.masksToBounds = true
        self.textFieldAlamat.layer.cornerRadius = 6
        self.textFieldAlamat.layer.masksToBounds = true
        self.textFieldAlamat.showArrow()
        ViewController.setOLabel(button: self.buttonLanjut, text: "Lanjut", filled: true, reverseColor: true)
        self.viewClickTambahAlamat.addTapGestureRecognizer {
            // kata pak Benny ini ga perlu lagi (28 Agustus 2018)
//            if self.addrsList.count == 10 {
//                E.showUIAlert(vc: self, title: "", message: "Alamat yang tersimpan sudah mencapai jumlah maksimal (10)")
//                return
//            }
            
            if self.modePengirim! {
                self.gotoDataPengirimVC()
            } else {
                self.gotoDataPenerimaVC()
            }
        }
        self.containerPickerAddrs.isHidden = true
        
        self.titleBar.text              = "Alamat \(self.modePengirim! ? "Pengirim" : "Penerima")"
        self.labelTextDataPengirim.text = "Data \(self.modePengirim! ? "Pengirim" : "Penerima")"
        self.labelTextNamaPengirim.text = "Nama \(self.modePengirim! ? "Pengirim" : "Penerima")"
        
        self.buttonLanjut.addTapGestureRecognizer {
            if self.addrsList.count > 0 {
                self.gotoNextPage()
            }
        }
    }

    func showData(){
        self.createAddrsPicker()
        self.addrsPicker.reloadComponent(0)
        if addrsList.count > 0 {
            self.containerPickerAddrs.isHidden = false
            self.currentSelectedAddrs = 0
            self.buttonLanjut.backgroundColor = UIColor(rgb: ViewController.PRIMARY_COLOR)
            self.buttonLanjut.layer.borderColor = UIColor(rgb: ViewController.PRIMARY_COLOR).cgColor
            self.showSelectedData()
        } else {
            self.buttonLanjut.backgroundColor = UIColor(rgb: 0x989898)
            self.buttonLanjut.layer.borderColor = UIColor(rgb: 0x989898).cgColor
            self.containerPickerAddrs.isHidden = true
        }
    }
    
    func getAddrsData(){
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(KirimRouter.fav_addr(parameters: [
            "member_id": HomeVC.user.id!,
            "is_pengirim": self.modePengirim! ? 1 : 0
            ])).responseJSON {
                json_data in
                
                // check if response is contains error or no result
                if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                    UIViewController.removeSpinner(spinner: sv)
                    return
                }
                
                self.addrsList.removeAll(keepingCapacity: false)
                
                // unsaved data
                if self.modePengirim! {
                    if DataPengirimVC.address != nil {
                        self.addrsList.append(DataPengirimVC.address!)
                    }
                } else {
                    if DataPenerimaVC.address != nil {
                        self.addrsList.append(DataPenerimaVC.address!)
                    }
                }
                
                let addrs = E.getDataAsArr(json_data: json_data)
                for addr in addrs {
                    let m = ModAddr(any: addr)
                    if m.error {
                        E.showUIError(vc: self, message: "Error while parsing addresses response")
                        return
                    } else {
                        self.addrsList.append(m)
                    }
                }
                
                UIViewController.removeSpinner(spinner: sv)
                self.showData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoDataPengirimVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataPengirimVCID") as! DataPengirimVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoDataPenerimaVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataPenerimaVCID") as! DataPenerimaVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoNextPage(){
        if self.modePengirim! {
            self.setDataPengirimToModel(addr: self.addrsList[self.currentSelectedAddrs])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataPengirimVCID") as! DataPengirimVC
            vc.existingAddrDataToShow = self.addrsList[self.currentSelectedAddrs]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.setDataPenerimaToModel(addr: self.addrsList[self.currentSelectedAddrs])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataPenerimaVCID") as! DataPenerimaVC
            vc.existingAddrDataToShow = self.addrsList[self.currentSelectedAddrs]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setDataPenerimaToModel(addr: ModAddr){
        let model                       = KirimVC.model_data
        model.consignee_first_name      = addr.first_name
        model.consignee_last_name       = addr.last_name!
        model.consignee_mobile_phone    = addr.mobile_phone_no
        model.consignee_address         = addr.address
        model.consignee_address_detail  = addr.address_detail!
        model.consignee_postal_code     = addr.postal_code
        model.id_consignee_district     = addr.id_district
    }
    
    func setDataPengirimToModel(addr: ModAddr){
        let model                       = KirimVC.model_data
        model.shipper_first_name        = addr.first_name
        model.shipper_last_name         = addr.last_name!
        model.shipper_mobile_phone      = addr.mobile_phone_no
        model.shipper_address           = addr.address
        model.shipper_address_detail    = addr.address_detail!
        model.shipper_postal_code       = addr.postal_code
        model.id_shipper_district       = addr.id_district
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func showSelectedData(){
        let addr = self.addrsList[self.currentSelectedAddrs]
        self.currentSelectedAddrsID = addr.id!
        self.removeImage.isHidden = self.currentSelectedAddrsID < 0
        self.textFieldAlamat.text = addr.keterangan_tempat
        self.labelNamaPengirim.text = addr.first_name! + " " + addr.last_name!
        self.labelNomorTelp.text = addr.mobile_phone_no!
        self.labelAlamat.text = addr.address! + "\n" + addr.address_detail!
    }
    
    @objc func doneAddrsPicker(){
        //
        self.currentSelectedAddrs = addrsPicker.selectedRow(inComponent: 0)
        self.showSelectedData()
        _ = self.textFieldAlamat.resignFirstResponder()
    }
    
    @objc func cancelAddrsPicker(){
        //
        _ = self.textFieldAlamat.resignFirstResponder()
    }

}

extension DataAlamatVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func createAddrsPicker(){
        self.assignPicker(delegateTo: self, pickerView: self.addrsPicker, inputField: textFieldAlamat,
                          action_done: #selector(DataAlamatVC.doneAddrsPicker),
                          action_cancel: #selector(DataAlamatVC.cancelAddrsPicker))
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return addrsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return addrsList[row].keterangan_tempat
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}

extension DataAlamatVC {
    
    private func call_remove_addr(id: Int, callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "id": id
        ]
        
        Alamofire.request(KirimRouter.remove_fav_addr(parameters: param)).responseJSON(completionHandler: callback)
    }
    
    func remove_addr(vcsender: UIViewController, id: Int, completion: (() -> ())?){
        let sv = UIViewController.displaySpinner(onView: vcsender.view)
        self.call_remove_addr(id: id) { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: vcsender, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // we dont care with the result
            UIViewController.removeSpinner(spinner: sv)
            E.showUISuccess(vc: vcsender, message: "Alamat berhasil dihapus", completion: {
                if completion != nil {
                    completion!()
                }
            });
        }
    }
    
    func alertRemoveAddr(completion: (() -> ())?){
        let finalTitle = ""
        let finalMessage = "Apakah Anda yakin ingin menghapus data alamat ini"
        
        let refreshAlert = UIAlertController(title: finalTitle, message: finalMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "YA", style: .default, handler: { (action: UIAlertAction!) in
            self.remove_addr(vcsender: self, id: self.currentSelectedAddrsID, completion: completion)
        }))
        refreshAlert.addAction(UIAlertAction(title: "TIDAK", style: .cancel, handler: nil))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
}

class ModAddr {
    var id                      : Int?      = nil
    var is_pengirim_penerima    : Int?      = nil
    var id_member               : Int?      = nil
    var keterangan_tempat       : String?   = nil
    var first_name              : String?   = nil
    var last_name               : String?   = nil
    var mobile_phone_no         : String?   = nil
    var address                 : String?   = nil
    var address_detail          : String?   = nil
    var id_province             : Int?      = nil
    var id_city                 : Int?      = nil
    var id_district             : Int?      = nil
    var postal_code             : String?   = nil
    var province                : String?   = nil
    var district                : String?   = nil
    var error                   : Bool      = false
    
    init(){}
    
    init(any: Any? = nil) {
        if any == nil {
            self.error = true
            return
        }
        
        let kvs = any as! [String: Any]
        if self.anyerror(kvs: kvs, keys: ["id", "is_pengirim_penerima", "id_member", "keterangan_tempat", "first_name", "last_name", "mobile_phone_no", "address", "address_detail", "id_province", "id_city", "id_district", "postal_code", "created_at", "updated_at", "province", "district"]) {
            self.error = true
            return
        }
        print(kvs.debugDescription)
        self.id = kvs["id"] as? Int
        self.is_pengirim_penerima = kvs["is_pengirim_penerima"] as? Int
        self.id_member = kvs["id_member"] as? Int
        self.keterangan_tempat = kvs["keterangan_tempat"] as? String
        self.first_name = kvs["first_name"] as? String
        self.last_name = kvs["last_name"] as? String
        self.mobile_phone_no = kvs["mobile_phone_no"] as? String
        self.address = kvs["address"] as? String
        self.address_detail = kvs["address_detail"] as? String
        self.id_province = kvs["id_province"] as? Int
        self.id_city = kvs["id_city"] as? Int
        self.id_district = kvs["id_district"] as? Int
        self.postal_code = kvs["postal_code"] as? String
        self.province = kvs["province"] as? String
        self.district = kvs["district"] as? String
    }
    
    func asParameters() -> Parameters {
        var kvs: Parameters = [:]
        kvs["is_pengirim_penerima"] = self.is_pengirim_penerima
        kvs["id_member"] = self.id_member
        kvs["keterangan_tempat"] = self.keterangan_tempat
        kvs["first_name"] = self.first_name
        kvs["last_name"] = self.last_name
        kvs["mobile_phone_no"] = self.mobile_phone_no
        kvs["address"] = self.address
        kvs["address_detail"] = self.address_detail
        kvs["id_province"] = self.id_province
        kvs["id_city"] = self.id_city
        kvs["id_district"] = self.id_district
        kvs["postal_code"] = self.postal_code
        
        return kvs
    }
    
    func anyerror(kvs: [String: Any], keys: [String]) -> Bool {
        for key in keys {
            if kvs[key] == nil {
                return true
            }
        }
        
        return false
    }
}
