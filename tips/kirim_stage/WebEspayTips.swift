//
//  WebEspayTips.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 24/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import WebKit

class WebEspayTips: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var url = "https://tips.co.id"
    public static var idPayment = ""
    
    public static func getViewController(url: String, payment_id: String) -> UIViewController{
        let vc = WebEspayTips()
        vc.url = url
        WebEspayTips.idPayment = payment_id
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect.init(origin: CGPoint.zero,
                                      size: CGSize.init(width: self.view.frame.width,
                                                        height: self.view.frame.height))
        self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(webView)
        webView.navigationDelegate = self
        
        let button = UIButton(frame: CGRect(x: 16, y: 16, width: 100, height: 50))
        button.backgroundColor = UIColor.red
        button.setTitle("Cancel", for: .normal)
        button.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        
        self.view.addSubview(button)
        
        webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)


        let myURL = URL(string: self.url)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url) {
            print("### URL:", self.webView.url!)
            let urlString: String = self.webView.url!.absoluteString
            if urlString == "https://tips.co.id/" {
                // back to previous stage
                self.navigationController?.popViewController(animated: true)
            }
            /* // long old story
            if urlString == "tips://berhasil" {
                WebEspayTips.statusPaymentFinish = true
                // back to previous stage
                self.navigationController?.popViewController(animated: true)
            } else if urlString == "tips://gagal" {
                WebEspayTips.statusPaymentSuccess = false
                WebEspayTips.statusPaymentMessage = urlString
                self.navigationController?.popViewController(animated: true)
            }
            */
        }
        
        if keyPath == #keyPath(WKWebView.estimatedProgress) {
            // When page load finishes. Should work on each page reload.
            if (self.webView.estimatedProgress == 1) {
                print("### EP:", self.webView.estimatedProgress)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
