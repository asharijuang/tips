//
//  DataPengirimVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 19/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class DataPengirimVC: KirimVCFamily {

    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var scrollContainer: UIScrollView!
    @IBOutlet weak var temukanLokasiBackground: UIView!
    @IBOutlet weak var keteranganTempatField: TextField!
    @IBOutlet weak var namaDepanField: TextField!
    @IBOutlet weak var namaBelakangField: TextField!
    @IBOutlet weak var noHpField: TextField!
    @IBOutlet weak var alamatField: TextField!
    @IBOutlet weak var catatanAlamatField: TextField!
    @IBOutlet weak var provinsiField: TextField!
    @IBOutlet weak var kotaField: TextField!
    @IBOutlet weak var kecamatanField: TextField!
    @IBOutlet weak var kodePosField: TextField!
    @IBOutlet weak var lanjutButton: UIButton!
    @IBOutlet weak var simpanAlamatToggle: UIToggleCheck!
    
    static var address: ModAddr?
    
    let provincePicker      = UIPickerView()
    let cityPicker          = UIPickerView()
    let subdistrictPicker   = UIPickerView()
    
    var provinceList        : [SLocProvince]    = []
    var cityList            : [SLocCity]        = []
    var subdistrictList     : [SLocSubdistrict] = []
    
    var existingAddrDataToShow: ModAddr? = nil
    
    override func viewDidAppear(_ animated: Bool) {
        if self.existingAddrDataToShow != nil {
            let addr = self.existingAddrDataToShow!
            self.keteranganTempatField.text = addr.keterangan_tempat
            self.namaDepanField.text = addr.first_name
            self.namaBelakangField.text = addr.last_name
            self.noHpField.text = addr.mobile_phone_no
            self.alamatField.text = addr.address
            self.catatanAlamatField.text = addr.address_detail
            self.kodePosField.text = addr.postal_code
            
            self.provinsiField.text = S.getProvinceById(id: addr.id_province!).name
            self.kotaField.text = S.getCityById(id: addr.id_city!).name
            self.kecamatanField.text = S.getSubdistrictById(id: addr.id_district!).name
            
//            let bottomOffset = CGPoint(x: 0, y: self.scrollContainer.contentSize.height - self.scrollContainer.bounds.size.height)
//            self.scrollContainer.setContentOffset(bottomOffset, animated: true)
            KirimVC.model_data.id_shipper_district = addr.id_district!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.restyleUIComponents()
        backImage.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        self.temukanLokasiBackground.addTapGestureRecognizer {
            self.gotoMap()
        }
        lanjutButton.addTapGestureRecognizer {
            if self.existingAddrDataToShow == nil {
                self.storeAddress()
                if !self.validateFieldOK(views: [
                    DataPengirimVC.address?.keterangan_tempat,
                    DataPengirimVC.address?.first_name,
                    DataPengirimVC.address?.last_name,
                    DataPengirimVC.address?.mobile_phone_no,
                    DataPengirimVC.address?.address,
//                    DataPengirimVC.address?.address_detail, // notes bisa gak diisi
                    DataPengirimVC.address?.id_district,
                    DataPengirimVC.address?.id_city,
                    DataPengirimVC.address?.id_province,
                    DataPengirimVC.address?.postal_code
                    ]){
                    self.showErrorTextField()
                    self.catatanAlamatField.hideError()
                    // do something
                    return
                }
            }
            
            self.setUIDataToModel()
            if !self.validateFieldOK(views: [
                KirimVC.model_data.shipper_keterangan_tempat_pengirim,
                KirimVC.model_data.shipper_first_name,
                KirimVC.model_data.shipper_last_name,
                KirimVC.model_data.shipper_mobile_phone,
                KirimVC.model_data.shipper_address,
//                KirimVC.model_data.shipper_address_detail, // notes bisa gak diisi
                KirimVC.model_data.shipper_postal_code
                ]){
                self.showErrorTextField()
                self.catatanAlamatField.hideError()
                // do something
                return
            }
            
            self.hideErrorTextField()
            
            if self.simpanAlamatToggle.getToggleState() && self.existingAddrDataToShow == nil {
                KirimVC.model_data.savePengirim = true
            }
            
            self.gotoAlamatPenerimaVC()
        }
        // Do any additional setup after loading the view.
    }
    
    func restyleUIComponents(){
        scrollContainer.layer.masksToBounds = true
        scrollContainer.layer.cornerRadius = 10
        temukanLokasiBackground.layer.cornerRadius = 6
        
        keteranganTempatField.layer.cornerRadius = 6
        namaDepanField.layer.cornerRadius = 6
        namaBelakangField.layer.cornerRadius = 6
        noHpField.layer.cornerRadius = 6
        noHpField.setInputPhoneNumber()
        alamatField.layer.cornerRadius = 6
        catatanAlamatField.layer.cornerRadius = 6
        provinsiField.layer.cornerRadius = 6
        kotaField.layer.cornerRadius = 6
        kecamatanField.layer.cornerRadius = 6
        kodePosField.layer.cornerRadius = 6
        kodePosField.setInputOnlyNumber()
        kodePosField.setMaxInputLength(max: 5)
        provinsiField.showArrow()
        kotaField.showArrow()
        kecamatanField.showArrow()
        ViewController.setOLabel(button: lanjutButton, text: "Lanjut", filled: true, reverseColor: true)
        self.createProvincePicker()
        
        self.is_full_addr(vcsender: self) { (isFull) in
            let realFull = isFull != nil && isFull!
            self.simpanAlamatToggle.setupXib(text: "Simpan data pengirim barang\( realFull ? "\n(Max 10 data)" : "")", state: !realFull, isChangeable: !realFull)
        }
        
        if !C().isGuest() {
            self.namaDepanField.text = HomeVC.user.first_name!
            self.namaBelakangField.text = HomeVC.user.last_name!
            self.noHpField.text = HomeVC.user.mobile_phone_no!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gotoDataPenerimaVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataPenerimaVCID") as! DataPenerimaVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoAlamatPenerimaVC(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DataAlamatVCID") as! DataAlamatVC
        vc.modePengirim = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoMap(){
        let storyboard = UIStoryboard(name: "Maps", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapsVCID") as! MapsViewController
        vc.success_callback = {lat, lng, location in
            self.alamatField.text = location
            KirimVC.model_data.shipper_longitude = lng
            KirimVC.model_data.shipper_latitude = lat
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func setProvinceData(id: Int? = nil){
        // do nothing
    }
    
    func setCityData(id: Int? = nil){
        // do nothing
    }
    
    func setSubdistrictData(id: Int? = nil){
        KirimVC.model_data.id_shipper_district = id
    }
    
    func storeAddress(){
        DataPengirimVC.address = ModAddr(any: nil)
        DataPengirimVC.address?.id                  = -1
        DataPengirimVC.address?.is_pengirim_penerima = 1
        DataPengirimVC.address?.id_member          = HomeVC.user.id!
        DataPengirimVC.address?.keterangan_tempat  = keteranganTempatField.text
        DataPengirimVC.address?.first_name         = namaDepanField.text
        DataPengirimVC.address?.last_name          = namaBelakangField.text
        DataPengirimVC.address?.mobile_phone_no    = noHpField.text
        DataPengirimVC.address?.address            = alamatField.text
        DataPengirimVC.address?.address_detail     = catatanAlamatField.text
        DataPengirimVC.address?.postal_code        = kodePosField.text
        
        if !kecamatanField.text!.isEmpty {
            let index = subdistrictPicker.selectedRow(inComponent: 0)
            let subdistrict = subdistrictList[index]
            DataPengirimVC.address?.id_province        = subdistrict.id_province
            DataPengirimVC.address?.id_city            = subdistrict.id_city
            DataPengirimVC.address?.id_district        = subdistrict.id
        }
    }
    
    func setUIDataToModel(){
        let model                                   = KirimVC.model_data
        model.shipper_keterangan_tempat_pengirim    = keteranganTempatField.text
        model.shipper_first_name                    = namaDepanField.text
        model.shipper_last_name                     = namaBelakangField.text!
        model.shipper_mobile_phone                  = noHpField.text
        model.shipper_address                       = alamatField.text
        model.shipper_address_detail                = catatanAlamatField.text!
        model.shipper_postal_code                   = kodePosField.text
    }
    
    func unsetProvinceCitySubdistrictData(){
        KirimVC.model_data.id_shipper_district = nil
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func doneProvincePicker(){
        //
        let province = provinceList[provincePicker.selectedRow(inComponent: 0)]
        // KirimVC.model_data.berat_estimasi = berat.value
        provinsiField.text = province.name
        _ = provinsiField.resignFirstResponder()
        
        kotaField.text = ""
        kecamatanField.text = ""
        
        // NOTE
        // - unset seluruh provinsi, kota, dan kecamatan dilakukan karena
        //   data yang dibutuhkan hanya id kecamatan saja (memiliki relasi
        //   ke kota dan provinsi)
        // - juga tidak dilakukan set id provinsi karena tidak dibutuhkan
        self.unsetProvinceCitySubdistrictData()
        self.createCityPicker(id_province: province.id!)
    }
    
    @objc func cancelProvincePicker(){
        //
//        if KirimVC.model_data.berat_estimasi == nil {
//            beratField.text = ""
//        }
        _ = provinsiField.resignFirstResponder()
    }
    
    
    @objc func doneCityPicker(){
        //
        let city = cityList[cityPicker.selectedRow(inComponent: 0)]
        // KirimVC.model_data.berat_estimasi = berat.value
        kotaField.text = city.name
        _ = kotaField.resignFirstResponder()
        
        kecamatanField.text = ""
        
        // NOTE
        // - unset seluruh provinsi, kota, dan kecamatan dilakukan karena
        //   data yang dibutuhkan hanya id kecamatan saja (memiliki relasi
        //   ke kota dan provinsi)
        // - juga tidak dilakukan set id kota karena tidak dibutuhkan
        self.unsetProvinceCitySubdistrictData()
        self.createSubdistrictPicker(id_city: city.id!)
    }
    
    @objc func cancelCityPicker(){
        //
        //        if KirimVC.model_data.berat_estimasi == nil {
        //            beratField.text = ""
        //        }
        _ = kotaField.resignFirstResponder()
    }
    
    
    @objc func doneSubdistrictPicker(){
        //
        let subdistrict = subdistrictList[subdistrictPicker.selectedRow(inComponent: 0)]
        // KirimVC.model_data.berat_estimasi = berat.value
        kecamatanField.text = subdistrict.name
        self.setSubdistrictData(id: subdistrict.id)
        _ = kecamatanField.resignFirstResponder()
    }
    
    @objc func cancelSubdistrictPicker(){
        //
        //        if KirimVC.model_data.berat_estimasi == nil {
        //            beratField.text = ""
        //        }
        _ = kecamatanField.resignFirstResponder()
    }
}

extension DataPengirimVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func createProvincePicker(){
        provinceList.removeAll(keepingCapacity: false)
        for s in S.location_province {
            provinceList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: provincePicker, inputField: provinsiField,
                          action_done: #selector(DataPengirimVC.doneProvincePicker),
                          action_cancel: #selector(DataPengirimVC.cancelProvincePicker))
    }
    
    func createCityPicker(id_province: Int){
        cityList.removeAll(keepingCapacity: false)
        for s in S.getLocationCity(id_province: id_province) {
            cityList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: cityPicker, inputField: kotaField,
                          action_done: #selector(DataPengirimVC.doneCityPicker),
                          action_cancel: #selector(DataPengirimVC.cancelCityPicker))
    }
    
    func createSubdistrictPicker(id_city: Int){
        subdistrictList.removeAll(keepingCapacity: false)
        for s in S.getLocationSubdistrict(id_city: id_city) {
            subdistrictList.append(s)
        }
        self.assignPicker(delegateTo: self, pickerView: subdistrictPicker, inputField: kecamatanField,
                          action_done: #selector(DataPengirimVC.doneSubdistrictPicker),
                          action_cancel: #selector(DataPengirimVC.cancelSubdistrictPicker))
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == provincePicker {
            return provinceList.count
        }
        if pickerView == cityPicker {
            return cityList.count
        }
        if pickerView == subdistrictPicker {
            return subdistrictList.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == provincePicker {
            return provinceList[row].name
        }
        if pickerView == cityPicker {
            return cityList[row].name
        }
        if pickerView == subdistrictPicker {
            return subdistrictList[row].name
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}


extension DataPengirimVC {
    
    private func call_store_addr(callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = DataPengirimVC.address!.asParameters()
        Alamofire.request(KirimRouter.add_fav_addr(parameters: param)).responseJSON(completionHandler: callback)
    }
    
    func store_addr(vcsender: UIViewController, completion: (() -> ())?){
        let sv = UIViewController.displaySpinner(onView: vcsender.view)
        self.call_store_addr { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: vcsender, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // we dont care with the result
            UIViewController.removeSpinner(spinner: sv)
            E.showUISuccess(vc: vcsender, message: "Alamat berhasil ditambahkan", completion: {
                if completion != nil {
                    completion!()
                }
            });
        }
    }
}


extension DataPengirimVC {
    
    private func call_is_full_addr(callback: @escaping (DataResponse<Any>) -> Void){
        let param: Parameters = [
            "member_id": HomeVC.user.id!,
            "is_pengirim": true
        ]
        Alamofire.request(KirimRouter.is_full_fav_addr(parameters: param)).responseJSON(completionHandler: callback)
    }
    
    func is_full_addr(vcsender: UIViewController, completion: ((_ isFull: Bool?) -> ())?){
        let sv = UIViewController.displaySpinner(onView: vcsender.view)
        self.call_is_full_addr { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: vcsender, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            let isFull = E.getDataAsBool(json_data: json_data)
            
            // we dont care with the result
            UIViewController.removeSpinner(spinner: sv)
            if completion != nil {
                completion!(isFull)
            }
        }
    }
}
