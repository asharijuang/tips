//
//  MasterDB.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 11/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

class MasterDB {
    public static var DBINSTANCE: Connection?
    public static var DBVersion: Int = 2
    
    public static func db() -> Connection? {
        if MasterDB.DBINSTANCE != nil {
            return MasterDB.DBINSTANCE
        }
        
        do {
            let databaseFileName = "tips-db.sqlite3"
            let databaseFilePath = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!)/\(databaseFileName)"
            MasterDB.DBINSTANCE = try Connection(databaseFilePath)
            return MasterDB.DBINSTANCE
        } catch {
            print(error)
            return nil
        }
    }
    
    public func createAllTables(){
        let db = MasterDB.db()
        _ = self.createStatus(db: db)
        _ = self.createAirport(db: db)
        _ = self.createAddtInfo(db: db)
        _ = self.createDelivery(db: db)
        _ = self.createsShipments(db: db)
        _ = self.createShipmentsData(db: db)
        _ = self.createDeliveriesData(db: db)
        _ = self.createProfile(db: db)
        _ = self.createImageSQL(db: db)
        _ = self.doMigrationIfNeeded()
    }
    
    public func doMigrationIfNeeded(){
        let ver = MasterDB.db()?.userVersion
        if ver! < MasterDB.DBVersion {
            migrate(version: MasterDB.DBVersion)
            MasterDB.db()?.userVersion = Int32(MasterDB.DBVersion)
        }
    }
    
    public func migrate(version: Int){
        switch version {
        case 2:
            _ = self.migrationV1(db: MasterDB.db())
            _ = self.migrationV2(db: MasterDB.db())
            break;
        case 1:
            _ = self.migrationV1(db: MasterDB.db())
            break;
        default:
            break
        }
    }
    
    public func resetShipments(){
        do {
            try MasterDB.db()?.run(MasterDB.Delivery_Goods._table.delete())
            try MasterDB.db()?.run(MasterDB.Goods._table.delete())
            try MasterDB.db()?.run(MasterDB.Statuses._table.delete())
            try MasterDB.db()?.run(MasterDB.Airports._table.delete())
            try MasterDB.db()?.run(MasterDB.AddtInfos._table.delete())
            try MasterDB.db()?.run(MasterDB.Deliveries._table.delete())
            try MasterDB.db()?.run(MasterDB.Shipments._table.delete())
            try MasterDB.db()?.run(MasterDB.ShipmentData._table.delete())
            try MasterDB.db()?.run(MasterDB.DeliveryData._table.delete())
        } catch {
            print(error)
        }
    }
    
    public func resetAndInsertShipments(shipments: [Any], deliveries: [Any]){
        
        self.resetShipments()
        
        for shipment in shipments {
            _ = MasterDB.ShipmentData.insert(kvJSON: shipment as! [String: Any])
        }
        
        for delivery in deliveries {
            _ = MasterDB.DeliveryData.insert(kvJSON: delivery as! [String: Any])
        }
    }
    
    public func insertSingleUser(user: [String: Any]){
        self.deleteSingleUser()
        _ = MasterDB.Profile.insert(kvJSON: user)
    }
    
    public func getSingleUser() -> [String: Any]{
        return MasterDB.Profile.getSingleAndRaw()
    }
    
    public func updateBalance(money: Int64) -> Bool{
        print(money)
        return MasterDB.Profile.update(money: money)
    }
    
    public func storeImage(name: String, url: String, data: UIImage) {
        let imageData: Data = UIImagePNGRepresentation(data)!
        let strBase64 = imageData.base64EncodedString()
        _ = MasterDB.ImageSQL.insert(kvJSON: [
            "name": name,
            "url": url,
            "data": strBase64
        ])
    }
    
    public func getImage(name: String) -> UIImage? {
        let imageRowData = MasterDB.ImageSQL.getByNameAndRaw(name: name)
        print("imagerowdata")
        if imageRowData.count == 0 {
            return nil
        }
        
        let base64Data = String(imageRowData["data"] as! String)
        let dataDecoded: Data? = Data(base64Encoded: base64Data)
        print("dataDecoded")
        if dataDecoded == nil {
            return nil
        }
        
        print("UIImage")
        return UIImage(data: dataDecoded!)!
    }
    
    public func deleteSingleUser(){
        do {
            try MasterDB.db()?.run(MasterDB.Profile._table.delete())
        } catch {
            print(error)
        }
    }
    
    public func getAllShipments() -> [Any]{
        var out: [Any] = []
        do {
            let shipmentsdatas = try MasterDB.db()?.prepare(MasterDB.ShipmentData._table.select(MasterDB.ShipmentData._table[*]))
            for shipmentdata in shipmentsdatas! {
                out.append(MasterDB.ShipmentData.getByIdAndRaw(__id: try shipmentdata.get(MasterDB.ShipmentData.id)))
            }
        } catch {
            print(error)
        }
        
        return out
    }
    
    public func getAllDeliveries() -> [Any]{
        var out: [Any] = []
        do {
            let deliveriesdatas = try MasterDB.db()?.prepare(MasterDB.DeliveryData._table.select(MasterDB.DeliveryData._table[*]))
            for deliverydata in deliveriesdatas! {
                out.append(MasterDB.DeliveryData.getByIdAndRaw(__id: try deliverydata.get(MasterDB.DeliveryData.id)))
            }
        } catch {
            print(error)
        }
        
        return out
    }
    
    public func createAirport(db: Connection?) -> Table {
        let airport = MasterDB.Airports._table
        do {
            try db?.run(airport.create(ifNotExists: true) { t in
                t.column(MasterDB.Airports.id, primaryKey: true)
                t.column(MasterDB.Airports.id_city)
                t.column(MasterDB.Airports.name)
                t.column(MasterDB.Airports.initial_code)
                t.column(MasterDB.Airports.status)
            })
        } catch {
            print(error)
        }
        
        return airport
    }
    
    public func createAddtInfo(db: Connection?) -> Table {
        let addtInfo = MasterDB.AddtInfos._table
        do {
            try db?.run(addtInfo.create(ifNotExists: true) { t in
                t.column(MasterDB.AddtInfos.id, primaryKey: true)
                t.column(MasterDB.AddtInfos.airline_name)
                t.column(MasterDB.AddtInfos.flight_code)
                t.column(MasterDB.AddtInfos.kode_bandara_asal)
                t.column(MasterDB.AddtInfos.kode_bandara_tujuan)
            })
        } catch {
            print(error)
        }
        
        return addtInfo
    }
    
    public func createStatus(db: Connection?) -> Table {
        let status = MasterDB.Statuses._table
        do {
            try db?.run(status.create(ifNotExists: true) { t in
                t.column(MasterDB.Statuses.id, primaryKey: true)
                t.column(MasterDB.Statuses.description)
                t.column(MasterDB.Statuses.detail)
                t.column(MasterDB.Statuses.step)
            })
        } catch {
            print(error)
        }
        
        return status
    }
    
    public func createGoods(db: Connection?) -> Table {
        let status = MasterDB.Goods._table
        do {
            try db?.run(status.create(ifNotExists: true) { t in
                t.column(MasterDB.Goods.id, primaryKey: true)
                t.column(MasterDB.Goods.shipment_contents)
                t.column(MasterDB.Goods.real_weight)
            })
        } catch {
            print(error)
        }
        
        return status
    }
    
    public func createDelivery_Goods(db: Connection?) -> Table {
        let status = MasterDB.Delivery_Goods._table
        do {
            try db?.run(status.create(ifNotExists: true) { t in
                t.column(MasterDB.Delivery_Goods.id, primaryKey: true)
                t.column(MasterDB.Delivery_Goods.id_goods)
                t.column(MasterDB.Delivery_Goods.id_delivery)
            })
        } catch {
            print(error)
        }
        
        return status
    }
    
    public func createDelivery(db: Connection?) -> Table {
        let delivery = MasterDB.Deliveries._table
        do {
            try db?.run(delivery.create(ifNotExists: true) { t in
                t.column(MasterDB.Deliveries.id, primaryKey: true)
                t.column(MasterDB.Deliveries.baggage_space)
                t.column(MasterDB.Deliveries.booking_code)
                t.column(MasterDB.Deliveries.depature)
                t.column(MasterDB.Deliveries.destination_city)
                t.column(MasterDB.Deliveries.detail_status)
                t.column(MasterDB.Deliveries.first_name)
                t.column(MasterDB.Deliveries.flight_code)
                t.column(MasterDB.Deliveries.id_airline)
                t.column(MasterDB.Deliveries.id_destination_airport)
                t.column(MasterDB.Deliveries.id_destination_city)
                t.column(MasterDB.Deliveries.id_member)
                t.column(MasterDB.Deliveries.id_origin_airport)
                t.column(MasterDB.Deliveries.id_origin_city)
                t.column(MasterDB.Deliveries.id_slot_status)
                t.column(MasterDB.Deliveries.id_wallet_transaction)
                t.column(MasterDB.Deliveries.last_name)
                t.column(MasterDB.Deliveries.origin_city)
                t.column(MasterDB.Deliveries.photo_tag)
                t.column(MasterDB.Deliveries.slot_id)
                t.column(MasterDB.Deliveries.slot_price_kg)
                t.column(MasterDB.Deliveries.sold_baggage_space)
                t.column(MasterDB.Deliveries.status_bayar)
                t.column(MasterDB.Deliveries.status_dispatch)
            })
        } catch {
            print(error)
        }
        
        return delivery
    }
    
    public func createsShipments(db: Connection?) -> Table{
        let shipments = MasterDB.Shipments._table
        do {
            try db?.run(shipments.create(ifNotExists: true) { t in
                t.column(MasterDB.Shipments.id, primaryKey: true)
                t.column(MasterDB.Shipments.add_insurance_cost)
                t.column(MasterDB.Shipments.add_notes)
                t.column(MasterDB.Shipments.consignee_address)
                t.column(MasterDB.Shipments.consignee_address_detail)
                t.column(MasterDB.Shipments.consignee_city)
                t.column(MasterDB.Shipments.consignee_districts)
                t.column(MasterDB.Shipments.consignee_first_name)
                t.column(MasterDB.Shipments.consignee_last_name)
                t.column(MasterDB.Shipments.consignee_latitude)
                t.column(MasterDB.Shipments.consignee_longitude)
                t.column(MasterDB.Shipments.consignee_mobile_phone)
                t.column(MasterDB.Shipments.consignee_postal_code)
                t.column(MasterDB.Shipments.consignee_province)
                t.column(MasterDB.Shipments.delivered_by)
                t.column(MasterDB.Shipments.delivered_date)
                t.column(MasterDB.Shipments.delivered_time)
                t.column(MasterDB.Shipments.destination_city)
                t.column(MasterDB.Shipments.detail_status)
                t.column(MasterDB.Shipments.dispatch_type)
                t.column(MasterDB.Shipments.estimate_goods_value)
                t.column(MasterDB.Shipments.estimate_weight)
                t.column(MasterDB.Shipments.flight_cost)
                t.column(MasterDB.Shipments.goods_status)
                t.column(MasterDB.Shipments.id_consignee_city)
                t.column(MasterDB.Shipments.id_consignee_districts)
                t.column(MasterDB.Shipments.id_consignee_province)
                t.column(MasterDB.Shipments.id_destination_city)
                t.column(MasterDB.Shipments.id_device)
                t.column(MasterDB.Shipments.id_office)
                t.column(MasterDB.Shipments.id_origin_city)
                t.column(MasterDB.Shipments.id_packaging)
                t.column(MasterDB.Shipments.id_payment_type)
                t.column(MasterDB.Shipments.id_shipment_status)
                t.column(MasterDB.Shipments.id_shipper)
                t.column(MasterDB.Shipments.id_shipper_city)
                t.column(MasterDB.Shipments.id_shipper_districts)
                t.column(MasterDB.Shipments.id_shipper_province)
                t.column(MasterDB.Shipments.id_slot)
                t.column(MasterDB.Shipments.id_wallet_transaction)
                t.column(MasterDB.Shipments.insurance_cost)
                t.column(MasterDB.Shipments.is_add_insurance)
                t.column(MasterDB.Shipments.is_delivery)
                t.column(MasterDB.Shipments.is_first_class)
                t.column(MasterDB.Shipments.is_matched)
                t.column(MasterDB.Shipments.is_online)
                t.column(MasterDB.Shipments.is_online_payment)
                t.column(MasterDB.Shipments.is_posted)
                t.column(MasterDB.Shipments.is_take)
                t.column(MasterDB.Shipments.origin_city)
                t.column(MasterDB.Shipments.packaging_date)
                t.column(MasterDB.Shipments.packaging_time)
                t.column(MasterDB.Shipments.payment_id)
                t.column(MasterDB.Shipments.photo_ktp)
                t.column(MasterDB.Shipments.photo_signature)
                t.column(MasterDB.Shipments.pickup_by)
                t.column(MasterDB.Shipments.pickup_date)
                t.column(MasterDB.Shipments.pickup_signature)
                t.column(MasterDB.Shipments.pickup_status)
                t.column(MasterDB.Shipments.pickup_time)
                t.column(MasterDB.Shipments.real_weight)
                t.column(MasterDB.Shipments.received_by)
                t.column(MasterDB.Shipments.received_image)
                t.column(MasterDB.Shipments.received_time)
                t.column(MasterDB.Shipments.registration_type)
                t.column(MasterDB.Shipments.shipment_contents)
                t.column(MasterDB.Shipments.shipment_id)
                t.column(MasterDB.Shipments.shipper_address)
                t.column(MasterDB.Shipments.shipper_address_detail)
                t.column(MasterDB.Shipments.shipper_city)
                t.column(MasterDB.Shipments.shipper_districts)
                t.column(MasterDB.Shipments.shipper_first_name)
                t.column(MasterDB.Shipments.shipper_last_name)
                t.column(MasterDB.Shipments.shipper_latitude)
                t.column(MasterDB.Shipments.shipper_longitude)
                t.column(MasterDB.Shipments.shipper_mobile_phone)
                t.column(MasterDB.Shipments.shipper_postal_code)
                t.column(MasterDB.Shipments.shipper_province)
                t.column(MasterDB.Shipments.status_dispatch)
                t.column(MasterDB.Shipments.transaction_date)
            })
        } catch {
            print(error)
        }
        
        return shipments
    }
    
    public func createShipmentsData(db: Connection?) -> Table {
        let shipmentsData = MasterDB.ShipmentData._table
        do {
            try db?.run(shipmentsData.create(ifNotExists: true) { t in
                t.column(MasterDB.ShipmentData.id, primaryKey: true)
                t.column(MasterDB.ShipmentData.id_addt_info)
                t.column(MasterDB.ShipmentData.id_shipment)
                t.column(MasterDB.ShipmentData.id_status)
            })
        } catch {
            print(error)
        }
        
        return shipmentsData
    }
    
    public func createDeliveriesData(db: Connection?) -> Table {
        let deliveriesData = MasterDB.DeliveryData._table
        do {
            try db?.run(deliveriesData.create(ifNotExists: true) { t in
                t.column(MasterDB.DeliveryData.id, primaryKey: true)
                t.column(MasterDB.DeliveryData.id_addt_info)
                t.column(MasterDB.DeliveryData.id_delivery)
                t.column(MasterDB.DeliveryData.id_status)
            })
        } catch {
            print(error)
        }
        
        return deliveriesData
    }
    
    public func createProfile(db: Connection?) -> Table {
        let profile = MasterDB.Profile._table
        do {
            try db?.run(profile.create(ifNotExists: true) { t in
                t.column(MasterDB.Profile.id)
                t.column(MasterDB.Profile.birth_date)
                t.column(MasterDB.Profile.id_city)
                t.column(MasterDB.Profile.last_name)
                t.column(MasterDB.Profile.mobile_phone_no)
                t.column(MasterDB.Profile.email)
                t.column(MasterDB.Profile.address)
                t.column(MasterDB.Profile.ref_code)
                t.column(MasterDB.Profile.profil_picture)
                t.column(MasterDB.Profile.money)
                t.column(MasterDB.Profile.first_name)
                t.column(MasterDB.Profile.id_office)
                t.column(MasterDB.Profile.sex)
            })
        } catch {
            print(error)
        }
        
        return profile
    }
    
    public func migrationV1(db: Connection?) -> Table {
        let profile = MasterDB.Profile._table
        do {
            try db?.run(profile.addColumn(MasterDB.Profile.sms_code, defaultValue: nil))
        } catch {
            print(error)
        }
        
        return profile
    }
    
    public func migrationV2(db: Connection?) {
        _ = self.createGoods(db: db)
        _ = self.createDelivery_Goods(db: db)
    }
    
    public func createImageSQL(db: Connection?) -> Table {
        let imageSQL = MasterDB.ImageSQL._table
        do {
            try db?.run(imageSQL.create(ifNotExists: true) { t in
                t.column(MasterDB.ImageSQL.id, primaryKey: true)
                t.column(MasterDB.ImageSQL.name)
                t.column(MasterDB.ImageSQL.url)
                t.column(MasterDB.ImageSQL.data)
            })
        } catch {
            print(error)
        }
        
        return imageSQL
    }
}

extension Connection {
    public var userVersion: Int32 {
        get { return Int32(try! scalar("PRAGMA user_version") as! Int64)}
        set { try! run("PRAGMA user_version = \(newValue)") }
    }
}
