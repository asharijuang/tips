//
//  ShipmentData.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class ShipmentData {
        public static var _table = Table("shipment_data")
        public static var id = Expression<Int64>("id")
        public static var id_addt_info = Expression<Int64>("id_addt_info")
        public static var id_shipment = Expression<Int64>("id_shipment")
        public static var id_status = Expression<Int64>("id_status")
        
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(" -- {{ shipment_data.getById }} -- ")
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                    ))
            } catch {
                print(" -- {{ shipment_data.getByIdAndRaw }} -- ")
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = try row?.get(id)
                out["id_addt_info"] = try row?.get(id_addt_info)
                out["id_shipment"] = try row?.get(id_shipment)
                out["id_status"] = try row?.get(id_status)
                out["addt_info"] = MasterDB.AddtInfos.getByIdAndRaw(__id: out["id_addt_info"] as! Int64)
                out["shipment"] = MasterDB.Shipments.getByIdAndRaw(__id: out["id_shipment"] as! Int64)
                out["status"] = MasterDB.Statuses.getByIdAndRaw(__id: out["id_status"] as! Int64)
            } catch {
                print(" -- {{ shipment_data.raw }} -- ")
                print(error)
            }
            
            return out
        }
        
        enum ShipmentDataError: Error {
            case exAddtInfos
            case exStatuses
            case exShipments
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            do {
                let addt_info_id_inserted = MasterDB.AddtInfos.insert(kvJSON: kvJSON["addt_info"] as! [String: Any])
                let status_id_inserted = MasterDB.Statuses.insert(kvJSON: kvJSON["status"] as! [String: Any])
                let shipment_id_inserted = MasterDB.Shipments.insert(kvJSON: kvJSON["shipment"] as! [String: Any])
                
                let rowId = try MasterDB.db()?.run(MasterDB.ShipmentData._table.insert(
                    MasterDB.ShipmentData.id_addt_info <- addt_info_id_inserted!,
                    MasterDB.ShipmentData.id_shipment <- shipment_id_inserted!,
                    MasterDB.ShipmentData.id_status <- status_id_inserted!
                ))
                
                return rowId
            } catch ShipmentDataError.exAddtInfos {
                print("shipmentdata.addt_info null")
                return nil
            } catch ShipmentDataError.exStatuses {
                print("shipmentdata.status null")
                return nil
            } catch ShipmentDataError.exShipments {
                print("shipmentdata.shipment null")
                return nil
            } catch {
                print(" -- {{ shipment_data.insert }} -- ")
                print(error)
                return nil
            }
        }
    }
}
