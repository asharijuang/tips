//
//  Profile.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class Profile {
        public static var _table = Table("profile")
        public static var id = Expression<Int64  >("id")
        public static var birth_date = Expression<String?>("birth_date") // optional
        public static var id_city = Expression<Int64?>("id_city") // optional
        public static var last_name = Expression<String?>("last_name") // optional
        public static var mobile_phone_no = Expression<String>("mobile_phone_no")
        public static var email = Expression<String?>("email")
        public static var address = Expression<String?>("address") // optional
        public static var ref_code = Expression<String?>("ref_code")
        public static var sms_code = Expression<String?>("sms_code")
        public static var profil_picture = Expression<String?>("profil_picture") // optional
        public static var money = Expression<Int64?>("money") // optional
        public static var first_name = Expression<String>("first_name")
        public static var id_office = Expression<String?>("id_office") // optional
        public static var sex = Expression<String?>("sex") // optional
        
        public static func getSingle() -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getSingleAndRaw() -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = (try row?.get(id))?.intValue()
                out["birth_date"] = try row?.get(birth_date)
                out["id_city"] = (try row?.get(id_city))?.intValue()
                out["last_name"] = try row?.get(last_name)
                out["mobile_phone_no"] = try row?.get(mobile_phone_no)
                out["email"] = try row?.get(email)
                out["address"] = try row?.get(address)
                out["ref_code"] = try row?.get(ref_code)
                out["sms_code"] = try row?.get(sms_code)
                out["profil_picture"] = try row?.get(profil_picture)
                out["money"] = (try row?.get(money))?.intValue()
                out["first_name"] = try row?.get(first_name)
                out["id_office"] = try row?.get(id_office)
                out["sex"] = try row?.get(sex)
            } catch {
                print(error)
            }
            
            return out
        }
        
        enum AirportsError: Error {
            case exAirports
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.Profile._table.insert(
                    MasterDB.Profile.id <- (kvJSON["id"] as? Int64)!,
                    MasterDB.Profile.birth_date <- (kvJSON["birth_date"] as? String), // optional
                    MasterDB.Profile.id_city <- (kvJSON["id_city"] as? Int64), // optional
                    MasterDB.Profile.last_name <- (kvJSON["last_name"] as? String), // optional
                    MasterDB.Profile.mobile_phone_no <- (kvJSON["mobile_phone_no"] as? String)!,
                    MasterDB.Profile.email <- (kvJSON["email"] as? String),
                    MasterDB.Profile.address <- (kvJSON["address"] as? String), // optional
                    MasterDB.Profile.ref_code <- (kvJSON["ref_code"] as? String), // optional
                    MasterDB.Profile.sms_code <- (kvJSON["sms_code"] as? String), // optional
                    MasterDB.Profile.profil_picture <- (kvJSON["profil_picture"] as? String), // optional
                    MasterDB.Profile.money <- (kvJSON["money"] as? Int64), // optional
                    MasterDB.Profile.first_name <- (kvJSON["first_name"] as? String)!,
                    MasterDB.Profile.id_office <- (kvJSON["id_office"] as? String), // optional
                    MasterDB.Profile.sex <- (kvJSON["sex"] as? String) // optional
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func update(money: Int64) -> Bool {
            do {
                _ = try MasterDB.db()?.run(MasterDB.Profile._table.update(
                    MasterDB.Profile.money <- money
                ))
                return true
            } catch {
                print(error)
                return false
            }
        }
    }
}
