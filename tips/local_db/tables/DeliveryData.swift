//
//  DeliveryData.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class DeliveryData {
        public static var _table = Table("delivery_data")
        public static var id = Expression<Int64>("id")
        public static var id_addt_info = Expression<Int64>("id_addt_info")
        public static var id_delivery = Expression<Int64>("id_delivery")
        public static var id_status = Expression<Int64>("id_status")
        
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = try row?.get(id)
                out["id_addt_info"] = try row?.get(id_addt_info)
                out["id_delivery"] = try row?.get(id_delivery)
                out["id_status"] = try row?.get(id_status)
                out["addt_info"] = MasterDB.AddtInfos.getByIdAndRaw(__id: out["id_addt_info"] as! Int64)
                out["delivery"] = MasterDB.Deliveries.getByIdAndRaw(__id: out["id_delivery"] as! Int64)
                out["status"] = MasterDB.Statuses.getByIdAndRaw(__id: out["id_status"] as! Int64)
            } catch {
                print(error)
            }
            
            return out
        }
        
        enum DeliveryDataError: Error {
            case exAddtInfos
            case exStatuses
            case exDeliveries
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            do {
                let addt_info_id_inserted = MasterDB.AddtInfos.insert(kvJSON: kvJSON["addt_info"] as! [String: Any])
                let status_id_inserted = MasterDB.Statuses.insert(kvJSON: kvJSON["status"] as! [String: Any])
                let delivery_id_inserted = MasterDB.Deliveries.insert(kvJSON: kvJSON["delivery"] as! [String: Any])
                
                if addt_info_id_inserted == nil {
                    throw DeliveryDataError.exAddtInfos
                }
                
                if delivery_id_inserted == nil {
                    throw DeliveryDataError.exDeliveries
                }
                
                if status_id_inserted == nil {
                    throw DeliveryDataError.exStatuses
                }
                
                let rowId = try MasterDB.db()?.run(MasterDB.DeliveryData._table.insert(
                    MasterDB.DeliveryData.id_addt_info <- addt_info_id_inserted!,
                    MasterDB.DeliveryData.id_delivery <- delivery_id_inserted!,
                    MasterDB.DeliveryData.id_status <- status_id_inserted!
                ))
                
                return rowId
            } catch DeliveryDataError.exAddtInfos {
                print("deliverydata.addt_info null")
                return nil
            } catch DeliveryDataError.exStatuses {
                print("deliverydata.status null")
                return nil
            } catch DeliveryDataError.exDeliveries {
                print("deliverydata.delivery null")
                return nil
            } catch {
                print(error)
                return nil
            }
        }
    }
}
