//
//  Delivery_Goods.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class Delivery_Goods {
        public static var _table = Table("delivery_goods")
        public static var id = Expression<Int64  >("id")
        public static var id_delivery = Expression<Int64  >("id_delivery")
        public static var id_goods = Expression<Int64  >("id_goods")
        
        // unused
        public static func getByIdDelivery(__delivery_id: Int64) -> AnySequence<Row>? {
            do {
                return try MasterDB.db()?.prepare(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id_delivery] == __delivery_id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdDeliveryAndRaw(__id: Int64) -> [Any] {
            do {
                return try self.raw(rows: MasterDB.db()?.prepare(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id_delivery] == __id)
                    ))
            } catch {
                print(error)
                return []
            }
        }
        
        public static func raw(rows: AnySequence<Row>?) -> [Any] {
            var res: [Any] = []
            do {
                for row in rows! {
                    var out: [String: Any] = [:]
                    out["id"] = (try row.get(id)).intValue()
                    out["id_delivery"] = try row.get(id_delivery).intValue()
                    out["id_goods"] = try row.get(id_goods).intValue()
                    res.append(out)
                }
                
            } catch {
                print(error)
            }
            
            return res
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.Delivery_Goods._table.insert(
                    MasterDB.Delivery_Goods.id_delivery <- (kvJSON["id_delivery"] as? Int64)!,
                    MasterDB.Delivery_Goods.id_goods <- (kvJSON["id_goods"] as? Int64)!
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
    }
}
