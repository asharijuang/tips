//
//  Airports.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class Airports {
        public static var _table = Table("airports")
        public static var id = Expression<Int64  >("id")
        public static var id_city = Expression<Int64  >("id_city")
        public static var name = Expression<String >("name")
        public static var initial_code = Expression<String >("initial_code")
        public static var status = Expression<Int64  >("status")
        
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = (try row?.get(id))?.intValue()
                out["id_city"] = (try row?.get(id_city))?.intValue()
                out["name"] = try row?.get(name)
                out["initial_code"] = try row?.get(initial_code)
                out["status"] = (try row?.get(status))?.intValue()
            } catch {
                print(error)
            }
            
            return out
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64?{
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.Airports._table.insert(
                    MasterDB.Airports.id_city <- (kvJSON["id_city"] as? Int64)!,
                    MasterDB.Airports.name <- (kvJSON["name"] as? String)!,
                    MasterDB.Airports.initial_code <- (kvJSON["initial_code"] as? String)!,
                    MasterDB.Airports.status <- (kvJSON["status"] as? Int64)!
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
    }
}
