//
//  Statuses.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class Statuses {
        public static var _table = Table("statuses")
        public static var id = Expression<Int64  >("id")
        public static var description = Expression<String >("description")
        public static var detail = Expression<String?>("detail")
        public static var step = Expression<Int64  >("step")
        
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = (try row?.get(id))?.intValue()
                out["description"] = try row?.get(description)
                out["detail"] = try row?.get(detail)
                out["step"] = (try row?.get(step))?.intValue()
            } catch {
                print(error)
            }
            
            return out
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.Statuses._table.insert(
                    MasterDB.Statuses.description <- (kvJSON["description"] as? String)!,
                    MasterDB.Statuses.detail <- (kvJSON["detail"] as? String), // optional
                    MasterDB.Statuses.step <- (kvJSON["step"] as? Int64)!
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
    }
}
