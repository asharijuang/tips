//
//  ImageSQL.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class ImageSQL {
        public static var _table = Table("image_sql")
        public static var id = Expression<Int64  >("id")
        public static var name = Expression<String>("name")
        public static var url = Expression<String>("url")
        public static var data = Expression<String>("data")
        
        public static func getByName(name: String) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.name] == name)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByNameAndRaw(name: String) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.name] == name)
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = try row?.get(id)
                out["url"] = try row?.get(url)
                out["name"] = try row?.get(name)
                out["data"] = try row?.get(data)
            } catch {
                print(error)
            }
            
            return out
        }
        
        
        public static func update(kvJSON: [String: Any]) -> Int? {
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.ImageSQL._table.update(
                    MasterDB.ImageSQL.data <- (kvJSON["data"] as? String)!
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            let existingData = MasterDB.ImageSQL.getByNameAndRaw(name: (kvJSON["name"] as? String)!)
            let isDataExist = existingData.count > 0
            if isDataExist {
                let current_url = existingData["url"] as! String
                let want_to_insert_url = kvJSON["url"] as! String
                
                if current_url == want_to_insert_url {
                    return existingData["id"] as? Int64
                }
                
                return Int64(MasterDB.ImageSQL.update(kvJSON: kvJSON)!)
            }
            
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.ImageSQL._table.insert(
                    MasterDB.ImageSQL.name <- (kvJSON["name"] as? String)!,
                    MasterDB.ImageSQL.url <- (kvJSON["url"] as? String)!,
                    MasterDB.ImageSQL.data <- (kvJSON["data"] as? String)!
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
    }
}
