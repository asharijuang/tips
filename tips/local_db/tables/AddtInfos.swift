//
//  AddtInfo.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class AddtInfos {
        public static var _table = Table("addt_infos")
        public static var id = Expression<Int64  >("id")
        public static var airline_name = Expression<String?>("airline_name")
        public static var flight_code = Expression<String?>("flight_code")
        public static var kode_bandara_asal = Expression<String?>("kode_bandara_asal")
        public static var kode_bandara_tujuan = Expression<String?>("kode_bandara_tujuan")
        
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = (try row?.get(id))?.intValue()
                out["airline_name"] = try row?.get(airline_name)
                out["flight_code"] = try row?.get(flight_code)
                out["kode_bandara_asal"] = try row?.get(kode_bandara_asal)
                out["kode_bandara_tujuan"] = try row?.get(kode_bandara_tujuan)
            } catch {
                print(error)
            }
            
            return out
        }
        
        public static func getById(id: Int64){
            
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64?{
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.AddtInfos._table.insert(
                    MasterDB.AddtInfos.airline_name <- (kvJSON["airline_name"] as? String), // optional
                    MasterDB.AddtInfos.flight_code <- (kvJSON["flight_code"] as? String), // optional
                    MasterDB.AddtInfos.kode_bandara_asal <- (kvJSON["kode_bandara_asal"] as? String), // optional
                    MasterDB.AddtInfos.kode_bandara_tujuan <- (kvJSON["kode_bandara_tujuan"] as? String) // optional
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
    }
}
