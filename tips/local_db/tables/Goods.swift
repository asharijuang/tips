//
//  Goods.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class Goods {
        public static var _table = Table("goods")
        public static var id = Expression<Int64  >("id")
        public static var shipment_contents = Expression<String >("shipment_contents")
        public static var real_weight = Expression<Double >("real_weight")
        
        
        // unused
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = (try row?.get(id))?.intValue()
                out["shipment_contents"] = try row?.get(shipment_contents)
                out["real_weight"] = try row?.get(real_weight)
            } catch {
                print(error)
            }
            
            return out
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.Goods._table.insert(
                    MasterDB.Goods.shipment_contents <- (kvJSON["shipment_contents"] as? String)!,
                    MasterDB.Goods.real_weight <- Double((kvJSON["real_weight"] as? String)!)!
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
    }
}
