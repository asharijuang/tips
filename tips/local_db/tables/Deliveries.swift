//
//  Deliveries.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class Deliveries {
        public static var _table = Table("deliveries")
        public static var id = Expression<Int64  >("id")
        public static var baggage_space = Expression<Double >("baggage_space")
        public static var booking_code = Expression<String >("booking_code")
        public static var depature = Expression<String >("depature")
        public static var destination_city = Expression<String >("destination_city")
        public static var detail_status = Expression<String?>("detail_status")
        public static var first_name = Expression<String >("first_name")
        public static var flight_code = Expression<String >("flight_code")
        public static var id_airline = Expression<Int64  >("id_airline")
        public static var id_destination_airport = Expression<Int64 >("id_destination_airport")
        public static var id_destination_city = Expression<Int64 >("id_destination_city")
        public static var id_member = Expression<Int64 >("id_member")
        public static var id_origin_airport = Expression<Int64 >("id_origin_airport")
        public static var id_origin_city = Expression<Int64 >("id_origin_city")
        public static var id_slot_status = Expression<Int64 >("id_slot_status")
        public static var id_wallet_transaction = Expression<Int64?>("id_wallet_transaction")
        public static var last_name = Expression<String?>("last_name")
        public static var origin_city = Expression<String >("origin_city")
        public static var photo_tag = Expression<String?>("photo_tag")
        public static var slot_id = Expression<String >("slot_id")
        public static var slot_price_kg = Expression<Int64 >("slot_price_kg")
        public static var sold_baggage_space = Expression<Double >("sold_baggage_space")
        public static var status_bayar = Expression<Int64 >("status_bayar")
        public static var status_dispatch = Expression<String >("status_dispatch")
        
        
        // unused
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                    ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        private static func getDeliveryGoodsIds(__id: Int64) -> [Int64]{
            var outIds: [Int64] = []
            for dg in Delivery_Goods.getByIdDeliveryAndRaw(__id: __id) {
                let kvDg = dg as! [String: Any]
                outIds.append(Int64(kvDg["id_goods"] as! Int))
            }
            
            return outIds
        }
        
        public static func getGoodsOf(delivery_id: Int64) -> [Any] {
            let goodsIds = MasterDB.Deliveries.getDeliveryGoodsIds(__id: delivery_id)
            var outGoods: [Any] = []
            for goodsId in goodsIds {
                outGoods.append(MasterDB.Goods.getByIdAndRaw(__id: goodsId))
            }
            
            return outGoods
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                let int64ID = (try row?.get(id))
                out["id"] = int64ID?.intValue()
                out["baggage_space"] = try row?.get(baggage_space)
                out["booking_code"] = try row?.get(booking_code)
                out["depature"] = try row?.get(depature)
                out["destination_city"] = try row?.get(destination_city)
                out["detail_status"] = try row?.get(detail_status)
                out["first_name"] = try row?.get(first_name)
                out["flight_code"] = try row?.get(flight_code)
                out["id_airline"] = (try row?.get(id_airline))?.intValue()
                out["id_destination_airport"] = (try row?.get(id_destination_airport))?.intValue()
                out["id_destination_city"] = (try row?.get(id_destination_city))?.intValue()
                out["id_member"] = (try row?.get(id_member))?.intValue()
                out["id_origin_airport"] = (try row?.get(id_origin_airport))?.intValue()
                out["id_origin_city"] = (try row?.get(id_origin_city))?.intValue()
                out["id_slot_status"] = (try row?.get(id_slot_status))?.intValue()
                out["id_wallet_transaction"] = (try row?.get(id_wallet_transaction))?.intValue()
                out["last_name"] = try row?.get(last_name)
                out["origin_city"] = try row?.get(origin_city)
                out["photo_tag"] = try row?.get(photo_tag)
                out["slot_id"] = try row?.get(slot_id)
                out["slot_price_kg"] = (try row?.get(slot_price_kg))?.intValue()
                out["sold_baggage_space"] = try row?.get(sold_baggage_space)
                out["status_bayar"] = (try row?.get(status_bayar))?.intValue()
                out["status_dispatch"] = try row?.get(status_dispatch)
                out["airport_origin"] = MasterDB.Airports.getByIdAndRaw(__id: Int64(out["id_origin_airport"] as! Int))
                out["airport_destination"] = MasterDB.Airports.getByIdAndRaw(__id: Int64(out["id_destination_airport"] as! Int))
                out["goods"] = MasterDB.Deliveries.getGoodsOf(delivery_id: int64ID!)
            } catch {
                print(error)
            }
            
            return out
        }
        
        enum AirportsError: Error {
            case exAirports
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64? {
            do {
                let airport_origin_id_inserted = MasterDB.Airports.insert(kvJSON: kvJSON["origin_airport"] as! [String : Any])
                let airport_destin_id_inserted = MasterDB.Airports.insert(kvJSON: kvJSON["destination_airport"] as! [String : Any])
                let rowId = try MasterDB.db()?.run(MasterDB.Deliveries._table.insert(
                    MasterDB.Deliveries.id_destination_airport <- airport_destin_id_inserted!,
                    MasterDB.Deliveries.id_origin_airport <- airport_origin_id_inserted!,
                    
                    MasterDB.Deliveries.baggage_space <- Double((kvJSON["baggage_space"] as? String)!)!,
                    MasterDB.Deliveries.booking_code <- (kvJSON["booking_code"] as? String)!,
                    MasterDB.Deliveries.depature <- (kvJSON["depature"] as? String)!,
                    MasterDB.Deliveries.destination_city <- (kvJSON["destination_city"] as? String)!,
                    MasterDB.Deliveries.detail_status <- (kvJSON["detail_status"] as? String), // optional
                    MasterDB.Deliveries.first_name <- (kvJSON["first_name"] as? String)!,
                    MasterDB.Deliveries.flight_code <- (kvJSON["flight_code"] as? String)!,
                    MasterDB.Deliveries.id_airline <- (kvJSON["id_airline"] as? Int64)!,
                    MasterDB.Deliveries.id_destination_city <- (kvJSON["id_destination_city"] as? Int64)!,
                    MasterDB.Deliveries.id_member <- (kvJSON["id_member"] as? Int64)!,
                    MasterDB.Deliveries.id_origin_city <- (kvJSON["id_origin_city"] as? Int64)!,
                    MasterDB.Deliveries.id_slot_status <- (kvJSON["id_slot_status"] as? Int64)!,
                    MasterDB.Deliveries.id_wallet_transaction <- (kvJSON["id_wallet_transaction"] as? Int64), // optional
                    MasterDB.Deliveries.last_name <- (kvJSON["last_name"] as? String), // optional
                    MasterDB.Deliveries.origin_city <- (kvJSON["origin_city"] as? String)!,
                    MasterDB.Deliveries.photo_tag <- (kvJSON["photo_tag"] as? String), // optional
                    MasterDB.Deliveries.slot_id <- (kvJSON["slot_id"] as? String)!,
                    MasterDB.Deliveries.slot_price_kg <- (kvJSON["slot_price_kg"] as? Int64)!,
                    MasterDB.Deliveries.sold_baggage_space <- Double((kvJSON["sold_baggage_space"] as? String)!)!,
                    MasterDB.Deliveries.status_bayar <- (kvJSON["status_bayar"] as? Int64)!,
                    MasterDB.Deliveries.status_dispatch <- (kvJSON["status_dispatch"] as? String)!
                ))
                
                let goods_list = kvJSON["goods"] as! [Any]
                for goods in goods_list {
                    let goodsRowId = MasterDB.Goods.insert(kvJSON: goods as! [String: Any])
                    _ = MasterDB.Delivery_Goods.insert(kvJSON: [
                        "id_delivery": rowId!,
                        "id_goods": goodsRowId!
                    ])
                }
                
                return rowId
            } catch AirportsError.exAirports {
                print("delivery.airport null")
                return nil
            } catch {
                print(error)
                return nil
            }
        }
    }
}
