//
//  Shipments.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import SQLite

extension MasterDB {
    class Shipments {
        public static var _table = Table("shipments")
        public static var id = Expression<Int64  >("id")
        public static var add_insurance_cost = Expression<Int64  >("add_insurance_cost")
        public static var add_notes = Expression<String?>("add_notes")
        public static var consignee_address = Expression<String >("consignee_address")
        public static var consignee_address_detail = Expression<String?>("consignee_address_detail")
        public static var consignee_city = Expression<String >("consignee_city")
        public static var consignee_districts = Expression<String >("consignee_districts")
        public static var consignee_first_name = Expression<String >("consignee_first_name")
        public static var consignee_last_name = Expression<String?>("consignee_last_name")
        public static var consignee_latitude = Expression<Double?>("consignee_latitude")
        public static var consignee_longitude = Expression<Double?>("consignee_longitude")
        public static var consignee_mobile_phone = Expression<String >("consignee_mobile_phone")
        public static var consignee_postal_code = Expression<String >("consignee_postal_code")
        public static var consignee_province = Expression<String >("consignee_province")
        public static var delivered_by = Expression<String?>("delivered_by")
        public static var delivered_date = Expression<String?>("delivered_date")
        public static var delivered_time = Expression<String?>("delivered_time")
        public static var destination_city = Expression<String >("destination_city")
        public static var detail_status = Expression<String?>("detail_status")
        public static var dispatch_type = Expression<String >("dispatch_type")
        public static var estimate_goods_value = Expression<String >("estimate_goods_value")
        public static var estimate_weight = Expression<Double >("estimate_weight")
        public static var flight_cost = Expression<Int64  >("flight_cost")
        public static var goods_status = Expression<String >("goods_status")
        public static var id_consignee_city = Expression<Int64  >("id_consignee_city")
        public static var id_consignee_districts = Expression<Int64  >("id_consignee_districts")
        public static var id_consignee_province = Expression<Int64  >("id_consignee_province")
        public static var id_destination_city = Expression<Int64  >("id_destination_city")
        public static var id_device = Expression<String?>("id_device")
        public static var id_office = Expression<Int64? >("id_office")
        public static var id_origin_city = Expression<Int64  >("id_origin_city")
        public static var id_packaging = Expression<Int64? >("id_packaging")
        public static var id_payment_type = Expression<Int64  >("id_payment_type")
        public static var id_shipment_status = Expression<Int64  >("id_shipment_status")
        public static var id_shipper = Expression<Int64  >("id_shipper")
        public static var id_shipper_city = Expression<Int64  >("id_shipper_city")
        public static var id_shipper_districts = Expression<Int64  >("id_shipper_districts")
        public static var id_shipper_province = Expression<Int64  >("id_shipper_province")
        public static var id_slot = Expression<String?>("id_slot")
        public static var id_wallet_transaction = Expression<String?>("id_wallet_transaction")
        public static var insurance_cost = Expression<Int64  >("insurance_cost")
        public static var is_add_insurance = Expression<Int64  >("is_add_insurance")
        public static var is_delivery = Expression<Int64  >("is_delivery")
        public static var is_first_class = Expression<Int64  >("is_first_class")
        public static var is_matched = Expression<Int64  >("is_matched")
        public static var is_online = Expression<Int64  >("is_online")
        public static var is_online_payment = Expression<Int64? >("is_online_payment")
        public static var is_posted = Expression<Int64  >("is_posted")
        public static var is_take = Expression<Int64  >("is_take")
        public static var origin_city = Expression<String >("origin_city")
        public static var packaging_date = Expression<String?>("packaging_date")
        public static var packaging_time = Expression<String?>("packaging_time")
        public static var payment_id = Expression<String?>("payment_id")
        public static var photo_ktp = Expression<String?>("photo_ktp")
        public static var photo_signature = Expression<String?>("photo_signature")
        public static var pickup_by = Expression<String?>("pickup_by")
        public static var pickup_date = Expression<String?>("pickup_date")
        public static var pickup_signature = Expression<String?>("pickup_signature")
        public static var pickup_status = Expression<String?>("pickup_status")
        public static var pickup_time = Expression<String?>("pickup_time")
        public static var real_weight = Expression<Double?>("real_weight")
        public static var received_by = Expression<String?>("received_by")
        public static var received_image = Expression<String?>("received_image")
        public static var received_time = Expression<String?>("received_time")
        public static var registration_type = Expression<String?>("registration_type")
        public static var shipment_contents = Expression<String?>("shipment_contents")
        public static var shipment_id = Expression<String>("shipment_id")
        public static var shipper_address = Expression<String >("shipper_address")
        public static var shipper_address_detail = Expression<String?>("shipper_address_detail")
        public static var shipper_city = Expression<String >("shipper_city")
        public static var shipper_districts = Expression<String >("shipper_districts")
        public static var shipper_first_name = Expression<String >("shipper_first_name")
        public static var shipper_last_name = Expression<String?>("shipper_last_name")
        public static var shipper_latitude = Expression<Double?>("shipper_latitude")
        public static var shipper_longitude = Expression<Double?>("shipper_longitude")
        public static var shipper_mobile_phone = Expression<String >("shipper_mobile_phone")
        public static var shipper_postal_code = Expression<String >("shipper_postal_code")
        public static var shipper_province = Expression<String >("shipper_province")
        public static var status_dispatch = Expression<String >("status_dispatch")
        public static var transaction_date = Expression<String >("transaction_date")
        
        public static func getById(__id: Int64) -> Row? {
            do {
                return try MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                )
            } catch {
                print(error)
                return nil
            }
        }
        
        public static func getByIdAndRaw(__id: Int64) -> [String: Any] {
            do {
                return try self.raw(row: MasterDB.db()?.pluck(self
                    ._table
                    .select(self._table[*])
                    .filter(self._table[self.id] == __id)
                ))
            } catch {
                print(error)
                return [:]
            }
        }
        
        public static func raw(row: Row?) -> [String: Any] {
            var out: [String: Any] = [:]
            do {
                out["id"] = (try row?.get(id))?.intValue()
                out["add_insurance_cost"] = (try row?.get(add_insurance_cost))?.intValue()
                out["add_notes"] = try row?.get(add_notes)
                out["consignee_address"] = try row?.get(consignee_address)
                out["consignee_address_detail"] = try row?.get(consignee_address_detail)
                out["consignee_city"] = try row?.get(consignee_city)
                out["consignee_districts"] = try row?.get(consignee_districts)
                out["consignee_first_name"] = try row?.get(consignee_first_name)
                out["consignee_last_name"] = try row?.get(consignee_last_name)
                out["consignee_latitude"] = try row?.get(consignee_latitude)
                out["consignee_longitude"] = try row?.get(consignee_longitude)
                out["consignee_mobile_phone"] = try row?.get(consignee_mobile_phone)
                out["consignee_postal_code"] = try row?.get(consignee_postal_code)
                out["consignee_province"] = try row?.get(consignee_province)
                out["delivered_by"] = try row?.get(delivered_by)
                out["delivered_date"] = try row?.get(delivered_date)
                out["delivered_time"] = try row?.get(delivered_time)
                out["destination_city"] = try row?.get(destination_city)
                out["detail_status"] = try row?.get(detail_status)
                out["dispatch_type"] = try row?.get(dispatch_type)
                out["estimate_goods_value"] = try row?.get(estimate_goods_value)
                out["estimate_weight"] = try row?.get(estimate_weight)
                out["flight_cost"] = (try row?.get(flight_cost))?.intValue()
                out["goods_status"] = try row?.get(goods_status)
                out["id_consignee_city"] = (try row?.get(id_consignee_city))?.intValue()
                out["id_consignee_districts"] = (try row?.get(id_consignee_districts))?.intValue()
                out["id_consignee_province"] = (try row?.get(id_consignee_province))?.intValue()
                out["id_destination_city"] = (try row?.get(id_destination_city))?.intValue()
                out["id_device"] = try row?.get(id_device)
                out["id_office"] = try row?.get(id_office)
                out["id_origin_city"] = (try row?.get(id_origin_city))?.intValue()
                out["id_packaging"] = (try row?.get(id_packaging))?.intValue()
                out["id_payment_type"] = (try row?.get(id_payment_type))?.intValue()
                out["id_shipment_status"] = (try row?.get(id_shipment_status))?.intValue()
                out["id_shipper"] = (try row?.get(id_shipper))?.intValue()
                out["id_shipper_city"] = (try row?.get(id_shipper_city))?.intValue()
                out["id_shipper_districts"] = (try row?.get(id_shipper_districts))?.intValue()
                out["id_shipper_province"] = (try row?.get(id_shipper_province))?.intValue()
                out["id_slot"] = try row?.get(id_slot)
                out["id_wallet_transaction"] = try row?.get(id_wallet_transaction)
                out["insurance_cost"] = (try row?.get(insurance_cost))?.intValue()
                out["is_add_insurance"] = (try row?.get(is_add_insurance))?.intValue()
                out["is_delivery"] = (try row?.get(is_delivery))?.intValue()
                out["is_first_class"] = (try row?.get(is_first_class))?.intValue()
                out["is_matched"] = (try row?.get(is_matched))?.intValue()
                out["is_online"] = (try row?.get(is_online))?.intValue()
                out["is_online_payment"] = (try row?.get(is_online_payment))?.intValue()
                out["is_posted"] = (try row?.get(is_posted))?.intValue()
                out["is_take"] = (try row?.get(is_take))?.intValue()
                out["origin_city"] = try row?.get(origin_city)
                out["packaging_date"] = try row?.get(packaging_date)
                out["packaging_time"] = try row?.get(packaging_time)
                out["payment_id"] = try row?.get(payment_id)
                out["photo_ktp"] = try row?.get(photo_ktp)
                out["photo_signature"] = try row?.get(photo_signature)
                out["pickup_by"] = try row?.get(pickup_by)
                out["pickup_date"] = try row?.get(pickup_date)
                out["pickup_signature"] = try row?.get(pickup_signature)
                out["pickup_status"] = try row?.get(pickup_status)
                out["pickup_time"] = try row?.get(pickup_time)
                out["real_weight"] = try row?.get(real_weight)
                out["received_by"] = try row?.get(received_by)
                out["received_image"] = try row?.get(received_image)
                out["received_time"] = try row?.get(received_time)
                out["registration_type"] = try row?.get(registration_type)
                out["shipment_contents"] = try row?.get(shipment_contents)
                out["shipment_id"] = try row?.get(shipment_id)
                out["shipper_address"] = try row?.get(shipper_address)
                out["shipper_address_detail"] = try row?.get(shipper_address_detail)
                out["shipper_city"] = try row?.get(shipper_city)
                out["shipper_districts"] = try row?.get(shipper_districts)
                out["shipper_first_name"] = try row?.get(shipper_first_name)
                out["shipper_last_name"] = try row?.get(shipper_last_name)
                out["shipper_latitude"] = try row?.get(shipper_latitude)
                out["shipper_longitude"] = try row?.get(shipper_longitude)
                out["shipper_mobile_phone"] = try row?.get(shipper_mobile_phone)
                out["shipper_postal_code"] = try row?.get(shipper_postal_code)
                out["shipper_province"] = try row?.get(shipper_province)
                out["status_dispatch"] = try row?.get(status_dispatch)
                out["transaction_date"] = try row?.get(transaction_date)
            } catch {
                print(error)
            }
            
            return out
        }
        
        public static func insert(kvJSON: [String: Any]) -> Int64?{
            do {
                let rowId = try MasterDB.db()?.run(MasterDB.Shipments._table.insert(
                    MasterDB.Shipments.add_insurance_cost <- (kvJSON["add_insurance_cost"] as? Int64)!,
                    MasterDB.Shipments.add_notes <- (kvJSON["add_notes"] as? String), // optional
                    MasterDB.Shipments.consignee_address <- (kvJSON["consignee_address"] as? String)!,
                    MasterDB.Shipments.consignee_address_detail <- (kvJSON["consignee_address_detail"] as? String), // optional
                    MasterDB.Shipments.consignee_city <- (kvJSON["consignee_city"] as? String)!,
                    MasterDB.Shipments.consignee_districts <- (kvJSON["consignee_districts"] as? String)!,
                    MasterDB.Shipments.consignee_first_name <- (kvJSON["consignee_first_name"] as? String)!,
                    MasterDB.Shipments.consignee_last_name <- (kvJSON["consignee_last_name"] as? String), // optional
                    MasterDB.Shipments.consignee_latitude <- (kvJSON["consignee_latitude"] as? Double), // optional
                    MasterDB.Shipments.consignee_longitude <- (kvJSON["consignee_longitude"] as? Double), // optional
                    MasterDB.Shipments.consignee_mobile_phone <- (kvJSON["consignee_mobile_phone"] as? String)!,
                    MasterDB.Shipments.consignee_postal_code <- (kvJSON["consignee_postal_code"] as? String)!,
                    MasterDB.Shipments.consignee_province <- (kvJSON["consignee_province"] as? String)!,
                    MasterDB.Shipments.delivered_by <- (kvJSON["delivered_by"] as? String), // optional
                    MasterDB.Shipments.delivered_date <- (kvJSON["delivered_date"] as? String), // optional
                    MasterDB.Shipments.delivered_time <- (kvJSON["delivered_time"] as? String), // optional
                    MasterDB.Shipments.destination_city <- (kvJSON["destination_city"] as? String)!,
                    MasterDB.Shipments.detail_status <- (kvJSON["detail_status"] as? String), // optional
                    MasterDB.Shipments.dispatch_type <- (kvJSON["dispatch_type"] as? String)!,
                    MasterDB.Shipments.estimate_goods_value <- (kvJSON["estimate_goods_value"] as? String)!,
                    MasterDB.Shipments.estimate_weight <- (kvJSON["estimate_weight"] as? Double)!,
                    MasterDB.Shipments.flight_cost <- (kvJSON["flight_cost"] as? Int64)!,
                    MasterDB.Shipments.goods_status <- (kvJSON["goods_status"] as? String)!,
                    MasterDB.Shipments.id_consignee_city <- (kvJSON["id_consignee_city"] as? Int64)!,
                    MasterDB.Shipments.id_consignee_districts <- (kvJSON["id_consignee_districts"] as? Int64)!,
                    MasterDB.Shipments.id_consignee_province <- (kvJSON["id_consignee_province"] as? Int64)!,
                    MasterDB.Shipments.id_destination_city <- (kvJSON["id_destination_city"] as? Int64)!,
                    MasterDB.Shipments.id_device <- (kvJSON["id_device"] as? String), // optional
                    MasterDB.Shipments.id_office <- (kvJSON["id_office"] as? Int64), // optional
                    MasterDB.Shipments.id_origin_city <- (kvJSON["id_origin_city"] as? Int64)!,
                    MasterDB.Shipments.id_packaging <- (kvJSON["id_packaging"] as? Int64), // optional
                    MasterDB.Shipments.id_payment_type <- (kvJSON["id_payment_type"] as? Int64)!,
                    MasterDB.Shipments.id_shipment_status <- (kvJSON["id_shipment_status"] as? Int64)!,
                    MasterDB.Shipments.id_shipper <- (kvJSON["id_shipper"] as? Int64)!,
                    MasterDB.Shipments.id_shipper_city <- (kvJSON["id_shipper_city"] as? Int64)!,
                    MasterDB.Shipments.id_shipper_districts <- (kvJSON["id_shipper_districts"] as? Int64)!,
                    MasterDB.Shipments.id_shipper_province <- (kvJSON["id_shipper_province"] as? Int64)!,
                    MasterDB.Shipments.id_slot <- (kvJSON["id_slot"] as? String), // optional
                    MasterDB.Shipments.id_wallet_transaction <- (kvJSON["id_wallet_transaction"] as? String), // optional
                    MasterDB.Shipments.insurance_cost <- (kvJSON["insurance_cost"] as? Int64)!,
                    MasterDB.Shipments.is_add_insurance <- (kvJSON["is_add_insurance"] as? Int64)!,
                    MasterDB.Shipments.is_delivery <- (kvJSON["is_delivery"] as? Int64)!,
                    MasterDB.Shipments.is_first_class <- (kvJSON["is_first_class"] as? Int64)!,
                    MasterDB.Shipments.is_matched <- (kvJSON["is_matched"] as? Int64)!,
                    MasterDB.Shipments.is_online <- (kvJSON["is_online"] as? Int64)!,
                    MasterDB.Shipments.is_online_payment <- (kvJSON["is_online_payment"] as? Int64), // optional
                    MasterDB.Shipments.is_posted <- (kvJSON["is_posted"] as? Int64)!,
                    MasterDB.Shipments.is_take <- (kvJSON["is_take"] as? Int64)!,
                    MasterDB.Shipments.origin_city <- (kvJSON["origin_city"] as? String)!,
                    MasterDB.Shipments.packaging_date <- (kvJSON["packaging_date"] as? String), // optional
                    MasterDB.Shipments.packaging_time <- (kvJSON["packaging_time"] as? String), // optional
                    MasterDB.Shipments.payment_id <- (kvJSON["payment_id"] as? String), // optional
                    MasterDB.Shipments.photo_ktp <- (kvJSON["photo_ktp"] as? String), // optional
                    MasterDB.Shipments.photo_signature <- (kvJSON["photo_signature"] as? String), // optional
                    MasterDB.Shipments.pickup_by <- (kvJSON["pickup_by"] as? String), // optional
                    MasterDB.Shipments.pickup_date <- (kvJSON["pickup_date"] as? String), // optional
                    MasterDB.Shipments.pickup_signature <- (kvJSON["pickup_signature"] as? String), // optional
                    MasterDB.Shipments.pickup_status <- (kvJSON["pickup_status"] as? String), // optional
                    MasterDB.Shipments.pickup_time <- (kvJSON["pickup_time"] as? String), // optional
                    MasterDB.Shipments.real_weight <- (kvJSON["real_weight"] as? Double), // optional
                    MasterDB.Shipments.received_by <- (kvJSON["received_by"] as? String), // optional
                    MasterDB.Shipments.received_image <- (kvJSON["received_image"] as? String), // optional
                    MasterDB.Shipments.received_time <- (kvJSON["received_time"] as? String), // optional
                    MasterDB.Shipments.registration_type <- (kvJSON["registration_type"] as? String), // optional
                    MasterDB.Shipments.shipment_contents <- (kvJSON["shipment_contents"] as? String), // optional
                    MasterDB.Shipments.shipment_id <- (kvJSON["shipment_id"] as? String)!,
                    MasterDB.Shipments.shipper_address <- (kvJSON["shipper_address"] as? String)!,
                    MasterDB.Shipments.shipper_address_detail <- (kvJSON["shipper_address_detail"] as? String), // optional
                    MasterDB.Shipments.shipper_city <- (kvJSON["shipper_city"] as? String)!,
                    MasterDB.Shipments.shipper_districts <- (kvJSON["shipper_districts"] as? String)!,
                    MasterDB.Shipments.shipper_first_name <- (kvJSON["shipper_first_name"] as? String)!,
                    MasterDB.Shipments.shipper_last_name <- (kvJSON["shipper_last_name"] as? String), // optional
                    MasterDB.Shipments.shipper_latitude <- (kvJSON["shipper_latitude"] as? Double), // optional
                    MasterDB.Shipments.shipper_longitude <- (kvJSON["shipper_longitude"] as? Double), // optional
                    MasterDB.Shipments.shipper_mobile_phone <- (kvJSON["shipper_mobile_phone"] as? String)!,
                    MasterDB.Shipments.shipper_postal_code <- (kvJSON["shipper_postal_code"] as? String)!,
                    MasterDB.Shipments.shipper_province <- (kvJSON["shipper_province"] as? String)!,
                    MasterDB.Shipments.status_dispatch <- (kvJSON["status_dispatch"] as? String)!,
                    MasterDB.Shipments.transaction_date <- (kvJSON["transaction_date"] as? String)!
                ))
                
                return rowId
            } catch {
                print(error)
                return nil
            }
        }
    }
    
}
