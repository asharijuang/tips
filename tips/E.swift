//
//  E.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 17/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

class E{
    
    static var last_error_message: String = ""
    static let connection_error: String = "Aplkasi ini membutuhkan koneksi Internet. Pastikan perangkat Anda terhubung dengan Internet"
    
    static func printLastErrorMessage(error: String){
        print("** --> debug error: " + error)
    }
    
    static func getDictData(json_data: DataResponse<Any>) -> [String: Any]{
        let value = json_data.result.value
        return (value as? [String: Any])!
    }
    
    static func isResponseErrorExist(json_data: DataResponse<Any>) -> Bool {
        print(json_data.response?.statusCode ?? -1)
        self.last_error_message = connection_error //"There is an error while connecting to server: #001"
        let has_connection_error = json_data.error != nil
        if has_connection_error {
            self.last_error_message = connection_error // "There is an error while connecting to server: #002"
            print(json_data.error.debugDescription)
            return has_connection_error
        }
        
        let is_not_200 = json_data.response?.statusCode != 200
        if is_not_200 {
            self.last_error_message = "There is an error while connecting to server: #003"
            print("response code is not 200")
            return is_not_200
        }
        
        let has_error: Bool = !(E.getDictData(json_data: json_data)["err"] is NSNull)
        if has_error{
            let error_data = E.getError(json_data: json_data)
            self.last_error_message = (error_data["message"] as? String)!
            print(error_data)
        }
        
        return has_error
    }
    
    static func isResponseDataExist(json_data: DataResponse<Any>) -> Bool {
        let has_result: Bool = !(E.getDictData(json_data: json_data)["result"] is NSNull)
        if !has_result {
            print("isResponseDataExist has no result")
        }
        
        return has_result
    }
    
    static func getError(json_data: DataResponse<Any>) -> [String: Any] {
        print(E.getDictData(json_data: json_data)["err"].debugDescription)
        let code_and_message    = E.getDictData(json_data: json_data)["err"] as? [String: Any]
        let code                = code_and_message!["code"] as? Int
        let mesg                = code_and_message!["message"] as? String
        
        if(code == nil){
            E.printLastErrorMessage(error: "on getError([String: Any]) -> [String: String], \"code\" is null")
        }
        
        if(mesg == nil){
            E.printLastErrorMessage(error: "on getError([String: Any]) -> [String: String], \"message\" is null")
        }
        
        return code_and_message!
    }
    
    static func getData(json_data: DataResponse<Any>) -> [String: Any] {
        //print(json_data.result.value.debugDescription)
        if E.isResponseDataExist(json_data: json_data) {
            let code_and_message = E.getDictData(json_data: json_data)["result"] as? [String: Any]
            return code_and_message!
        }
        
        E.printLastErrorMessage(error: "on isResponseDataExist([String: Any]) -> [String: String], error is null")
        return [:]
    }
    
    static func getDataAsArr(json_data: DataResponse<Any>) -> [Any] {
        //print(json_data.debugDescription)
        if E.isResponseDataExist(json_data: json_data) {
            let code_and_message = E.getDictData(json_data: json_data)["result"] as? [Any]
            return code_and_message!
        }
        
        E.printLastErrorMessage(error: "on isResponseDataExist([Any]) -> [String], error is null")
        return []
    }
    
    static func getDataAsBool(json_data: DataResponse<Any>) -> Bool? {
        //print(json_data.debugDescription)
        if E.isResponseDataExist(json_data: json_data) {
            let code_and_message = E.getDictData(json_data: json_data)["result"] as? Bool
            return code_and_message
        }
        
        E.printLastErrorMessage(error: "on isResponseDataExist([Any]) -> [String], error is null")
        return nil
    }
    
    static func showUIAlert(vc: UIViewController, title: String, message: String? = nil, completion: (()->())? = nil){
        let alertController = UIAlertController(title: title, message: message == nil ? self.last_error_message : message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            (result : UIAlertAction) -> Void in
            if completion != nil{
                completion?()
            }
        }
        alertController.addAction(okAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    static func showUIError(vc: UIViewController, message: String? = nil, title: String? = "Error",
                            completion: (()->())? = nil){
        E.showUIAlert(vc: vc, title: title!, message: message, completion: completion)
    }
    
    static func showUIWarning(vc: UIViewController, message: String? = nil, completion: (()->())? = nil){
        E.showUIAlert(vc: vc, title: "Warning", message: message, completion: completion)
    }
    
    static func showUIUncomplete(vc: UIViewController, message: String? = nil, completion: (()->())? = nil){
        E.showUIAlert(vc: vc, title: "Uncomplete", message: message, completion: completion)
    }
    
    static func showUISuccess(vc: UIViewController, message: String? = nil, title: String? = "Success",
                              completion: (()->())? = nil){
        E.showUIAlert(vc: vc, title: title!, message: message, completion: completion)
    }
}
