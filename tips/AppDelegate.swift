//
//  AppDelegate.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import TwitterCore
import TwitterKit
import Firebase
import UserNotifications
import AlamofireNetworkActivityLogger
import FacebookCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id_key"
    static var screenSize: CGSize?
    
    // change value below to false as you want to test on development
    static let modeProduction: Bool = true

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if !AppDelegate.modeProduction {
            NetworkActivityLogger.shared.level = .debug
            NetworkActivityLogger.shared.startLogging()
        }
        
        if AppDelegate.modeProduction {
            C().setProductionEndpoint()
        }
        
        /* :)) */
        // wtf
        GMSServices.provideAPIKey("AIzaSyA2S48UXxt2cveZkqTi0AMGz5IDLE0w4i4")
        GMSPlacesClient.provideAPIKey("AIzaSyBq367HPO0hynLg9oJ1VlcQxCzcCNDsBzo")
        TWTRTwitter.sharedInstance().start(withConsumerKey:"zYv6QbYPX36Jkguw0tUghQlqg",
                                           consumerSecret:"Qi4IvtHvt7rKdADVOHM4ReoHQxkhsABYCicExGDCNl0GEX3cHN")
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        SDKApplicationDelegate.shared.application(app, open: url, options: options)
        return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Application: DidEnterBackground")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("Application: DidBecomeActive")
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
