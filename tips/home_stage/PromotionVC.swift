//
//  PromotionVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class PromotionVC: XUIViewController {

    @IBOutlet weak var imagePromo: UIImageView!
    @IBOutlet weak var labelPromo: UILabel!
    @IBOutlet weak var buttonOK: UIButton!
    @IBOutlet weak var modalBackground: UIView!
    @IBOutlet weak var blurArea: UIView!
    
    public var data: ModPromotion?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupLayout()
        blurArea.addTapGestureRecognizer{
            self.backStack()
        }
        modalBackground.addTapGestureRecognizer{
            self.dismissKeyboard()
            return
        }
        // Do any additional setup after loading the view.
    }
    
    func setupLayout(){
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        self.labelPromo.text = data?.description
        self.modalBackground.layer.cornerRadius = 10;
        self.modalBackground.layer.masksToBounds = true
        ViewController.setOLabel(button: self.buttonOK, text: "OK", filled: false, reverseColor: true)
        self.buttonOK.addTapGestureRecognizer {
            self.postPromo()
        }
        
        if data == nil {
            return
        }
        
        let urlString = data!.image_promo!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print(urlString)
        if let url = URL(string: urlString) {
            self.imagePromo.sd_setImage(with: url) { (image, err, sdicach, url) in
                if image == nil {
                    return
                }
                
                self.imagePromo.image = image!
            }
        } else {
            print("could not open url, it was nil")
        }
        
    }
    
    
    func postPromo() {
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(HomeRouter.ambil_promo(parameters: [
            "id_user": HomeVC.user.id!,
            "id_promo": data?.promo_id!
        ])).responseJSON(completionHandler: {
            json_data in
            
            print(json_data)
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
            }
            
            let msg = E.getData(json_data: json_data)
            E.showUISuccess(vc: self, message: msg["message"] as? String, completion: {
                UIViewController.removeSpinner(spinner: sv)
                self.dismiss(animated: false, completion: nil)
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class ModPromotion{
    var promo_id: Int?
    var image_promo: String?
    var description: String?
}
