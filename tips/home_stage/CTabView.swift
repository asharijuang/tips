//
//  CTabView.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 07/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class CTabView: UIScrollView{
    
    var borderBottomWidth: CGFloat = CGFloat(0);
    var currentIndexSelected: Int = 2;
    var border = CALayer()
    var mainView: UIView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
    var callbackClicked: ((_ i: Int) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    func setCallbackClicked(c: @escaping ((_ i: Int) -> Void)){
        self.callbackClicked = c
    }
    
    func focusToCurrentChild(){
        UIView.animate(withDuration: 150, animations: {
            self.border.frame = CGRect(x: CGFloat(self.currentIndexSelected) * self.borderBottomWidth, y: self.frame.height - CGFloat(2), width: self.borderBottomWidth, height: CGFloat(2))
            

            let midSelf = CGFloat(self.frame.size.width / 2)
            let midChild = self.borderBottomWidth / 2
            let minXPositionChild = midSelf - midChild
            let maxChildX = self.mainView.frame.size.width - CGFloat(self.frame.size.width)
            
            let currentChildOriginPosition = CGFloat(self.currentIndexSelected) * self.borderBottomWidth
            
            var moveXDelta = currentChildOriginPosition - minXPositionChild
            moveXDelta = moveXDelta < 0 ? 0 : moveXDelta
            moveXDelta = moveXDelta >= maxChildX ? maxChildX : moveXDelta
            
            print("min pos   : " + minXPositionChild.description)
            print("max pos   : " + maxChildX.description)
            print("cur orig  : " + currentChildOriginPosition.description)
            print("move needs: " + moveXDelta.description)
            
            self.setContentOffset(CGPoint(x: moveXDelta, y: self.contentOffset.y), animated: true)
        })
    }
    
    func initAll(datas: [[String: Any]]){
        var lastX = CGFloat(0)
        var i: Int = 0
        self.mainView.removeAllSubviews()
        for data in datas{
            let type = data["type"] as? String
            let detail_data = data[(type == "S" ? "shipment" : "delivery")] as? [String: Any]
            let partView = createHeaderTabData(xposition: lastX, data: detail_data!, type: type!)
            let index = i
            partView.addTapGestureRecognizer {
                self.scrollToTab(n: index)
            }
            self.mainView.addSubview(partView)
            lastX += partView.frame.size.width
            borderBottomWidth = partView.frame.size.width
            i += 1
        }
        self.mainView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: lastX, height: self.frame.height)
        if #available(iOS 11.0, *) {
            self.border.backgroundColor = UIColor(named: "primary")?.cgColor
        }
        self.border.frame = CGRect(x: 0, y: self.frame.height - CGFloat(2), width: borderBottomWidth, height: CGFloat(2))
        self.mainView.layer.addSublayer(self.border)
        self.addSubview(self.mainView)
        let leftPositionMainView = NSLayoutConstraint(item: self.mainView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0)
        let topPositionMainView = NSLayoutConstraint(item: self.mainView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0)
        let heightPositionMainView = NSLayoutConstraint(item: self.mainView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1.0, constant: 0)
        
        self.addConstraint(leftPositionMainView)
        self.addConstraint(topPositionMainView)
        self.addConstraint(heightPositionMainView)
        
        self.contentSize = CGSize(width: CGFloat(self.mainView.frame.size.width), height: CGFloat(50))
    }
    
    func scrollToTab(n: Int){
        self.currentIndexSelected = n
        self.focusToCurrentChild()
        self.callbackClicked!(n)
    }
    
    func createHeaderTabData(xposition: CGFloat, data: [String: Any], type: String) -> UIView{
        let isShipment      = type == "S"
        let data_keyof_code = isShipment ? "shipment_id" : "slot_id"
        let image_keyo_code = isShipment ? "shipment_icon_blue" : "plane_icon_blue"
        let left_right_padding = 15
        let image_width = 25
        let text_width = 90
        let global_width = image_width + text_width
        let global_height = 50
        let view = UIView(frame: CGRect(x: CGFloat(xposition), y: CGFloat(0), width: CGFloat(global_width + 3 * left_right_padding), height: CGFloat(global_height)))
        let image_view = UIImage(named: image_keyo_code)
        let ui_image = UIImageView(image: image_view)
        ui_image.frame = CGRect(x: CGFloat(left_right_padding), y: CGFloat(0), width: CGFloat(image_width), height: CGFloat(global_height))
        ui_image.contentMode = .scaleAspectFit
        let code_view = UILabel(frame: CGRect(x: CGFloat(2 * left_right_padding) + ui_image.frame.width, y: CGFloat(0), width: CGFloat(text_width), height: CGFloat(global_height)))
        code_view.textAlignment = .center
        code_view.text = data[data_keyof_code] as? String
        view.addSubview(ui_image)
        view.addSubview(code_view)
        
        return view
    }
}

extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate typealias Action = (() -> Void)?
    
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
}

extension UIView {
    func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
}
