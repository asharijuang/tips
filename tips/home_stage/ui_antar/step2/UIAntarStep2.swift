//
//  UIAntarStep2.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//


import UIKit

class UIAntarStep2: CustomUIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelSlotId: UILabel!
    @IBOutlet weak var miniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var toggleWithCaption: UIToggleCheck!
    @IBOutlet weak var containerButton: UIView!
    @IBOutlet weak var buttonBatal: UIButton!
    @IBOutlet weak var buttonKonfirmasi: UIButton!
    
    func setupXib(modAntarStep2: ModAntarStep2){
        if(modAntarStep2.is_complete){
            self.xibSetup(nibName: "UIAntarStep2")
            labelSlotId.text = modAntarStep2.slot_id
            miniStepDetail.xibSetup(modMiniStepDetail: modAntarStep2.mod_mini_step_detail)
            toggleWithCaption.setupXib(text: "Konfirmasi akan mengantar barang antaran", state: false)
            detailLabel.text = "Anda akan mendapatkan paket\n\(modAntarStep2.number_kg)KG,\ndan Anda"
                                + "akan mendapatkan\nIDR \(modAntarStep2.number_money),-"
            self.setupContainerAndLabel()
        }else{
            print("your ModAntarStep2 instance is not complete yet")
        }
    }
    
    func setupXib(code: String, isShipment: Bool, n: Int, deskripsi: String, detail: String?){
        self.xibSetup(nibName: "UIAntarStep2")
        labelSlotId.text = code
        miniStepDetail.xibSetup(isShipment: isShipment, n: n, deskripsi: deskripsi, detail: detail)
        toggleWithCaption.setupXib(text: "Konfirmasi akan mengantar barang antaran", state: false)
        detailLabel.text = "Anda akan mendapatkan paket [X]KG, dan Anda akan mendapatkan IDR 0,-"
        self.setupContainerAndLabel()
    }
    
    func setupContainerAndLabel(){
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = UIColor.white
        ViewController.setOLabel(button: buttonBatal, text: "Batal", filled: false)
        ViewController.setOLabel(button: buttonKonfirmasi, text: "Konfirmasi", filled: true)
    }
    
    func getSize() -> CGSize{
        return CGSize(width: containerView.frame.size.width + containerButton.frame.size.width,
                      height: containerView.frame.size.height + containerButton.frame.size.height)
    }
    
    func getRect() -> CGRect{
        return CGRect(x: 0, y: 0,
                      width: containerView.frame.size.width + containerButton.frame.size.width,
                      height: containerView.frame.size.height + containerButton.frame.size.height)
    }
}


class ModAntarStep2{
    public var slot_id: String = ""
    public var mod_mini_step_detail: ModMiniStepDetail = ModMiniStepDetail()
    public var number_kg: String = ""
    public var number_money: String = ""
    public var is_complete: Bool = false
}

