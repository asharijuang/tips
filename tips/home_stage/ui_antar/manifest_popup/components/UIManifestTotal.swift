//
//  UIManifestTotal.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIManifestTotal: CustomUIView {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelTotalWeight: UILabel!
    
    func setupXib(model: ModManifestTotal){
        self.xibSetup(nibName: "UIManifestTotal")
        if model.is_complete {
            self.labelTotalWeight.text = (Double().formatNumber(model.total_weight!, toPlaces: 2))! + " kg"
        }
        
        // setup container
        self.setupContainer()
    }
    
    func setupContainer(){
        //
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
    
}

class ModManifestTotal{
    public var total_weight: Double?
    public var is_complete: Bool = false
}
