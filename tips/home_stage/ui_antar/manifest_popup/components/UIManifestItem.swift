//
//  UIManifestItem.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIManifestItem: CustomUIView {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelContentName: UILabel!
    @IBOutlet weak var labelContentWeight: UILabel!
    @IBOutlet weak var stackHoriView: UIStackView!
    
    func setupXib(model: ModManifestItem){
        self.xibSetup(nibName: "UIManifestItem")
        if model.is_complete {
            self.labelContentName.text = model.name
            self.labelContentWeight.text = (Double().formatNumber(model.weight!, toPlaces: 2))! + " kg"
            self.containerView.frame = CGRect(x: 0, y: 0,
                                              width: self.containerView.frame.size.width,
                                              height: 4 * 2 + (self.labelContentName.font.pointSize) *
                                                      CGFloat(self.labelContentName.numberTruncated + 1))
        }
        
        // setup container
        self.setupContainer()
    }
    
    func setupContainer(){
        //
        self.layer.cornerRadius = 0
        self.layer.masksToBounds = false
        self.backgroundColor = self.containerView.backgroundColor
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}

class ModManifestItem{
    public var name: String?
    public var weight: Double?
    public var is_complete: Bool = false
}

