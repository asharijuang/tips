//
//  UIManifestTutup.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIManifestTutup: CustomUIView {
    @IBOutlet weak var containerView: UIView!
    
    func setupXib(){
        self.xibSetup(nibName: "UIManifestTutup")
        
        // setup container
        self.setupContainer()
    }
    
    func setupContainer(){
        //
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}

