//
//  UIManifestHeader.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIManifestHeader: CustomUIView {
    @IBOutlet weak var containerView: UIView!
    
    func setupXib(){
        self.xibSetup(nibName: "UIManifestHeader")
        
        // setup container
        self.setupContainer()
    }
    
    func setupContainer(){
        //
    }
    
    func getSize() -> CGSize{
        print(self.containerView.debugDescription)
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}

class ModManifestHeader{
    public var is_complete: Bool = false
}
