//
//  ManifestPopupVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 20/08/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class ManifestPopupVC: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    
    var goods_data: [Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        self.view.isOpaque = false
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupScroll(goods: self.goods_data!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupScroll(goods: [Any]){
        
        let viewContainer: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        let global_widht = scrollView.bounds.width
        let header: UIManifestHeader = UIManifestHeader()
        header.setupXib()
        header.frame = CGRect(x: 0, y: 0, width: global_widht, height: header.getSize().height)
        viewContainer.addSubview(header)
        
        var ylast = header.frame.height
        
        var total_weight: Double = Double(0)
        for goods_item in goods {
            let kvgoods = goods_item as! [String: Any]
            let modelItem = ModManifestItem()
            modelItem.name = kvgoods["shipment_contents"] as? String
            modelItem.weight = kvgoods["real_weight"] as? Double
            modelItem.is_complete = true
            total_weight += modelItem.weight!
            
            let item: UIManifestItem = UIManifestItem()
            item.setupXib(model: modelItem)
            item.frame = CGRect(x: 0, y: ylast,
                                width: global_widht,
                                height: item.getSize().height)
            ylast += item.frame.height
            viewContainer.addSubview(item)
        }
        
        let total: UIManifestTotal = UIManifestTotal()
        let model_total_weight = ModManifestTotal()
        model_total_weight.total_weight = total_weight
        model_total_weight.is_complete = true
        total.setupXib(model: model_total_weight)
        total.frame = CGRect(x: 0, y: ylast, width: global_widht, height: total.getSize().height)
        ylast += total.frame.height
        viewContainer.addSubview(total)
        
        let footer: UIManifestTutup = UIManifestTutup()
        footer.setupXib()
        footer.frame = CGRect(x: 0, y: ylast, width: global_widht, height: footer.getSize().height)
        ylast += footer.frame.height
        viewContainer.addSubview(footer)
        
        footer.addTapGestureRecognizer {
            self.dismiss(animated: true, completion: nil)
        }
        
        scrollView.addSubview(viewContainer)
        scrollView.contentSize = CGSize(width: scrollView.bounds.width, height: ylast)
        viewContainer.frame = CGRect(x: 0, y: 0, width: global_widht, height: ylast)
        
        if ylast < scrollView.frame.height {
            let middlex = scrollView.frame.height / 2
            let real_top = middlex - ylast / 2
            
            let rectscroller = CGRect(x: scrollView.frame.origin.x, y: real_top, width: global_widht, height: ylast)
            scrollView.frame = rectscroller
        }
        
        viewContainer.layer.cornerRadius = CGFloat(10)
        viewContainer.layer.masksToBounds = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
