//
//  UIAntarStep1.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIAntarStep1: CustomUIView {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelSlotId: UILabel!
    @IBOutlet weak var miniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var flightOriDest: UIFlightOriDest!
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func setupXib(modAntarStep1: ModAntarStep1){
        if(modAntarStep1.is_complete){
            self.xibSetup(nibName: "UIAntarStep1")
            labelSlotId.text = modAntarStep1.slot_id
            miniStepDetail.xibSetup(modMiniStepDetail: modAntarStep1.mod_mini_step_detail)
            flightOriDest.setupXib(modFlightOriDest: modAntarStep1.mod_flight_ori_dest)
            self.setupContainer()
        }else{
            print("your ModAntarStep1 instance is not complete yet")
        }
    }
    
    func setupXib(code: String, isShipment: Bool, n: Int, deskripsi: String, detail: String?){
        self.xibSetup(nibName: "UIAntarStep1")
        labelSlotId.text = code
        miniStepDetail.xibSetup(isShipment: isShipment, n: n, deskripsi: deskripsi, detail: detail)
        flightOriDest.setupXib()
        self.setupContainer()
    }
    
    func setupContainer(){
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = UIColor.white
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}


class ModAntarStep1{
    public var slot_id: String = ""
    public var mod_mini_step_detail: ModMiniStepDetail = ModMiniStepDetail()
    public var mod_flight_ori_dest: ModFlightOriDest = ModFlightOriDest()
    public var is_complete: Bool = false
}

