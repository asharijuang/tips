//
//  UIAntarStep4.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 14/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIAntarStep4: CustomUIView{
    
    @IBOutlet weak var textIngat: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelSlotId: UILabel!
    @IBOutlet weak var flightOriDest: UIFlightOriDest!
    @IBOutlet weak var miniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var imageManifest: UIImageView!
    
    func setupXib(modAntarStep4: ModAntarStep4){
        if(modAntarStep4.is_complete){
            self.xibSetup(nibName: "UIAntarStep4")
            labelSlotId.text = modAntarStep4.slot_id
            miniStepDetail.xibSetup(modMiniStepDetail: modAntarStep4.mod_mini_step_detail)
            flightOriDest.setupXib(modFlightOriDest: modAntarStep4.mod_flight_ori_dest)
            self.setupContainer()
        }else{
            print("your ModAntarStep4 instance is not complete yet")
        }
    }
    
    func setupXib(code: String, isShipment: Bool, n: Int, deskripsi: String, detail: String?){
        self.xibSetup(nibName: "UIAntarStep4")
        labelSlotId.text = code
        miniStepDetail.xibSetup(isShipment: isShipment, n: n, deskripsi: deskripsi, detail: detail)
        flightOriDest.setupXib()
        self.setupContainer()
    }
    
    func setupContainer(){
        textIngat.text = "INGAT!\nTekan tombol kamera untuk mengambil gambar label bagasi Anda"
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = UIColor.white
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}

class ModAntarStep4{
    public var slot_id: String = ""
    public var mod_mini_step_detail: ModMiniStepDetail = ModMiniStepDetail()
    public var mod_flight_ori_dest: ModFlightOriDest = ModFlightOriDest()
    public var is_complete: Bool = false
}
