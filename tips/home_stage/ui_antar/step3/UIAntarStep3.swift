//
//  UIAntarStep3.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class UIAntarStep3: CustomUIView{
    @IBOutlet weak var labelSlotId: UILabel!
    @IBOutlet weak var miniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var flightOriDest: UIFlightOriDest!
    @IBOutlet weak var imageBarcode: UIImageView!
    @IBOutlet weak var textWeightBarcode: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageManifest: UIImageView!
    
    func setupXib(modAntarStep3: ModAntarStep3){
        if(modAntarStep3.is_complete){
            self.xibSetup(nibName: "UIAntarStep3")
            labelSlotId.text = modAntarStep3.slot_id
            miniStepDetail.xibSetup(modMiniStepDetail: modAntarStep3.mod_mini_step_detail)
            flightOriDest.setupXib(modFlightOriDest: modAntarStep3.mod_flight_ori_dest)
            textWeightBarcode.text = modAntarStep3.goods_weight + " Kg"
            
            // hide imageManifest on step < 4
            imageManifest.isHidden = modAntarStep3.mod_mini_step_detail.step < 4
            
            let urlString = modAntarStep3.barcode_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: urlString)
            self.imageBarcode.sd_setImage(with: url) { (image, err, sdicach, url) in
                self.imageBarcode.image = image
            }
            
            // TODO: imageBarcode diisi dengan modAntarStep3.barcode_url
            self.setupContainer()
        }else{
            print("your ModAntarStep3 instance is not complete yet")
        }
    }
    
    func setupXib(code: String, isShipment: Bool, n: Int, deskripsi: String, detail: String?){
        self.xibSetup(nibName: "UIAntarStep3")
        labelSlotId.text = code
        miniStepDetail.xibSetup(isShipment: isShipment, n: n, deskripsi: deskripsi, detail: detail)
        flightOriDest.setupXib()
        self.setupContainer()
    }
    
    func setupContainer(){
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = UIColor.white
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}

class ModAntarStep3{
    public var slot_id: String = ""
    public var mod_mini_step_detail: ModMiniStepDetail = ModMiniStepDetail()
    public var mod_flight_ori_dest: ModFlightOriDest = ModFlightOriDest()
    public var barcode_url: String = ""
    public var goods_weight: String = ""
    public var is_complete: Bool = false
}
