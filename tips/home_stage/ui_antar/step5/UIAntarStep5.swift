//
//  UIAntarStep5.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 14/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIAntarStep5: CustomUIView{
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelSlotId: UILabel!
    @IBOutlet weak var miniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var imageQR: UIImageView!
    @IBOutlet weak var textWeightKg: UILabel!
    @IBOutlet weak var imagePhotoTag: UIImageView!
    @IBOutlet weak var imageManifest: UIImageView!
    
    func setupXib(modAntarStep5: ModAntarStep5){
        if(modAntarStep5.is_complete){
            self.xibSetup(nibName: "UIAntarStep5")
            labelSlotId.text = modAntarStep5.slot_id
            miniStepDetail.xibSetup(modMiniStepDetail: modAntarStep5.mod_mini_step_detail)
            self.textWeightKg.text = modAntarStep5.goods_weight + " Kg"
            
            // set qr image
            let urlString = modAntarStep5.barcode_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url = URL(string: urlString)
            self.imageQR.sd_setImage(with: url) { (image, err, sdicach, url) in
                self.imageQR.image = image
            }
            
            // set photo tag image
            let urlPhotoTagString = modAntarStep5.photo_tag_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let urlPhotoTag = URL(string: urlPhotoTagString)
            self.imagePhotoTag.sd_setImage(with: urlPhotoTag) { (image, err, sdicach, url) in
                self.imagePhotoTag.image = image
            }
            
            // setup container
            self.setupContainer()
        }else{
            print("your ModAntarStep5 instance is not complete yet")
        }
    }
    
    func setupXib(code: String, isShipment: Bool, n: Int, deskripsi: String, detail: String?){
        self.xibSetup(nibName: "UIAntarStep5")
        labelSlotId.text = code
        miniStepDetail.xibSetup(isShipment: isShipment, n: n, deskripsi: deskripsi, detail: detail)
        self.setupContainer()
    }
    
    func setupContainer(){
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = UIColor.white
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}

class ModAntarStep5{
    public var slot_id: String = ""
    public var mod_mini_step_detail: ModMiniStepDetail = ModMiniStepDetail()
    public var barcode_url: String = ""
    public var photo_tag_url: String = ""
    public var goods_weight: String = ""
    public var is_complete: Bool = false
}

