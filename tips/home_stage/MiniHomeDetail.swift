//
//  MiniHomeDetail.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 08/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class MiniHomeDetail: XUIViewController {
    var detailDatas: [[String: Any]] = []
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initData(){
        
    }
    
    
}
