//
//  UIKirim>1.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 10/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIKirimStep_1: CustomUIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelKodeKirim: UILabel!
    @IBOutlet weak var miniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var labelBerat: UILabel!
    @IBOutlet weak var labelKonten: UILabel!
    @IBOutlet weak var labelNamaPengirim: UILabel!
    @IBOutlet weak var labelAlamatPengirim: UILabel!
    @IBOutlet weak var labelNoHPPengirim: UILabel!
    @IBOutlet weak var labelNamaPenerima: UILabel!
    @IBOutlet weak var labelAlamatPenerima: UILabel!
    @IBOutlet weak var labelNoHPPenerima: UILabel!
 
    var totalCharOnLeftView: Int = 0
    var totalCharOnRightView: Int = 0
    
    func setupXib(){
        self.xibSetup(nibName: "UIKirimStep_1")
        self.setupContainer()
    }
    
    func setupXib(modKirimStep_1: ModKirimStep_1){
        if modKirimStep_1.is_complete {
            self.setupXib(step              : modKirimStep_1.step,
                          deskripsi         : modKirimStep_1.deskripsi,
                          detail_deskripsi  : modKirimStep_1.detail_deskripsi,
                          kode              : modKirimStep_1.kode,
                          berat             : modKirimStep_1.berat,
                          konten            : modKirimStep_1.konten,
                          nama_penerima     : modKirimStep_1.nama_penerima,
                          alamat_penerima   : modKirimStep_1.alamat_penerima,
                          detail_alamat_penerima: modKirimStep_1.catatan_alamat_penerima,
                          no_hp_penerima    : modKirimStep_1.no_hp_penerima,
                          nama_pengirim     : modKirimStep_1.nama_pengirim,
                          alamat_pengirim   : modKirimStep_1.alamat_pengirim,
                          detail_alamat_pengirim: modKirimStep_1.catatan_alamat_pengirim,
                          no_hp_pengirim    : modKirimStep_1.no_hp_pengirim)
        } else {
            print("your ModKirimStep_1 instance is not complete yet")
        }
    }
    
    func setupXib(step: Int, deskripsi: String, detail_deskripsi: String,
                  kode: String, berat: Double, konten: String,
                  nama_penerima: String, alamat_penerima: String, detail_alamat_penerima: String, no_hp_penerima: String,
                  nama_pengirim: String, alamat_pengirim: String, detail_alamat_pengirim: String, no_hp_pengirim: String){
        self.xibSetup(nibName: "UIKirimStep_1")
        self.miniStepDetail.xibSetup(isShipment: true, n: step, deskripsi: deskripsi, detail: detail_deskripsi)
        
        self.labelKodeKirim.text    = kode
        self.labelBerat.text        = String(Int(berat)) + " kg"
        labelKonten.text            = konten
        
        labelNamaPenerima.text      = nama_penerima
        labelAlamatPenerima.text    = alamat_penerima + "\n - " + detail_alamat_penerima
        labelNoHPPenerima.text      = no_hp_penerima
        
        labelNamaPenerima.sizeToFit()
        labelAlamatPenerima.sizeToFit()
        labelNoHPPenerima.sizeToFit()
        
        totalCharOnRightView         += labelNamaPenerima.text!.count
        totalCharOnRightView         += labelAlamatPenerima.text!.count
        totalCharOnRightView         += labelNoHPPenerima.text!.count
        
        labelNamaPengirim.text      = nama_pengirim
        labelAlamatPengirim.text    = alamat_pengirim + "\n - " + detail_alamat_pengirim
        labelNoHPPengirim.text      = no_hp_pengirim
        
        labelNamaPengirim.sizeToFit()
        labelAlamatPengirim.sizeToFit()
        labelNoHPPengirim.sizeToFit()
        
        totalCharOnLeftView        += labelNamaPengirim.text!.count
        totalCharOnLeftView        += labelAlamatPengirim.text!.count
        totalCharOnLeftView        += labelNoHPPengirim.text!.count
        
        self.setupContainer()
    }
    
    func setupContainer(){
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = UIColor.white
        self.setNeedsLayout()
        
        let rightIsActive = totalCharOnRightView > totalCharOnLeftView
        print("total cleft \(totalCharOnLeftView)")
        print("total cright \(totalCharOnRightView)")
        
    }
    
    func getSize() -> CGSize{
        return self.containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return self.containerView.frame
    }
}

class ModKirimStep_1 {
    public var step: Int = 1
    public var deskripsi: String = ""
    public var detail_deskripsi = ""
    public var kode: String = ""
    public var berat: Double = 0.0
    public var konten: String = ""
    
    public var nama_pengirim: String = ""
    public var alamat_pengirim: String = ""
    public var catatan_alamat_pengirim: String = ""
    public var no_hp_pengirim: String = ""
    
    public var nama_penerima: String = ""
    public var alamat_penerima: String = ""
    public var catatan_alamat_penerima: String = ""
    public var no_hp_penerima: String = ""
    
    public var is_complete: Bool = false
}
