//
//  UIKirimStep1.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 10/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIKirimStep1: CustomUIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet weak var kirim_1_constraintheight: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var kirimStep_1: UIKirimStep_1!
    @IBOutlet public weak var buttonCancel: UIButton!
    @IBOutlet public weak var buttonConfirm: UIButton!

    func setupXib(){
        self.xibSetup(nibName: "UIKirimStep1")
        self.setupContainer()
    }
    
    func setupXib(modKirimStep1: ModKirimStep1){
        if modKirimStep1.is_complete {
            self.setupXib(modKirimStep_1: modKirimStep1.kirimStep_1)
        } else {
            print("your ModKirimStep1 instance is not complete yet")
        }
    }
    
    func setupXib(modKirimStep_1: ModKirimStep_1){
        self.xibSetup(nibName: "UIKirimStep1")
        self.kirimStep_1.setupXib(modKirimStep_1: modKirimStep_1)
        ViewController.setFilledLabel(button: self.buttonCancel, text: "Back", colorGrayElseRed: true, ratioCircle: 0.4)
        ViewController.setFilledLabel(button: self.buttonConfirm, text: "Cancel Shipment", colorGrayElseRed: false, ratioCircle: 0.4)
        
        self.setupContainer()
    }
    
    func setupContainer(){
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = UIColor.white
    }
    
    func getSize() -> CGSize{
        return self.containerView.frame.size
    }
    
    func getRect() -> CGRect{
        print(self.containerView == nil)
        return self.containerView.frame
    }
}

class ModKirimStep1{
    public var kirimStep_1: ModKirimStep_1 = ModKirimStep_1()
    public var is_complete: Bool = false
}
