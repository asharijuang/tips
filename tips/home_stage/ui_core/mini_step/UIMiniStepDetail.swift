//
//  ShipmentMiniDetail.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 08/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIMiniStepDetail: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var contentView : UIView?
    
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelDeskripsi: UILabel!
    @IBOutlet weak var garis: UIView!
    @IBOutlet weak var n1: UILabelPadding!
    @IBOutlet weak var n2: UILabelPadding!
    @IBOutlet weak var n3: UILabelPadding!
    @IBOutlet weak var n4: UILabelPadding!
    @IBOutlet weak var n5: UILabelPadding!
    @IBOutlet weak var n6: UILabelPadding!
    @IBOutlet weak var n7: UILabelPadding!
    @IBOutlet weak var n8: UILabelPadding!
    
    
    @IBOutlet weak var o1: UILabelPaddingO!
    @IBOutlet weak var o2: UILabelPaddingO!
    @IBOutlet weak var o3: UILabelPaddingO!
    @IBOutlet weak var o4: UILabelPaddingO!
    @IBOutlet weak var o5: UILabelPaddingO!
    @IBOutlet weak var o6: UILabelPaddingO!
    @IBOutlet weak var o7a: UILabelPaddingO!
    @IBOutlet weak var o7: UILabelPaddingO!
    
    @IBOutlet weak var viewO7a: UIView!
    @IBOutlet weak var viewN8: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(isShipment: Bool, n: Int, deskripsi: String, detail: String?) {
        self.init(frame: CGRect.zero)
        xibSetup(isShipment: isShipment, n: n, deskripsi: deskripsi, detail: detail)
    }
    
    convenience init(modMiniStepDetail: ModMiniStepDetail) {
        self.init(frame: CGRect.zero)
        if(modMiniStepDetail.is_complete){
            xibSetup(isShipment : modMiniStepDetail.is_shipment,
                     n          : modMiniStepDetail.step,
                     deskripsi  : modMiniStepDetail.deskripsi,
                     detail     : modMiniStepDetail.detail)
        }
    }
    
    func xibSetup(modMiniStepDetail: ModMiniStepDetail){
        if(modMiniStepDetail.is_complete){
            xibSetup(isShipment : modMiniStepDetail.is_shipment,
                     n          : modMiniStepDetail.step,
                     deskripsi  : modMiniStepDetail.deskripsi,
                     detail     : modMiniStepDetail.detail)
        }else{
            print("your ModMiniStepDetail instance is not complete yet")
        }
    }
    
    func xibSetup(isShipment: Bool, n: Int, deskripsi: String, detail: String?) {
        contentView = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // autoresize
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
        
        labelDeskripsi.text = deskripsi
        if(detail != nil){
            labelDetail.text = detail
        }else{
            labelDetail.isHidden = true
        }
        
        setupCircle(viewLabel: n1)
        setupCircle(viewLabel: n2)
        setupCircle(viewLabel: n3)
        setupCircle(viewLabel: n4)
        setupCircle(viewLabel: n5)
        setupCircle(viewLabel: n6)
        setupCircle(viewLabel: n7)
        setupCircle(viewLabel: n8)
        
        setupCircleO(viewLabel: o1)
        setupCircleO(viewLabel: o2)
        setupCircleO(viewLabel: o3)
        setupCircleO(viewLabel: o4)
        setupCircleO(viewLabel: o5)
        setupCircleO(viewLabel: o6)
        setupCircleO(viewLabel: o7)
        setupCircleO(viewLabel: o7a)
        
        if(!isShipment){
            viewN8.isHidden = true
            viewO7a.isHidden = true
        }
        
        showStepN(is_shipment: isShipment, n: n)
    }
    
    func setupCircle(viewLabel: UILabelPadding){
        let samewh = NSLayoutConstraint(item: viewLabel, attribute: .width, relatedBy: .equal, toItem: viewLabel, attribute: .height, multiplier: 1.0, constant: 0)
        viewLabel.addConstraint(samewh)
        viewLabel.backgroundColor = UIColor(rgb: 0x417DBC)
        viewLabel.textAlignment = .center
        viewLabel.layer.cornerRadius = viewLabel.intrinsicContentSize.height / 2
        viewLabel.layer.masksToBounds = true
        viewLabel.textColor = UIColor.white
        viewLabel.isHidden = true
    }
    
    func setupCircleO(viewLabel: UILabelPaddingO){
        viewLabel.text = ""
        let samewh = NSLayoutConstraint(item: viewLabel, attribute: .width, relatedBy: .equal, toItem: viewLabel, attribute: .height, multiplier: 1.0, constant: 0)
        viewLabel.addConstraint(samewh)
        viewLabel.backgroundColor = UIColor.white
        viewLabel.textAlignment = .center
        viewLabel.layer.cornerRadius = viewLabel.intrinsicContentSize.height / 2
        viewLabel.layer.masksToBounds = true
        viewLabel.textColor = UIColor.white
        viewLabel.layer.borderWidth = 2
        viewLabel.layer.borderColor = garis.backgroundColor?.cgColor
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "UIMiniStepDetail", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    func showStepN(is_shipment: Bool, n: Int){
        var arrlabel: [UILabelPadding];
        
        if(is_shipment){
            arrlabel = [n1, n2, n3, n4, n5, n6, n7, n8]
        }else{
            arrlabel = [n1, n2, n3, n4, n5, n6, n7]
        }
        
        arrlabel[n - 1].isHidden = false
    }
}

class UILabelPadding: UILabel {
    let padding = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }
    
    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
}

class UILabelPaddingO: UILabel {
    let padding = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }
    
    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
}

class ModMiniStepDetail{
    public var is_shipment: Bool = false
    public var step: Int = 1
    public var deskripsi: String = ""
    public var detail: String = ""
    public var is_complete: Bool = false
}
