//
//  CustomUIView.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 12/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class CustomUIView: UIView{
    
    var contentView : UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func xibSetup(nibName: String) {
        contentView = loadViewFromNib(nibName: nibName)
        
        
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // autoresize
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib(nibName: String) -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 10
    }
}

