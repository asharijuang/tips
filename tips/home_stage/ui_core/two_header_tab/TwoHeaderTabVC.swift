//
//  TwoHeaderTabVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 04/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class TwoHeaderTabVC: XUIViewController, UIScrollViewDelegate {

    @IBOutlet weak var titleBar: UILabel!
    @IBOutlet weak var vcMAIN: UIView!
    @IBOutlet weak var constraintTabContent: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthContainer: NSLayoutConstraint!
    @IBOutlet weak var headerScrollView: UIScrollView!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var cv1: UIView!
    @IBOutlet weak var cv2: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentContainerView: UIView!
    
    public static let MODE_S_DAN_K: Int = 71
    public static let MODE_PESAN: Int = 72
    public static let MODE_BANTUAN: Int = 73
    public static let MODE_RIWAYAT_ORDER: Int = 74
    public static let MODE_SA_SAJA: Int = 75
    public static let MODE_SK_SAJA: Int = 76
    public static var tab1title: String = ""
    public static var tab2title: String = ""
    
    public var mode: Int?
    public var callbackOnBack: (() -> ())?
    public var callbackOnSlideScroll: ((_ position: Int) -> ())?
    
    var usingHeaderTab: Bool = false
    var pesanVC: PesanVC?
    
    // border bottom
    var border = CALayer()
    var borderBottomWidth = CGFloat(0)
    var borderBottomYPos = CGFloat(0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backImage.addTapGestureRecognizer {
            self.doBack()
        }
        
        if self.mode == nil {
            return
        }
        
        switch self.mode {
        case TwoHeaderTabVC.MODE_S_DAN_K:
            self.segueModeSyaratDanKetentuan()
            break
        case TwoHeaderTabVC.MODE_RIWAYAT_ORDER:
            self.segueRiwayatOrder()
            break
        case TwoHeaderTabVC.MODE_BANTUAN:
            self.segueModeBantuan()
            break
        case TwoHeaderTabVC.MODE_PESAN:
            self.segueModePesan()
            break
        case TwoHeaderTabVC.MODE_SA_SAJA:
            self.segueModeSyaratAntarSaja()
            break
        case TwoHeaderTabVC.MODE_SK_SAJA:
            self.segueModeSyaratKirimSaja()
            break
        case .none:
            break;
        case .some(_):
            break;
        }
        // Do any additional setup after loading the view.
        
        self.setupLayoutContainer()
    }
    
    func segueModeSyaratDanKetentuan(){
        titleBar.text = "Syarat & Ketentuan"
        usingHeaderTab = true
        self.performSegue(withIdentifier: "segue_syarat_antar", sender: nil)
        self.performSegue(withIdentifier: "segue_syarat_kirim", sender: nil)
    }
    
    func segueModeSyaratAntarSaja(){
        titleBar.text = "Syarat & Ketentuan"
        usingHeaderTab = false
        self.performSegue(withIdentifier: "segue_syarat_antar_saja", sender: nil)
    }
    
    func segueModeSyaratKirimSaja(){
        titleBar.text = "Syarat & Ketentuan"
        usingHeaderTab = false
        self.performSegue(withIdentifier: "segue_syarat_kirim_saja", sender: nil)
    }
    
    func segueModeBantuan(){
        titleBar.text = "Bantuan"
        usingHeaderTab = false
        self.performSegue(withIdentifier: "segue_bantuan", sender: nil)
    }
    
    func segueRiwayatOrder(){
        titleBar.text = "Riwayat Order"
        usingHeaderTab = true
        self.performSegue(withIdentifier: "segue_riwayat_order_antar", sender: nil)
        self.performSegue(withIdentifier: "segue_riwayat_order_kirim", sender: nil)
    }
    
    func segueModePesan(){
        titleBar.text = "Pesan"
        usingHeaderTab = false
        self.performSegue(withIdentifier: "segue_pesan", sender: nil)
        self.performSegue(withIdentifier: "segue_pesan_detail", sender: nil)
    }
    
    func setupLayoutContainer(){
        if !usingHeaderTab {
            headerScrollView.isHidden = true
            constraintTabContent.constant = CGFloat(0)
            scrollView.isScrollEnabled = false
        }
        
        
        let border = CALayer()
        border.backgroundColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.headerScrollView.frame.size.height - 1,
                              width: self.headerScrollView.frame.size.width, height: 1)
        self.headerScrollView.layer.addSublayer(border)
        
        vcMAIN.layer.cornerRadius = 10
        vcMAIN.layer.masksToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // to get actual scroll view frame size width on phone
        if usingHeaderTab {
            let sv = UIViewController.displaySpinner(onView: self.view)
            self.prepareTabView()
            UIViewController.removeSpinner(spinner: sv)
            
            // only delegate when using tab header
            self.callbackOnSlideScroll = ({ (position) -> () in
                self.focusToCurrentChild(i: position)
            })
        }
        
        self.scrollView.delegate = self
    }
    
    func prepareTabView() {
        print(headerScrollView.bounds.size.width)
        let width_header_item_tab  = headerScrollView.bounds.size.width / 2
        let height_header_item_tab = headerScrollView.bounds.size.height
        
        self.borderBottomWidth = width_header_item_tab

        let tabFrame1 = CGRect(x: 0, y: 0,
                              width: width_header_item_tab,
                              height: height_header_item_tab)
        
        let tabFrame2 = CGRect(x: width_header_item_tab, y: 0,
                               width: width_header_item_tab,
                               height: height_header_item_tab)
        
        let header1 = self.createHeaderTabView(frame: tabFrame1, label: TwoHeaderTabVC.tab1title, withImage: nil)
        let header2 = self.createHeaderTabView(frame: tabFrame2, label: TwoHeaderTabVC.tab2title, withImage: nil)
        
        header1.addTapGestureRecognizer {
            self.focusToCurrentChild(i: 0)
        }
        
        header2.addTapGestureRecognizer {
            self.focusToCurrentChild(i: 1)
        }
        
        print(header1.frame.debugDescription)
        print(header2.frame.debugDescription)
        
        headerScrollView.addSubview(header1)
        headerScrollView.addSubview(header2)
        
        self.borderBottomYPos = height_header_item_tab - CGFloat(2)
        self.border.backgroundColor = UIColor(rgb: ViewController.PRIMARY_COLOR).cgColor
        self.border.frame = CGRect(x: 0, y: self.borderBottomYPos,
                                   width: self.borderBottomWidth, height: CGFloat(2))
        headerScrollView.layer.addSublayer(self.border)
    }
    
    func doBack(){
        if self.callbackOnBack != nil {
            self.callbackOnBack?()
        } else {
            self.backStack()
        }
    }
    
    func focusToCurrentChild(i: Int){
        UIView.animate(withDuration: 150, animations: {
            self.border.frame = CGRect(x: CGFloat(i) * self.borderBottomWidth, y: self.borderBottomYPos,
                                       width: self.borderBottomWidth, height: CGFloat(2))
            self.scrollView.setContentOffset(CGPoint(x: CGFloat(i) * self.scrollView.frame.size.width, y: 0), animated: true)
        })
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(self.scrollView.contentOffset.x / self.scrollView.frame.size.width)

        if self.callbackOnSlideScroll != nil {
            self.callbackOnSlideScroll?(Int(pageNumber))
        } else {
            print("self.callbackOnSlideScroll is null")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // identified as first tab segue, or single view (no tab)
        if (segue.identifier! == "segue_syarat_antar" || segue.identifier! == "segue_bantuan" ||
            segue.identifier! == "segue_riwayat_order_antar" || segue.identifier! == "segue_pesan" ||
            segue.identifier! == "segue_syarat_antar_saja" || segue.identifier! == "segue_syarat_kirim_saja") {
            
            // *** SPECIAL CASE *** //
            // i love somthing special
            if segue.identifier! == "segue_pesan" {
                let dest = segue.destination as? PesanVC
                self.pesanVC = dest
                let src = segue.source as? TwoHeaderTabVC
                dest?.parentVC = src
                
                self.callbackOnBack = ({ () -> () in
                    if (dest?.isOnDetailMessage)! {
                        print("MSG:gotoleft")
                        dest?.slideLeftToListMessage()
                    } else {
                        print("MSG:backkk")
                        self.backStack()
                    }
                })
                
                // to do: masih blm jalan...
                self.callbackOnSlideScroll = ({ (position) -> () in
                    if position == 0 {
                        print("MSG:its 0")
                        dest?.isOnDetailMessage = false
                        self.scrollView.isScrollEnabled = false
                    } else {
                        print("MSG:its not 0")
                        dest?.isOnDetailMessage = true
                        self.scrollView.isScrollEnabled = true
                    }
                })
            }
            
            let cv1vc = segue.destination
            self.addChildViewController(cv1vc)
            cv1vc.view.translatesAutoresizingMaskIntoConstraints = false
            self.cv1.addSubview(cv1vc.view)
            
            NSLayoutConstraint.activate([
                cv1vc.view.leadingAnchor.constraint(equalTo: self.cv1.leadingAnchor),
                cv1vc.view.trailingAnchor.constraint(equalTo: self.cv1.trailingAnchor),
                cv1vc.view.topAnchor.constraint(equalTo: self.cv1.topAnchor),
                cv1vc.view.bottomAnchor.constraint(equalTo: self.cv1.bottomAnchor)
            ])
            
            cv1vc.didMove(toParentViewController: self)
        }
        
        // identified as second tab segue, only with tab view
        if (segue.identifier! == "segue_syarat_kirim" || segue.identifier! == "segue_riwayat_order_kirim" ||
            segue.identifier! == "segue_pesan_detail") {
            
            // *** SPECIAL CASE *** //
            // i love somthing special
            if segue.identifier! == "segue_pesan_detail" {
                if self.pesanVC != nil {
                    pesanVC?.detailVC = segue.destination as? PesanDetailVC
                }
                let dest = segue.destination as? PesanDetailVC
                let src = segue.source as? TwoHeaderTabVC
                dest?.parentVC = src
            }
            
            let cv2vc = segue.destination
            self.addChildViewController(cv2vc)
            cv2vc.view.translatesAutoresizingMaskIntoConstraints = false
            self.cv2.addSubview(cv2vc.view)
            
            NSLayoutConstraint.activate([
                cv2vc.view.leadingAnchor.constraint(equalTo: self.cv2.leadingAnchor),
                cv2vc.view.trailingAnchor.constraint(equalTo: self.cv2.trailingAnchor),
                cv2vc.view.topAnchor.constraint(equalTo: self.cv2.topAnchor),
                cv2vc.view.bottomAnchor.constraint(equalTo: self.cv2.bottomAnchor)
                ])
            
            cv2vc.didMove(toParentViewController: self)
        }
    }
    
    
    func createHeaderTabView(frame: CGRect, label: String, withImage: UIImage? = nil) -> UIView{
        // initialize padding size
        let left_right_padding = CGFloat(0)
        
        if withImage != nil {
            let image_width_height = CGFloat(32)
            
            // initialize view instance
            let view = UIView(frame: frame)
            
            // initialize image view
            let image_view = withImage
            let ui_image = UIImageView(image: image_view)
            
            // set image frame and attributes
            ui_image.frame = CGRect(x: CGFloat(left_right_padding), y: CGFloat(0), width: CGFloat(image_width_height), height: CGFloat(image_width_height))
            ui_image.contentMode = .scaleAspectFit
            
            // initialize label/text view
            let width_used = 4 * left_right_padding + ui_image.frame.width
            let code_view = UILabel(frame: CGRect(x: CGFloat(2 * left_right_padding) + ui_image.frame.width, y: CGFloat(0), width: CGFloat(frame.size.width - width_used), height: frame.size.height))
            code_view.textAlignment = .center
            code_view.text = label
            
            // add image and text to view instance
            view.addSubview(ui_image)
            view.addSubview(code_view)
            view.backgroundColor = UIColor.lightGray
            
            return view
        } else {
            // initialize view instance
            let view = UIView(frame: frame)
            
            // initialize label/text view
            let label_text = UILabel(frame: CGRect(x: left_right_padding, y: CGFloat(0),
                                                  width: frame.size.width - 2 * left_right_padding,
                                                  height: frame.size.height))
            label_text.textAlignment = .center
            label_text.text = label
            
            // add image and text to view instance
            view.addSubview(label_text)
            
            return view
        }
    }
    func generateRandomColor() -> UIColor {
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
