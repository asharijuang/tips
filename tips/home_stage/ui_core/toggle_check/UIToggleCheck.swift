//
//  UIToggleCheck.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIToggleCheck: CustomUIView{
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var toggleImage: UIImageView!
    @IBOutlet weak var caption: UILabel!
    var textGestureCallback: (() -> ())?
    var changeable: Bool = true
    
    var stateChecked: Bool = false
    
    func setupXib(text: String? = "", state: Bool = false, isChangeable: Bool? = true){
        self.xibSetup(nibName: "UIToggleCheck")
        self.caption.text = text
        self.setImageState(state: state)
        self.changeable = isChangeable!
        self.toggleImage.addTapGestureRecognizer{
            self.stateChecked = !self.stateChecked
            self.toggleImageState()
        }
        self.caption.addTapGestureRecognizer {
            if self.textGestureCallback != nil {
                self.textGestureCallback!()
            }
        }
    }
    
    func toggleImageState(){
        if !self.changeable {
            return
        }
        
        let image = UIImage(named: self.stateChecked ? "checked" : "not_checked")
        self.toggleImage.image = image
    }
    
    func setImageState(state: Bool){
        if !self.changeable {
            return
        }
        
        self.stateChecked = state
        self.toggleImageState()
    }
    
    func getToggleState() -> Bool {
        return self.stateChecked
    }
    
    func getSize() -> CGSize{
        return containerView.frame.size
    }
    
    func getRect() -> CGRect{
        return containerView.frame
    }
}
