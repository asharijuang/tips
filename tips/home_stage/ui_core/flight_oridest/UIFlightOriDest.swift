//
//  UIFlightOriDest.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIFlightOriDest: CustomUIView {

    @IBOutlet weak var labelOriCity: UILabel!
    @IBOutlet weak var labelOriAirportCode: UILabel!
    @IBOutlet weak var labelOriDateTime: UILabel!
    @IBOutlet weak var labelDestCity: UILabel!
    @IBOutlet weak var labelDestAirportCode: UILabel!
    @IBOutlet weak var labelBookingCode: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func setupXib(){
        self.xibSetup(nibName: "UIFlightOriDest")
    }
    
    func setupXib(modFlightOriDest: ModFlightOriDest){
        if(modFlightOriDest.is_complete){
            self.setupXib(oriCity           : modFlightOriDest.ori_city,
                          oriAirportCode    : modFlightOriDest.ori_airport_code,
                          oriDateTime       : modFlightOriDest.ori_date_time,
                          destCity          : modFlightOriDest.dest_city,
                          destAirportCode   : modFlightOriDest.dest_airport_code,
                          flightCode        : modFlightOriDest.flight_code)
        }else{
            print("your ModFlightOriDest instance is not complete yet")
        }
    }
    
    func setupXib(oriCity: String, oriAirportCode: String, oriDateTime: String,
                  destCity: String, destAirportCode: String, flightCode: String? = nil){
        self.xibSetup(nibName: "UIFlightOriDest")
        labelOriCity.text           = oriCity
        labelOriAirportCode.text    = oriAirportCode
        labelOriDateTime.text       = oriDateTime.split(separator: " ").joined(separator: "\n")
        labelDestCity.text          = destCity
        labelDestAirportCode.text   = destAirportCode
        if(flightCode == nil){
            labelBookingCode.isHidden = true
        }else{
            labelBookingCode.text   = flightCode
        }
    }
}

class ModFlightOriDest{
    public var ori_city: String = ""
    public var ori_airport_code: String = ""
    public var ori_date_time: String = ""
    public var dest_city: String = ""
    public var dest_airport_code: String = ""
    public var flight_code: String = ""
    public var is_complete: Bool = false
}
