//
//  UIMiniStepArrow.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 19/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class UIMiniStepArrow: CustomUIView{
    
    @IBOutlet weak var miniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var downArrowImage: UIImageView!
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    convenience init(modMiniStepArrow: ModMiniStepArrow) {
        self.init(frame: CGRect.zero)
        if(modMiniStepArrow.is_complete){
            self.xibSetup(dataMiniStepDetail: modMiniStepArrow.mini_step_detail, using_arrow: modMiniStepArrow.using_arrow)
        }else{
            print("your ModMiniStepDetail instance is not complete yet")
        }
    }
    
    func xibSetup(modMiniStepArrow: ModMiniStepArrow){
        if(modMiniStepArrow.is_complete){
            self.xibSetup(dataMiniStepDetail: modMiniStepArrow.mini_step_detail, using_arrow: modMiniStepArrow.using_arrow)
        }else{
            print("your ModMiniStepDetail instance is not complete yet")
        }
    }
    
    func xibSetup(dataMiniStepDetail: ModMiniStepDetail, using_arrow: Bool){
        self.xibSetup(nibName: "UIMiniStepArrow")
        downArrowImage.isHidden = !using_arrow
        miniStepDetail.xibSetup(modMiniStepDetail: dataMiniStepDetail)
    }
}

class ModMiniStepArrow{
    var mini_step_detail: ModMiniStepDetail = ModMiniStepDetail()
    var using_arrow: Bool = false
    var is_complete: Bool = false
}
