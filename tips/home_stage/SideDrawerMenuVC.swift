//
//  SideDrawerMenuVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 30/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import SDWebImage

class SideDrawerMenuVC: XUIViewController {

    @IBOutlet weak var labelUang: UILabel!
    @IBOutlet weak var labelKodeReferral: UILabel!
    @IBOutlet weak var imgShareReferral: UIImageView!
    @IBOutlet weak var namaEmailContainer: UIView!
    @IBOutlet weak var fotoProfilImage: UIImageView!
    @IBOutlet weak var labelNama: UILabel!
    @IBOutlet weak var labelEmailDanNoHP: UILabel!
    @IBOutlet weak var buttonKeluar: UIButton!
    @IBOutlet weak var side_bantuan: UIView!
    @IBOutlet weak var viewReferral: UIView!
    @IBOutlet weak var side_syaratDanKetentuan: UIView!
    @IBOutlet weak var side_pesan: UIView!
    @IBOutlet weak var side_riwayatOrder: UIView!
    @IBOutlet weak var constraintTopRefView: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomRefView: NSLayoutConstraint!
    
    var parentdrawer: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.applyProfileData()
        side_syaratDanKetentuan.addTapGestureRecognizer {
            TwoHeaderTabVC.tab1title = "ANTAR"
            TwoHeaderTabVC.tab2title = "KIRIM"
            SideDrawerMenuVC.gotoSideMenu(vcsender: self, mod: TwoHeaderTabVC.MODE_S_DAN_K)
        }
        
        side_bantuan.addTapGestureRecognizer {
            SideDrawerMenuVC.gotoSideMenu(vcsender: self, mod: TwoHeaderTabVC.MODE_BANTUAN)
        }
        
        side_pesan.addTapGestureRecognizer {
            SideDrawerMenuVC.gotoSideMenu(vcsender: self, mod: TwoHeaderTabVC.MODE_PESAN)
        }
        
        side_riwayatOrder.addTapGestureRecognizer {
            TwoHeaderTabVC.tab1title = "ANTAR"
            TwoHeaderTabVC.tab2title = "KIRIM"
            SideDrawerMenuVC.gotoSideMenu(vcsender: self, mod: TwoHeaderTabVC.MODE_RIWAYAT_ORDER)
        }
        
        labelUang.addTapGestureRecognizer {
            self.gotoRedeem(money: self.labelUang.text!)
        }
        namaEmailContainer.addTapGestureRecognizer {
            if !C().isGuest() {
                self.gotoProfileVC()
            }
        }
        fotoProfilImage.addTapGestureRecognizer {
            if !C().isGuest() {
                self.gotoProfileVC()
            }
        }
        buttonKeluar.addTapGestureRecognizer {
            MasterDB().deleteSingleUser()
            MasterDB().resetShipments()
            
            if (self.parentdrawer is AntarDrawerVC || self.parentdrawer is KirimDrawerVC) {
                self.backBackStack()
            } else {
                self.backStack()
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if ViewController.special_drawer_width == nil {
            return
        }
        
        if self.view.frame.width != ViewController.special_drawer_width {
            let space = self.view.frame.width - ViewController.special_drawer_width!
            self.view.frame = CGRect(x: space,
                                     y: self.view.frame.origin.y,
                                     width: ViewController.special_drawer_width!,
                                     height: self.view.frame.size.height)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ViewController.special_drawer_width = self.view.frame.width
        if ProfileVC.hasProfileChange {
            ProfileVC.hasProfileChange = false
            self.applyProfileData()
        }
        
        let isRefShow = S.referral != nil
        viewReferral.isHidden = !isRefShow
        if isRefShow {
            self.imgShareReferral.addTapGestureRecognizer {
                self.showShareReferral(refcode: HomeVC.user.ref_code!,
                                       refferedvalue: (S.referral?.referred_amount)!)
            }
            self.constraintTopRefView.constant = CGFloat(24)
            self.constraintBottomRefView.constant = CGFloat(24)
        } else {
            self.constraintTopRefView.constant = CGFloat(0)
            self.constraintBottomRefView.constant = CGFloat(0)
        }
        
        if HomeVC.user.money != nil {
            let int_money = Int(HomeVC.user.money!)!.formattedWithSeparator
            labelUang.text = "Rp " + int_money
        }
        
    }
    
    public func applyProfileData(){
        if C().isGuest() {
            viewReferral.isHidden = true
        }
        fotoProfilImage.layer.cornerRadius = fotoProfilImage.frame.size.width / 2
        viewReferral.layer.cornerRadius = 4
        viewReferral.layer.masksToBounds = true
        labelNama.text = HomeVC.user.first_name! + " " + (HomeVC.user.last_name ?? "")
        
        if HomeVC.user.email != nil {
            labelEmailDanNoHP.text = HomeVC.user.email! + "\n" + HomeVC.user.mobile_phone_no!
        } else {
            labelEmailDanNoHP.text = "TIPS Guest"
        }
        if HomeVC.user.ref_code != nil {
            labelKodeReferral.text = HomeVC.user.ref_code!
        }
        if HomeVC.user.profil_picture != nil {
            fotoProfilImage.sd_setImage(with: URL(string: HomeVC.user.profil_picture!)) { (image, err, sdicach, url) in
                // self.profileImage.image = image.
                self.fotoProfilImage.image = ViewController.maskCircleImage(image: image!)
            }
        }
        ViewController.setOLabel(button: buttonKeluar, text: C().isGuest() ? "Sign In" : "Sign Out", filled: true, reverseColor: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showShareReferral(refcode: String, refferedvalue: Double){
        let firstActivityItem = "Hi there, let's join TIPS by using https://app.tips.co.id/ref/"
                                + String(refcode) + " and get IDR " + String(refferedvalue) + " now!"

        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        // activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func gotoProfileVC(){
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileVCID") as! ProfileVC
        self.present(vc, animated: true, completion: nil)
    }
    
    public static func gotoSideMenu(vcsender: UIViewController, mod: Int){
        let storyboard = UIStoryboard(name: "Sample", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TwoHeaderTabVCID") as! TwoHeaderTabVC
        vc.mode = mod
        vcsender.present(vc, animated: true, completion: nil)
    }
    
    func gotoRedeem(money: String){
        let storyboard = UIStoryboard(name: "Redeem", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RedeemVCID") as! RedeemVC
        RedeemVC.money = money
        self.present(vc, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        formatter.decimalSeparator = ","
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}
