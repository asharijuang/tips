//
//  LacakVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 13/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class LacakVC: XUIViewController {

    @IBOutlet weak var backgroundSearchbox: UIView!
    @IBOutlet weak var containerSearch: UIView!
    @IBOutlet weak var containerCard: UIView!
    @IBOutlet weak var labelKode: UILabel!
    @IBOutlet weak var stubMiniStepDetail: UIMiniStepDetail!
    @IBOutlet weak var fieldSearch: UITextField!
    @IBOutlet weak var fieldSearchLeading: NSLayoutConstraint!
    @IBOutlet weak var fieldSearchTrailing: NSLayoutConstraint!
    @IBOutlet weak var imageSearch: UIImageView!
    @IBOutlet weak var imageBack: UIImageView!
    
    private var stateSearch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()

        // Do any additional setup after loading the view.
    }
    
    func invalidateState(){
        self.containerCard.isHidden = self.stateSearch
        self.containerSearch.isHidden = !self.stateSearch
    }
    
    func setupLayout(){
        self.stateSearch = true
        self.invalidateState()
        let halfHeight = self.backgroundSearchbox.frame.size.height / 2
        self.backgroundSearchbox.layer.cornerRadius = halfHeight
        self.backgroundSearchbox.layer.masksToBounds = true
        self.fieldSearchLeading.constant = halfHeight
        self.fieldSearchTrailing.constant = halfHeight
        self.fieldSearch.setUpperCase()
        self.fieldSearch.setMaxInputLength(max: 7)
        self.containerCard.layer.cornerRadius = 10
        self.containerCard.layer.masksToBounds = true
        self.imageSearch.addTapGestureRecognizer {
            let kode = self.fieldSearch.text
            self.findShipment(shipment_id: kode!)
        }
        self.imageBack.addTapGestureRecognizer {
            if !self.stateSearch {
                self.stateSearch = true
                self.invalidateState()
            } else {
                self.backStack()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findShipment(shipment_id: String) {
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.request(KirimRouter.status_kirim(parameters: [
            "shipment_id": shipment_id
            ])).responseJSON(completionHandler: {
                json_data in
                
                print(json_data)
                
                // check if response is contains error or no result
                if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                    E.showUIError(vc: self, completion: {
                        UIViewController.removeSpinner(spinner: sv)
                    })
                    return
                }
                
                self.stateSearch = false
                self.invalidateState()
                let _data = E.getData(json_data: json_data)
                
                let _status     = _data["status"] as? [String: Any]
                let _step       = _status!["step"] as? Int
                
                let _s_or_d     = _data["shipment"] as? [String: Any]
                let _desc       = _status!["description"] as? String
                
                let _code       = _s_or_d!["shipment_id"] as? String
                
                let minstde     = ModMiniStepDetail()
                minstde.deskripsi = _desc!
                minstde.is_shipment = true
                minstde.step    = _step!
                minstde.is_complete = true
                
                self.stubMiniStepDetail.xibSetup(modMiniStepDetail: minstde)
                self.labelKode.text = _code!
                UIViewController.removeSpinner(spinner: sv)
            })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
