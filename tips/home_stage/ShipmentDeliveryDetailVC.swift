//
//  ShipmentDataVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 10/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//


import UIKit
import Alamofire

class ShipmentDeliveryDetailVC: XUIViewController {

    @IBOutlet weak var backButton: UIImageView!
    var data: Any?
    var isShipment: Bool?
    var step: Int?
    var completion: (() -> ())?
    let imagePicker = UIImagePickerController()
    
    var slot_id_for_send_tag_kirim: String = ""
    
    @IBOutlet weak var imageCamera: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var cameraButtonContainer: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initAllView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initAllView(){
        cameraButtonContainer.isHidden = true
        self.imageCamera.addTapGestureRecognizer {
            self.showCameraButton()
        }
        initTypeAndStep()
        backButton.addTapGestureRecognizer{
            self.backStack()
        }
    }
    
    func setupCameraButton(){
        cameraButtonContainer.isHidden = false
        cameraButtonContainer.layer.cornerRadius = cameraButtonContainer.frame.size.height / 2
        cameraButtonContainer.dropShadow(color: UIColor.black, opacity: 0.5, offSet: CGSize(width: -1, height: 1),
                                         radius: cameraButtonContainer.frame.size.height / 2, scale: true)
    }
    
    func showCameraButton(){
        self.imagePicker.sourceType = .camera
        self.imagePicker.delegate =  self
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func initTypeAndStep(){
        let _data       = data as? [String: Any]
        
        let _type       = _data?["type"] as? String
        self.isShipment = _type == "S"
        
        let _status     = _data?["status"] as? [String: Any]
        self.step       = _status!["step"] as? Int
    }
    
    func initData(){
        let _data       = data as? [String: Any]
        
        let _type       = _data?["type"] as? String
        let isShipment  = _type == "S"
        
        let _status     = _data?["status"] as? [String: Any]
        let _step       = _status!["step"] as? Int
        
        let _s_or_d     = _data?[isShipment ? "shipment" : "delivery"] as? [String: Any]
        let _desc       = _status!["description"] as? String
        let _detl       = _status!["detail"] as? String
        
        let _code       = _s_or_d![isShipment ? "shipment_id" : "slot_id"] as? String
        
        let minstde         = ModMiniStepDetail()
        minstde.deskripsi   = _desc!
        minstde.is_shipment = isShipment
        minstde.detail      = _detl ?? ""
        minstde.step        = _step!
        minstde.is_complete = true
        
        // for shipment
        let _konten     = (isShipment ? _s_or_d!["shipment_contents"]: "") as? String
        
        let _ndpeng      = (isShipment ? _s_or_d!["shipper_first_name"]: "") as? String
        let _nbpeng      = (isShipment ? _s_or_d!["shipper_last_name"]: "") as? String
        let _alpeng      = (isShipment ? _s_or_d!["shipper_address"]: "") as? String
        let _nhpeng      = (isShipment ? _s_or_d!["shipper_mobile_phone"]: "") as? String
        let _adpeng      = (isShipment ? _s_or_d!["shipper_address_detail"]: "") as? String
        
        let _ndpenr      = (isShipment ? _s_or_d!["consignee_first_name"]: "") as? String
        let _nbpenr      = (isShipment ? _s_or_d!["consignee_last_name"]: "") as? String
        let _alpenr      = (isShipment ? _s_or_d!["consignee_address"]: "") as? String
        let _nhpenr      = (isShipment ? _s_or_d!["consignee_mobile_phone"]: "") as? String
        let _adpenr      = (isShipment ? _s_or_d!["consignee_address_detail"]: "") as? String
        
        let _estwei      = (isShipment ? _s_or_d!["estimate_weight"]: "") as? Double
        let _rilwei      = (isShipment ? _s_or_d!["real_weight"]: "") as? Double
        let _finwei      = _rilwei == nil || _rilwei == 0 ? _estwei : _rilwei
        
        // for delivery
        
        
        var viewDetail: UIView?
        var frameOriRect: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        
        if !isShipment {
            let _sold                   = _s_or_d!["sold_baggage_space"] as? Double
            let _pricekg                = _s_or_d!["slot_price_kg"] as? Int
            let _photo_tag              = _s_or_d!["photo_tag"] as? String
            
            let modflori                = ModFlightOriDest()
            let originAirport           = _s_or_d!["airport_origin"] as! [String: Any]
            let destinationAirport      = _s_or_d!["airport_destination"] as! [String: Any]
            let goods_data              = _s_or_d!["goods"] as! [Any]
            
            modflori.ori_airport_code   = originAirport["initial_code"] as! String
            modflori.ori_city           = _s_or_d!["origin_city"] as! String
            modflori.ori_date_time      = _s_or_d!["depature"] as! String
            modflori.dest_airport_code  = destinationAirport["initial_code"] as! String
            modflori.dest_city          = _s_or_d!["destination_city"] as! String
            modflori.is_complete        = true
            
            
            switch self.step {
            case 1:
                let mod1 = ModAntarStep1()
                mod1.mod_mini_step_detail = minstde
                mod1.mod_flight_ori_dest = modflori
                mod1.slot_id = _code!
                mod1.is_complete = true
                
                let viewDetailSpec = UIAntarStep1()
                viewDetailSpec.setupXib(modAntarStep1: mod1)
                frameOriRect = viewDetailSpec.getRect()
                viewDetail = viewDetailSpec
            case 2:
                let mod2 = ModAntarStep2()
                minstde.detail = "Klik untuk KONFIRMASI / PEMBATALAN"
                mod2.mod_mini_step_detail = minstde
                mod2.number_kg = String(_sold!)
                mod2.number_money = String(_sold! * Double(_pricekg!))
                mod2.slot_id = _code!
                mod2.is_complete = true
                
                let viewDetailSpec = UIAntarStep2()
                viewDetailSpec.setupXib(modAntarStep2: mod2)
                frameOriRect = viewDetailSpec.getRect()
                viewDetailSpec.buttonBatal.addTapGestureRecognizer {
                    self.gestureConfirmDelivery_2(yesElseNo: false, _code: _code!)
                }
                viewDetailSpec.buttonKonfirmasi.addTapGestureRecognizer {
                    if viewDetailSpec.toggleWithCaption.getToggleState() {
                        self.gestureConfirmDelivery_2(yesElseNo: true, _code: _code!)
                    }
                }
                viewDetail = viewDetailSpec
            case 3:
                let mod3 = ModAntarStep3()
                mod3.barcode_url = __CR.BASE_URL + "/qrcodeX?data=" + _code!
                print(mod3.barcode_url)
                mod3.goods_weight = String(_sold!)
                mod3.slot_id = _code!
                mod3.mod_mini_step_detail = minstde
                mod3.mod_flight_ori_dest = modflori
                mod3.is_complete = true
                
                let viewDetailSpec = UIAntarStep3()
                viewDetailSpec.setupXib(modAntarStep3: mod3)
                frameOriRect = viewDetailSpec.getRect()
                viewDetail = viewDetailSpec
            case 4:
                let mod4 = ModAntarStep4()
                mod4.mod_mini_step_detail = minstde
                mod4.mod_flight_ori_dest = modflori
                mod4.slot_id = _code!
                mod4.is_complete = true
                
                let viewDetailSpec = UIAntarStep4()
                viewDetailSpec.setupXib(modAntarStep4: mod4)
                frameOriRect = viewDetailSpec.getRect()
                viewDetail = viewDetailSpec

                self.setupCameraButton()
                self.slot_id_for_send_tag_kirim = _code!
                viewDetailSpec.imageManifest.addTapGestureRecognizer {
                    self.gestureManifestDelivery4to7(kvGoods: goods_data)
                }
            case 5:
                let mod5 = ModAntarStep5()
                mod5.barcode_url = __CR.BASE_URL + "/qrcodeX?data=" + _code!
                mod5.photo_tag_url = _photo_tag ?? "#"
                mod5.goods_weight = String(_sold!)
                mod5.mod_mini_step_detail = minstde
                mod5.slot_id = _code!
                mod5.is_complete = true
                
                let viewDetailSpec = UIAntarStep5()
                viewDetailSpec.setupXib(modAntarStep5: mod5)
                frameOriRect = viewDetailSpec.getRect()
                viewDetail = viewDetailSpec
                viewDetailSpec.imageManifest.addTapGestureRecognizer {
                    self.gestureManifestDelivery4to7(kvGoods: goods_data)
                }
            case 6:
                let mod3 = ModAntarStep3()
                mod3.barcode_url = __CR.BASE_URL + "/qrcodeX?data=" + _code!
                print(mod3.barcode_url)
                mod3.goods_weight = String(_sold!)
                mod3.slot_id = _code!
                mod3.mod_mini_step_detail = minstde
                mod3.mod_flight_ori_dest = modflori
                mod3.is_complete = true
                
                let viewDetailSpec = UIAntarStep3()
                viewDetailSpec.setupXib(modAntarStep3: mod3)
                frameOriRect = viewDetailSpec.getRect()
                viewDetail = viewDetailSpec
                viewDetailSpec.imageManifest.addTapGestureRecognizer {
                    self.gestureManifestDelivery4to7(kvGoods: goods_data)
                }
            case 7:
                let mod3 = ModAntarStep3()
                mod3.barcode_url = __CR.BASE_URL + "/qrcodeX?data=" + _code!
                mod3.goods_weight = String(_sold!)
                mod3.slot_id = _code!
                mod3.mod_mini_step_detail = minstde
                mod3.mod_flight_ori_dest = modflori
                mod3.is_complete = true
                
                let viewDetailSpec = UIAntarStep3()
                viewDetailSpec.setupXib(modAntarStep3: mod3)
                frameOriRect = viewDetailSpec.getRect()
                viewDetail = viewDetailSpec
                viewDetailSpec.imageManifest.addTapGestureRecognizer {
                    self.gestureManifestDelivery4to7(kvGoods: goods_data)
                }
            default:
                return
            }
        } else {
            
            let model               = ModKirimStep_1()
            model.kode              = _code!
            model.konten            = _konten!
            model.berat             = _finwei!
            model.step              = _step!
            model.deskripsi         = _desc!
            model.detail_deskripsi  = ""
            model.nama_pengirim     = _ndpeng! + " " +  _nbpeng!
            model.alamat_pengirim   = _alpeng!
            model.catatan_alamat_pengirim = _adpeng!
            model.no_hp_pengirim    = _nhpeng!
            model.nama_penerima     = _ndpenr! + " " +  _nbpenr!
            model.alamat_penerima   = _alpenr!
            model.catatan_alamat_penerima = _adpenr!
            model.no_hp_penerima    = _nhpenr!
            model.is_complete       = true
            
            switch self.step {
            case 1:
                let viewDetailSpec      = UIKirimStep1()
                let temp                = ModKirimStep1()
                temp.kirimStep_1        = model
                temp.is_complete        = true
                viewDetailSpec.setupXib(modKirimStep1: temp)
                frameOriRect            = viewDetailSpec.getRect()
                viewDetailSpec.buttonCancel.addTapGestureRecognizer {
                    self.backStack()
                }
                viewDetailSpec.buttonConfirm.addTapGestureRecognizer {
                    self.gestureCancelShipment_1(_code: _code!)
                }
                viewDetail              = viewDetailSpec
                break;
            case 2, 3, 4, 5, 6, 7, 8:
                let viewDetailSpec      = UIKirimStep_1()
                viewDetailSpec.setupXib(modKirimStep_1: model)
                frameOriRect            = viewDetailSpec.getRect()
                viewDetail              = viewDetailSpec
                break
            default:
                return
            }
        }
        
        self.labelTitle.text = isShipment ? "KIRIM" : "ANTAR"
        viewDetail!.layer.cornerRadius = 10
        viewDetail!.layer.masksToBounds = true
        viewDetail?.frame = CGRect(x: 0, y: 0, width: scroll.frame.size.width, height: (frameOriRect.size.height))
        scroll.addSubview(viewDetail!)
        
        let co_he = NSLayoutConstraint(item: viewDetail!, attribute: .height, relatedBy: .equal, toItem: scroll, attribute: .height, multiplier: 1.0, constant: 0)
        let co_wi = NSLayoutConstraint(item: viewDetail!, attribute: .width, relatedBy: .equal, toItem: scroll, attribute: .width, multiplier: 1.0, constant: 0)
        scroll.addConstraints([co_he, co_wi])
        scroll.contentSize = (viewDetail?.frame.size)!
    }
}

extension ShipmentDeliveryDetailVC {
    func gestureCancelShipment_1(_code: String){
        let refreshAlert = UIAlertController(title: "Cancel Shipment", message: "Do you want to cancel your shipment", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.cancel_shipment(member_id: HomeVC.user.id!, shipment_id: _code, completion: self.completion)
        }))
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    func cancel_shipment(member_id: Int, shipment_id: String, completion: (() -> ())?) {
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.call_cancel_shipment(member_id: member_id, shipment_id: shipment_id) { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }

            // we dont care with the result
            UIViewController.removeSpinner(spinner: sv)
            E.showUIError(vc: self, message: "Shipment berhasil dibatalkan", title: "", completion: {
                self.backStack()
                if completion != nil {
                    completion!()
                }
            });
        }
    }
    
    func call_cancel_shipment(member_id: Int, shipment_id: String, callback: @escaping (DataResponse<Any>) -> Void) {
        let cancelParam: Parameters = [
            "id_shipper": member_id,
            "shipment_id": shipment_id
        ]
        
        Alamofire.request(KirimRouter.cancel_kirim(parameters: cancelParam)).responseJSON(completionHandler: callback)
    }
}


extension ShipmentDeliveryDetailVC {
    func gestureConfirmDelivery_2(yesElseNo: Bool, _code: String){
        let yesTitle = "Confirm"
        let noTitle = "Cancel"
        let yesMessage = "Apakah Anda yakin untuk konfirmasi?"
        let noMessage = "Apakah Anda yakin untuk batal?"
        
        let finalTitle = yesElseNo ? yesTitle : noTitle
        let finalMessage = yesElseNo ? yesMessage : noMessage
        
        let refreshAlert = UIAlertController(title: finalTitle, message: finalMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ya", style: .default, handler: { (action: UIAlertAction!) in
            self.confirm_delivery(isConfirm: yesElseNo, slot_id: _code, completion: {
                self.completion!()
            })
        }))
        refreshAlert.addAction(UIAlertAction(title: "Tidak", style: .cancel, handler: nil))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    func gestureManifestDelivery4to7(kvGoods: [Any]){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ManifestPopupVCID") as! ManifestPopupVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.goods_data = kvGoods
        self.present(vc, animated: true, completion: nil)
    }
    
    func confirm_delivery(isConfirm: Bool, slot_id: String, completion: (() -> ())?) {
        let sv = UIViewController.displaySpinner(onView: self.view)
        self.call_confirm_delivery(isConfirm: isConfirm, slot_id: slot_id) { json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    UIViewController.removeSpinner(spinner: sv)
                })
                return
            }
            
            // we dont care with the result
            UIViewController.removeSpinner(spinner: sv)
            let completeMessage = isConfirm ? "Konfirmasi berhasil" : "Pembatalan berhasil"
            E.showUISuccess(vc: self, message: completeMessage, title: "", completion: {
                self.backStack()
                if completion != nil {
                    completion!()
                }
            });
        }
    }
    
    func call_confirm_delivery(isConfirm: Bool, slot_id: String, callback: @escaping (DataResponse<Any>) -> Void) {
        let cancelParam: Parameters = [
            "slot_id": slot_id,
            "confirmation": isConfirm
        ]
        
        Alamofire.request(AntarRouter.konfirmasi_antar(parameters: cancelParam)).responseJSON(completionHandler: callback)
    }
}

extension ShipmentDeliveryDetailVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        print(AntarRouter.baseURLString + AntarRouter.kirim_tag(parameters: [:]).path)
        print(self.slot_id_for_send_tag_kirim)
        let sv = UIViewController.displaySpinner(onView: self.view)
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(UIImageJPEGRepresentation(image!, CGFloat(0.5))!, withName: "photo_tag", fileName: "sample.jpg", mimeType: "image/pg")
                multipartFormData.append((self.slot_id_for_send_tag_kirim.data(using: .utf8))!, withName: "slot_id")
            },
            to: String(AntarRouter.baseURLString) + "/delivery/send_tag",
            method: .post,
            headers: ["app-kind": "ios"],
            encodingCompletion: { encodingResult in
                print(encodingResult)
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { json_data in
                        
                        print(json_data)
                        // check if response is contains error or no result
                        if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                            E.showUIError(vc: self, completion: {
                                UIViewController.removeSpinner(spinner: sv)
                            })
                            return
                        }
                        
                        // we dont care with the result
                        UIViewController.removeSpinner(spinner: sv)
                        let completeMessage = "Foto telah terupload. Semoga perjalanan Anda menyenangkan"
                        E.showUISuccess(vc: self, message: completeMessage, completion: {
                            self.backStack()
                            if self.completion != nil {
                                self.completion!()
                            }
                        });
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    UIViewController.removeSpinner(spinner: sv)
                    print(encodingError)
                }
            }
        )
    }
}
