//
//  HomeVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 05/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit
import Alamofire

class HomeVC: XUIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    static var user: User = User()
    static var drawerController: HomeDrawerVC?
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var imageIklan: UIRoundedImageView!
    
    @IBOutlet weak var burgerKing: UIImageView!
    @IBOutlet weak var stackKirimToClick: UIStackView!
    @IBOutlet weak var stackAntarToClick: UIStackView!
    @IBOutlet weak var tabHeader: CTabView!             // -- C@1
    @IBOutlet weak var tabScroll: UIScrollView!
    @IBOutlet weak var containerLacak: UIView!
    @IBOutlet weak var buttonLacak: UIButton!
    @IBOutlet weak var containerTabScroll: UIView!      // -- C@2
    @IBOutlet weak var containerAKHeight: NSLayoutConstraint!
    
    @IBOutlet weak var background_container: UIUpperRoundedImageView!
    
    
    @IBOutlet weak var kirimTopCT: NSLayoutConstraint!
    @IBOutlet weak var kirimBottomCT: NSLayoutConstraint!
    @IBOutlet weak var dividerTopCT: NSLayoutConstraint!
    @IBOutlet weak var dividerBottomCT: NSLayoutConstraint!
    @IBOutlet weak var antarTopCT: NSLayoutConstraint!
    @IBOutlet weak var antarBottomCT: NSLayoutConstraint!
    
    
    
    var firstShowPromotion = true
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white
        self.scrollView.addSubview(refreshControl)
        self.scrollView.isScrollEnabled = true
        self.scrollView.alwaysBounceVertical = true
        self.containerTabScroll.layer.masksToBounds = true
        
        print(MasterDB().getSingleUser())
        
        stackAntarToClick.addTapGestureRecognizer {
            if C().isGuest() {
                E.showUIError(vc: self, message: "Silahkan login untuk melanjutkan")
                return
            }
            self.gotoAntarVC()
        }
        
        stackKirimToClick.addTapGestureRecognizer {
            self.gotoKirimVC()
        }
        
        burgerKing.addTapGestureRecognizer {
            HomeVC.drawerController?.setDrawerState(.opened, animated: true)
        }
        
        self.containerLacak.addTapGestureRecognizer {
            self.gotoLacak()
        }
        self.buttonLacak.addTapGestureRecognizer {
            self.gotoLacak()
        }

        self.tabScroll.delegate = self
        self.getHomeData()
        //self.adjustConstraintToDevice()
        self.runTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.stopTimer()
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !self.isConnectedToInternet() {
            self.setHomeStatusData()
        }
        
        self.setImageBanner()
        
        if self.firstShowPromotion && !C().isGuest() {
            self.getPromo()
            self.firstShowPromotion = false
        }
    }
    
    func adjustConstraintToDevice(){
        let percentage = (AppDelegate.screenSize?.width)! / CGFloat(Device.getSimpleNameDevice() == Device.IPHONE ? 720 : 850) * 0.7
        //let percentage = CGFloat(0.2)
        print("percentage is \(percentage)")
        kirimTopCT.constant = kirimTopCT.constant *  percentage
        kirimBottomCT.constant = kirimBottomCT.constant * percentage
        dividerTopCT.constant = dividerTopCT.constant * percentage
        dividerBottomCT.constant = dividerBottomCT.constant * percentage
        antarTopCT.constant = antarTopCT.constant * percentage
        antarBottomCT.constant = antarBottomCT.constant * percentage
        containerAKHeight.constant = containerAKHeight.constant * percentage
    }
    
    func isConnectedToInternet() -> Bool{
        guard let status = Network.reachability?.status else { return false }
        switch status {
        case .unreachable:
            return false
        case .wifi:
            return true
        case .wwan:
            return true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        getHomeData(pullRefresh: true)
    }
    
    func setNoSD(){
        self.background_container.backgroundColor = UIColor(white: 1, alpha: 0.5)
        self.tabHeader.isHidden = true
        self.tabScroll.isHidden = true
    }
    
    func setExistSD(){
        self.background_container.backgroundColor = UIColor(white: 1, alpha: 1.0)
        self.tabHeader.isHidden = false
        self.tabScroll.isHidden = false
    }
    
    func setHomeStatusData(){
        let masterDB = MasterDB()
        var datas: [[String:Any]] = []
        let shipments = masterDB.getAllShipments()
        let deliveries = masterDB.getAllDeliveries()
        
        let count_s_d = shipments.count + deliveries.count
        
        if(count_s_d == 0){
            self.setNoSD()
        } else {
            self.setExistSD()
        }

        // iterate shipments part
        for __s in shipments{
            let shipment_object = __s as? [String: Any]
            let shipment = shipment_object!["shipment"] as? [String: Any]
            let status = shipment_object!["status"] as? [String: Any]
            datas.insert([
                "status": status!,
                "shipment": shipment!,
                "type": "S",
                ], at: 0)
        }
        print(deliveries.debugDescription)
        
        // iterate deliveries part
        for __d in deliveries{
            let delivery_object = __d as? [String: Any]
            let delivery = delivery_object!["delivery"] as? [String: Any]
            let status = delivery_object!["status"] as? [String: Any]
            datas.insert([
                "status": status!,
                "delivery": delivery!,
                "type": "D",
                ], at: 0)
        }
        
        self.tabHeader.setCallbackClicked(c: {i in
            print(i)
            let moveXDelta = self.tabScroll.frame.size.width * CGFloat(i)
            self.tabScroll.setContentOffset(CGPoint(x: moveXDelta, y: self.tabScroll.contentOffset.y), animated: true)
        })
        self.tabHeader.initAll(datas: datas)
        
        var totalwidth = CGFloat(0)
        for d in datas {
            let _type = d["type"] as? String
            let _status = d["status"] as? [String: Any]
            
            let _step = _status!["step"] as? Int
            let _desc = _status!["description"] as? String
            let _detl = _status!["detail"] as? String
            
            // Note: only step 1, 2, 3, 4, 5, 6 that clickable
            // let with_down_arrow = _step! <= 6
            // Note: only step 1, 2, 3, 4, 5, 6 that clickable (revision 31 Juli 2018)
            let with_down_arrow = _step! <= 8 // (revision 31 Juli 2018)
            let v = self.createMiniDetail(isShipment: _type == "S",
                                          step: _step!,
                                          deskripsi: _desc!,
                                          detail: _detl ?? "",
                                          with_down_arrow: with_down_arrow)
            
            v.frame = CGRect(x: totalwidth, y: 0, width: v.frame.size.width, height: v.frame.size.height)
            self.tabScroll.addSubview(v)
            
            let passingData = d
            
            // clickable only for step 1, 2, 3, 4, and 5
            if with_down_arrow {
                v.addTapGestureRecognizer{
                    self.gotoShipmentDataVC(data: passingData)
                }
            }
            
            totalwidth += v.frame.size.width
        }
        
        self.tabScroll.contentSize = CGSize(width: totalwidth, height: self.tabScroll.frame.size.height)
        
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
    }
    
    func setImageBanner(){
        let image = MasterDB().getImage(name: "iklan")
        if image != nil {
            print("image ada")
            self.imageIklan.image = ViewController.maskCircleImage(image: image!, exactRadius: 10)
        } else {
            print("image tidak ada")
            self.getBanner()
        }
    }
    
    func getBanner() {
        Alamofire.request(HomeRouter.iklan(parameters: [:])).responseJSON(completionHandler: {
            json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                return
            }
            
            // parse the main result
            let arrresult = E.getData(json_data: json_data)["iklan"] as! [Any]
            if arrresult.count == 0 {
                return
            }
            
            let result = arrresult[0] as! [String: Any]
            var imageUrl = result["img_src"] as! String
            imageUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            if let url = URL(string: imageUrl) {
                self.imageIklan.sd_setImage(with: url) { (image, err, sdicach, url) in
                    MasterDB().storeImage(name: "iklan", url: imageUrl, data: image!)
                    self.imageIklan.image = ViewController.maskCircleImage(image: image!, exactRadius: 10)
                }
            } else {
                print("could not open url, it was nil")
            }
        })
    }
    
    func getPromo() {
        Alamofire.request(HomeRouter.promo(parameters: ["id_user": HomeVC.user.id!])).responseJSON(completionHandler: {
            json_data in
            
            print(json_data)
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                return
            }
            
            // parse the main result
            let arrresult = E.getData(json_data: json_data)["promo"] as! [Any]
            if arrresult.count == 0 {
                return
            }
            
            let result = arrresult[0] as! [String: Any]
            let mod = ModPromotion()
            
            mod.promo_id = result["id"] as? Int
            mod.description = result["content"] as? String
            mod.image_promo = result["img_src"] as? String
            self.showPromotion(mod: mod)
        })
    }
    
    func getHomeData(pullRefresh: Bool? = false){
        var sv: UIView? = nil
        if !pullRefresh! {
            sv = UIViewController.displaySpinner(onView: self.view)
        }
        
        getHomeData {
            json_data in
            
            // check if response is contains error or no result
            if E.isResponseErrorExist(json_data: json_data) || !E.isResponseDataExist(json_data: json_data) {
                E.showUIError(vc: self, completion: {
                    if !pullRefresh! {
                        UIViewController.removeSpinner(spinner: sv!)
                    }
                    
                    if self.refreshControl.isRefreshing {
                        self.refreshControl.endRefreshing()
                    }
                })
                return
            }
            
            
            // parse the main result
            let result = E.getData(json_data: json_data)
            
            // parse deliveries and shipments part
            let deliveries      = result["delivery"] as? [Any]
            let shipments       = result["shipments"] as? [Any]
            let static_data     = result["static_data"] as? [String: Any]
            let etc_message     = result["etc_message"] as? String
            let referral        = result["referral"] as? [String: Any]
            let money           = result["money"] as? Int
            S.etc_message       = etc_message!
            HomeVC.user.money   = String(money!)
            self.storeReferralIfExist(data: referral)
            self.cacheAllStaticData(data: static_data)
            
            let masterDB = MasterDB()
            masterDB.resetAndInsertShipments(shipments: shipments!, deliveries: deliveries!)
            _ = masterDB.updateBalance(money: Int64(money!))
            HomeVC.user = LoginVC.parseUserLoginData(data: MasterDB().getSingleUser())!

            self.setHomeStatusData()
            if !pullRefresh! {
                UIViewController.removeSpinner(spinner: sv!)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        print("page number: " + pageNumber.description)
        self.tabHeader.scrollToTab(n: Int.init(pageNumber))
    }
    
    override func backStack(){
        ViewController.isStackNextVCActive = false
        super.backStack()
    }
    
    func getHomeData(callback: @escaping (DataResponse<Any>) -> Void){
        let homeParam: Parameters = [
            "member_id": HomeVC.user.id!
        ]
        
        Alamofire.request(HomeRouter.home(parameters: homeParam)).responseJSON(completionHandler: callback)
    }
    
    func createMiniDetail(isShipment: Bool, step: Int, deskripsi: String, detail: String? = "", with_down_arrow: Bool) -> UIView{
        // init model
        let modMiniStepArrow = ModMiniStepArrow()
        
        let modMiniStepDetail = ModMiniStepDetail()
        modMiniStepDetail.deskripsi = deskripsi
        modMiniStepDetail.detail = detail!
        modMiniStepDetail.is_shipment = isShipment
        modMiniStepDetail.step = step
        modMiniStepDetail.is_complete = true
        
        modMiniStepArrow.using_arrow = with_down_arrow
        modMiniStepArrow.mini_step_detail = modMiniStepDetail
        modMiniStepArrow.is_complete = true
        
        let c = UIMiniStepArrow(modMiniStepArrow: modMiniStepArrow)
        c.frame = CGRect(x: 0, y: 0, width: tabScroll.frame.size.width, height: tabScroll.frame.size.height)
        
        return c
    }
    
    func gotoShipmentDataVC(data: Any?){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShipmentDataVCID") as! ShipmentDeliveryDetailVC
        vc.data = data
        vc.completion = ({ () -> () in
            self.getHomeData()
        })
        self.present(vc, animated: true, completion: nil)
    }
    
    func gotoAntarVC(){
        /*
        let storyboard = UIStoryboard(name: "Antar", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AntarNAVCID") as! GeneralNAVC
        vc.listGesturesStackCan = [
            2 // only on second stack navigation
        ]
        vc.completion = ({ () -> () in
            self.getHomeData()
        })
        self.present(vc, animated: true, completion: nil)
        */
        
        let storyboard = UIStoryboard(name: "Antar", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AntarDrawerVCID") as! AntarDrawerVC
        vc.completion = ({ () -> () in
            self.getHomeData()
        })
        self.present(vc, animated: true, completion: nil)
    }
    
    func gotoKirimVC(){
        /*
        let storyboard = UIStoryboard(name: "Kirim", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "KirimNAVCID") as! GeneralNAVC
        vc.listGesturesStackCan = [
            2, // Alamat Pengirim VC
            3, // Alamat Penerima VC
            4, // Persetujuan VC
            5, // Asuransi VC
            6  // Metode Pembayaran VC
        ]
        vc.completion = ({ () -> () in
            self.getHomeData()
        })
        self.present(vc, animated: true, completion: nil)
        */
        
        let storyboard = UIStoryboard(name: "Kirim", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "KirimDrawerVCID") as! KirimDrawerVC
        vc.completion = ({ () -> () in
            self.getHomeData()
        })
        self.present(vc, animated: true, completion: nil)
    }
    
    func gotoLacak(){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LacakVCID") as! LacakVC
        self.present(vc, animated: true, completion: nil)
    }
    
    func showPromotion(mod: ModPromotion){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PromotionVCID") as! PromotionVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.data = mod
        
        self.present(vc, animated: true, completion: nil)
    }
}

class UIRoundedImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 10)
    }
}

class UIUpperRoundedImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners([.topLeft, .topRight], radius: 10)
    }
}

class UILowerRoundedImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners([.bottomLeft, .bottomRight], radius: 10)
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension HomeVC {
    func storeReferralIfExist(data: [String: Any]? = nil){
        if data == nil {
            S.referral = nil
            print("Error: no referral served")
            return
        }
        
        print("OK: referral served")
        S.referral = SReferral()
        S.referral?.referral_amount    = data!["referral_amount"] as? Double
        S.referral?.referred_amount    = data!["referred_amount"] as? Double
    }
}


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
// -- EXTENSION FOR CACHING DATA -- //
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //

extension HomeVC {
    func cacheAllStaticData(data: [String: Any]? = nil){
        if data == nil {
            print("Error: no static_data served")
            return
        }
        
        // parse airport data
        let airport                 = data!["airport"] as? [Any]
        self.cacheAirportData(data: airport)
        
        // parse goods weight data for shipment and delivery
        let goods_weight            = data!["goods_weight"] as? [String: Any]
        self.cacheGoodsWeightData(data: goods_weight)
        
        let city                    = data!["airport_city"] as? [Any]
        self.cacheCityData(data: city)
        
        let city_price              = data!["airport_city_price"] as? [Any]
        self.cacheCityPriceData(data: city_price)
        
        let location                = data!["location"] as? [String: Any]
        self.cacheLocationData(data: location)
        
        let price_goods_estimate    = data!["price_goods_estimate"] as? [Any]
        self.cachePriceGoodsEstimateData(data: price_goods_estimate)
        
        let payment_method          = data!["payment_method"] as? [Any]
        self.cachePaymentMethodData(data: payment_method)
        
        let insurance                = data!["insurance"] as? [String: Any]
        self.cacheInsuranceData(data: insurance)
        
        let statusesd                = data!["all_status"] as? [String: Any]
        self.cacheStatusesData(data: statusesd)
    }
    
    func cacheInsuranceData(data: [String: Any]? = nil){
        if data == nil {
            print("Error: no static_data.insurance data served")
            return
        }
        
        let default_ins = Float((data!["default_insurance"] as? String)!)
        let adition_ins = Float((data!["additional_insurance"] as? Int)!)
        
        S.insurance.default_insurance       = default_ins
        S.insurance.additional_insurance    = adition_ins
    }
    
    func cacheLocationData(data: [String: Any]? = nil){
        if data == nil {
            print("Error: no static_data.location data served")
            return
        }
        
        let province    = data!["province"]     as? [Any]
        self.cacheLocationProvinceData(data: province)
        
        let city        = data!["city"]         as? [Any]
        self.cacheLocationCityData(data: city)
        
        let subdistrict = data!["subdistrict"]  as? [Any]
        self.cacheLocationSubdistrictData(data: subdistrict)
    }
    
    func cacheStatusesData(data: [String: Any]? = nil){
        if data == nil {
            print("Error: no static_data.all_status data served")
            return
        }
        
        let delivery_statuses   = data!["shipment"] as? [Any]
        self.cacheStatus(isShipmentElseDelivery: false, data: delivery_statuses)
        
        let shipment_statuses   = data!["delivery"] as? [Any]
        self.cacheStatus(isShipmentElseDelivery: true, data: shipment_statuses)
    }
    
    func cacheLocationProvinceData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.location.province data served")
            return
        }
        
        // keep clean
        S.location_province.removeAll(keepingCapacity: false)
        
        // iterate data
        for __lp in data! {
            // parse
            let dict_loc_prov = __lp as? [String: Any]
            
            // new instance
            let loc_prov = SLocProvince()
            loc_prov.id     = dict_loc_prov!["id"] as? Int
            loc_prov.name   = dict_loc_prov!["name"] as? String
            
            // append new element
            S.location_province.append(loc_prov)
        }
    }
    
    func cacheLocationCityData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.location.city data served")
            return
        }
        
        // keep clean
        S.location_city.removeAll(keepingCapacity: false)
        
        // iterate data
        for __lc in data! {
            // parse
            let dict_loc_city = __lc as? [String: Any]
            
            // new instance
            let loc_city = SLocCity()
            loc_city.id             = dict_loc_city!["id"] as? Int
            loc_city.name           = dict_loc_city!["name"] as? String
            loc_city.id_airportcity = dict_loc_city!["id_airportcity"] as? Int
            loc_city.id_province    = dict_loc_city!["id_province"] as? Int
            
            // append new element
            S.location_city.append(loc_city)
        }
    }
    
    func cacheLocationSubdistrictData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.location.subdistrict data served")
            return
        }
        
        // keep clean
        S.location_subdistrict.removeAll(keepingCapacity: false)
        
        // iterate data
        for __ls in data! {
            // parse
            let dict_loc_subd = __ls as? [String: Any]
            
            // new instance
            let loc_subd = SLocSubdistrict()
            loc_subd.id             = dict_loc_subd!["id"] as? Int
            loc_subd.name           = dict_loc_subd!["name"] as? String
            loc_subd.id_province    = dict_loc_subd!["id_province"] as? Int
            loc_subd.id_city        = dict_loc_subd!["id_city"] as? Int
            
            // append new element
            S.location_subdistrict.append(loc_subd)
        }
    }
    
    func cacheStatus(isShipmentElseDelivery: Bool, data: [Any]? = nil){
        let key = isShipmentElseDelivery ? "shipment" : "delivery"
        if data == nil {
            print("Error: no static_data.all_status.\(key) data served")
            return
        }
        
        // keep clean
        if isShipmentElseDelivery {
            S.statuses_shipment.removeAll(keepingCapacity: false)
        } else {
            S.statuses_delivery.removeAll(keepingCapacity: false)
        }
        
        // iterate data
        for __s in data! {
            // parse
            let dict_status = __s as? [String: Any]
            
            // new instance
            let status = SStatusShipmentDelivery()
            status.id           = dict_status!["id"] as? Int
            status.step         = dict_status!["step"] as? Int
            status.description  = dict_status!["description"] as? String
            
            // append new element
            if isShipmentElseDelivery {
                S.statuses_shipment.append(status)
            } else {
                S.statuses_delivery.append(status)
            }
        }
    }
    
    func cacheCityData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.airport_city data served")
            return
        }
        
        // keep clean
        S.airport_city_list.removeAll(keepingCapacity: false)
        
        // iterate data
        for __c in data! {
            // parse
            let dict_city = __c as? [String: Any]
            
            // new instance
            let city = SCity()
            city.id     = dict_city!["id"] as? Int
            city.name   = dict_city!["name"] as? String
            
            // append new element
            S.airport_city_list.append(city)
        }
    }
    
    func cacheCityPriceData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.airport_city_price data served")
            return
        }
        
        // keep clean
        S.airport_city_price_list.removeAll(keepingCapacity: false)
        
        // iterate data
        for __c in data! {
            // parse
            let dict_city_price = __c as? [String: Any]
            
            // new instance
            let city_price = SCityPrice()
            city_price.id_origin      = dict_city_price!["id_origin_city"] as? Int
            city_price.id_destination = dict_city_price!["id_destination_city"] as? Int
            city_price.regular        = dict_city_price!["regular"] as? Int
            city_price.gold           = dict_city_price!["gold"] as? Int
            
            // append new element
            S.airport_city_price_list.append(city_price)
        }
    }
    
    func cacheAirportData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.airport data served")
            return
        }
        
        // keep clean
        S.airport_list.removeAll(keepingCapacity: false)
        
        // iterate data
        for __a in data! {
            // parse
            let dict_airport = __a as? [String: Any]
            
            // new instance
            let s_airport = SAirport()
            s_airport.id            = dict_airport!["id"] as? Int
            s_airport.initial_code  = dict_airport!["initial_code"] as? String
            s_airport.name          = dict_airport!["name"] as? String
            s_airport.id_city       = dict_airport!["id_city"] as? Int
            s_airport.city_name     = dict_airport!["city_name"] as? String
            
            // append new element
            S.airport_list.append(s_airport)
        }
    }
    
    func cacheGoodsWeightData(data: [String: Any]? = nil){
        if data == nil {
            print("Error: no static_data.goods_weight data served")
            return
        }
        
        let goods_weight_s  = data!["shipment"] as? [Int]
        let goods_weight_d  = data!["delivery"] as? [Int]
        
        // cache the data
        S.goods_weight_list_shipment.removeAll(keepingCapacity: false)
        S.goods_weight_list_delivery.removeAll(keepingCapacity: false)
        for w in goods_weight_s! {
            S.goods_weight_list_shipment.append(SWeightInt(x: w))
        }
        for w in goods_weight_d! {
            S.goods_weight_list_delivery.append(SWeightInt(x: w))
        }
    }
    
    func cachePriceGoodsEstimateData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.price_goods_estimate data served")
            return
        }
        
        // keep clean
        S.price_goods_estimate.removeAll(keepingCapacity: false)
        
        // iterate data
        for __p in data! {
            // parse
            let dict_price_ge = __p as? [String: Any]
            
            // new instance
            let s_price_ge = SPriceGoodsEstimate()
            s_price_ge.id               = dict_price_ge!["id"] as? Int
            s_price_ge.price_estimate   = dict_price_ge!["price_estimate"] as? String
            s_price_ge.nominal          = dict_price_ge!["nominal"] as? Int
            
            // append new element
            S.price_goods_estimate.append(s_price_ge)
        }
    }
    
    func cachePaymentMethodData(data: [Any]? = nil){
        if data == nil {
            print("Error: no static_data.payment_method data served")
            return
        }
        
        // keep clean
        S.payment_method.removeAll(keepingCapacity: false)
        
        // iterate data
        for __pm in data! {
            // parse
            let dict_paymentm = __pm as? [String: Any]
            
            // new instance
            let s_paymentm = SPaymentMethod()
            s_paymentm.bank_code        = dict_paymentm!["bankCode"] as? String
            s_paymentm.product_code     = dict_paymentm!["productCode"] as? String
            s_paymentm.product_name     = dict_paymentm!["productName"] as? String
            s_paymentm.is_cash          = dict_paymentm!["isCash"] as? Bool
            
            // append new element
            S.payment_method.append(s_paymentm)
        }
    }
}

extension HomeVC {
    func runTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 120, target: self, selector: (#selector(HomeVC.callHomeStatus)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        self.timer.invalidate()
    }
    
    @objc func callHomeStatus(){
        self.getHomeData()
    }
}

class User{
    var id: Int?
    var birth_date: String?
    var first_name: String?
    var last_name: String?
    var mobile_phone_no: String?
    var email: String?
    var ref_code: String?
    var money: String?
    var profil_picture: String?
    var sex: String?
    var sms_code: String?
}
