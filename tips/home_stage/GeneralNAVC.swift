//
//  AntarNAVC.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 30/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import UIKit

class GeneralNAVC: UINavigationController, UIGestureRecognizerDelegate {

    var completion: (() -> ())?
    var listGesturesStackCan: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return self.listGesturesStackCan.contains(viewControllers.count)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
