//
//  KirimRouter.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 21/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

enum KirimRouter: URLRequestConvertible {
    case submit_kirim(parameters: Parameters)
    case cancel_kirim(parameters: Parameters)
    case status_kirim(parameters: Parameters)
    case fav_addr(parameters: Parameters)
    case add_fav_addr(parameters: Parameters)
    case remove_fav_addr(parameters: Parameters)
    case is_full_fav_addr(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL
    
    var method: HTTPMethod {
        switch self {
        case .submit_kirim:
            return .post
        case .cancel_kirim:
            return .post
        case .status_kirim:
            return .get
        case .fav_addr:
            return .get
        case .add_fav_addr:
            return .post
        case .remove_fav_addr:
            return .get
        case .is_full_fav_addr:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .submit_kirim:
            return "/shipment"
        case .cancel_kirim:
            return "/shipment/cancel"
        case .status_kirim:
            return "/shipment/status"
        case .fav_addr:
            return "/favorite_address"
        case .add_fav_addr:
            return "/favorite_address"
        case .remove_fav_addr(let parameters):
            print("/favorite_address/del/\( (parameters["id"] as! Int).description )")
            return "/favorite_address/del/\( (parameters["id"] as! Int).description )"
        case .is_full_fav_addr:
            return "/favorite_address/is_full"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try KirimRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
        
        switch self {
        case .submit_kirim(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .cancel_kirim(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .status_kirim(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .fav_addr(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .add_fav_addr(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .remove_fav_addr(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .is_full_fav_addr(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
