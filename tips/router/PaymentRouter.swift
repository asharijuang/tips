//
//  OnlinePayment.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 24/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

enum PaymentRouter: URLRequestConvertible {
    case transaction(parameters: Parameters)
    case payment_status(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL
    
    var method: HTTPMethod {
        switch self {
        case .transaction:
            return .post
        case .payment_status:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .transaction:
            return "/transaction"
        case .payment_status:
            return "/payment/status"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try AntarRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
        
        switch self {
        case .transaction(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .payment_status(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}

