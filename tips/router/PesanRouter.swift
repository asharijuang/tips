//
//  PesanRouter.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 05/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//


import Foundation
import Alamofire

enum PesanRouter: URLRequestConvertible {
    case get_all_messages(parameters: Parameters)
    case delete_message(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL + "/pesan"
    
    var method: HTTPMethod {
        switch self {
        case .get_all_messages:
            return .get
        case .delete_message:
            return .delete
        }
    }
    
    var path: String {
        switch self {
        case .get_all_messages(let parameters):
            return "/\((parameters["id_user"] as! Int).description)"
        case .delete_message(let parameters):
            return "/\((parameters["id_user"] as! Int).description)/\((parameters["id_pesan"] as! Int).description)"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try PesanRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
        
        switch self {
        case .get_all_messages(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .delete_message(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}

