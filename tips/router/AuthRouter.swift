//
//  LoginRouter.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 07/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

enum AuthRouter: URLRequestConvertible {
    case login(parameters: Parameters)
    case login_fb(parameters: Parameters)
    case login_twitter(parameters: Parameters)
    case verify_fb_phonenum(parameters: Parameters)
    case guest(parameters: Parameters)
    case reset_password(parameters: Parameters)
    case register(parameters: Parameters)
    case verifySMS(parameters: Parameters)
    case resendSMS(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL
    
    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .login_fb:
            return .post
        case .login_twitter:
            return .post
        case .verify_fb_phonenum:
            return.post
        case .guest:
            return .post
        case .reset_password:
            return .get
        case .register:
            return .post
        case .verifySMS:
            return .post
        case .resendSMS:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .login:
            return "/login"
        case .login_fb:
            return "/login/fb"
        case .login_twitter:
            return "/login/twitter"
        case .verify_fb_phonenum:
            return "/verify/fb_twitter_pn"
        case .guest:
            return "/login/device"
        case .reset_password:
            return "/reset_password"
        case .register:
            return "/register"
        case .verifySMS:
            return "/verify/phone"
        case .resendSMS:
            return "/verify/resend"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try AuthRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
        
        switch self {
        case .login(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .login_fb(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .login_twitter(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .verify_fb_phonenum(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .guest(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .reset_password(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .register(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .verifySMS(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .resendSMS(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
