//
//  RiwayatRouter.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 06/07/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

enum RiwayatRouter: URLRequestConvertible {
    case data_antar(parameters: Parameters)
    case data_kirim(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL
    
    var method: HTTPMethod {
        switch self {
        case .data_antar:
            return .post
        case .data_kirim:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .data_antar:
            return "/delivery/search"
        case .data_kirim:
            return "/shipment/search"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try RiwayatRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
//        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .data_antar(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .data_kirim(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
