//
//  AntarRouter.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 18/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

enum AntarRouter: URLRequestConvertible {
    case submit_antar(parameters: Parameters)
    case konfirmasi_antar(parameters: Parameters)
    case kirim_tag(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL
    
    var method: HTTPMethod {
        switch self {
        case .submit_antar:
            return .post
        case .konfirmasi_antar:
            return .post
        case .kirim_tag:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .submit_antar:
            return "/delivery"
        case .konfirmasi_antar:
            return "/delivery/confirm"
        case .kirim_tag:
            return "/delivery/send_tag"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try AntarRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
        
        switch self {
        case .submit_antar(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .konfirmasi_antar(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .kirim_tag(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
