//
//  CR.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 07/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation

class __CR{
    static let prefix_url_prod = "iris"
    static let prefix_url_dev = "ragnar"
    
    static var prefix = C().getEndpointConfig() ? prefix_url_prod : prefix_url_dev
    static var VERY_BASE_URL: String = "https://" + prefix + ".tips.co.id/"
    static var BASE_URL: String = "\(VERY_BASE_URL)api"
    
    static func changeEndpoint() {
        __CR.prefix = C().getEndpointConfig() ? prefix_url_prod : prefix_url_dev
        __CR.VERY_BASE_URL = "https://" + __CR.prefix + ".tips.co.id/"
        __CR.BASE_URL = "\(__CR.VERY_BASE_URL)api"
    }
}
