//
//  HomeRouter.swift
//  app
//
//  Created by Rio Chandra Rajagukguk on 07/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

enum HomeRouter: URLRequestConvertible {
    case home(parameters: Parameters)
    case help(parameters: Parameters)
    case terms_n_conds(parameters: Parameters)
    case redeem(parameters: Parameters)
    case iklan(parameters: Parameters)
    case promo(parameters: Parameters)
    case ambil_promo(parameters: Parameters)
    case report(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL
    
    var method: HTTPMethod {
        switch self {
        case .home:
            return .get
        case .help:
            return .get
        case .terms_n_conds:
            return .get
        case .redeem:
            return .get
        case .iklan:
            return .get
        case .promo:
            return .get
        case .ambil_promo:
            return .post
        case .report:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .home:
            return "/home"
        case .help:
            return "/help"
        case .terms_n_conds:
            return "/term"
        case .redeem:
            return "/promos"
        case .iklan:
            return "/iklan"
        case .promo:
            return "/promo"
        case .ambil_promo:
            return "/promo"
        case .report:
            return "/report"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try HomeRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
        
        switch self {
        case .home(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .help(_):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        case .terms_n_conds(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .redeem(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .iklan(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .promo(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .ambil_promo(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .report(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
