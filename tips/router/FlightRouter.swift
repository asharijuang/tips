//
//  FlightRouter.swift
//  tips
//
//  Created by Rio Chandra Rajagukguk on 18/06/18.
//  Copyright © 2018 TIPS. All rights reserved.
//

import Foundation
import Alamofire

enum FlightRouter: URLRequestConvertible {
    case airport_list(parameters: Parameters)
    case check_flight(parameters: Parameters)
    case create_flight(parameters: Parameters)
    
    static let baseURLString = __CR.BASE_URL + "/flight"
    
    var method: HTTPMethod {
        switch self {
        case .airport_list:
            return .get
        case .check_flight:
            return .post
        case .create_flight:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .airport_list:
            return "/airport"
        case .check_flight:
            return "/check_flight_b_n_d"
        case .create_flight:
            return "/create_flight"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try FlightRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("ios", forHTTPHeaderField: "app-kind")
        
        switch self {
        case .airport_list(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .check_flight(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .create_flight(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
